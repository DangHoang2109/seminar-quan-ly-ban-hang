namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StoreCashModel")]
    public partial class StoreCashModel
    {
        public int id { get; set; }

        public string detail { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dateCreate { get; set; }

        public long? valueBill { get; set; }

        public int? storeID { get; set; }

        public int? stateID { get; set; }

        public int? orderID { get; set; }

        public int? creatorID { get; set; }

        public virtual AccountModel AccountModel { get; set; }

        public virtual CashFlowState CashFlowState { get; set; }

        public virtual StoreModel StoreModel { get; set; }
    }
}
