namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExportOrderModel")]
    public partial class ExportOrderModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ExportOrderModel()
        {
            ExportOrderInfoModels = new HashSet<ExportOrderInfoModel>();
        }

        public int id { get; set; }

        public int? customerID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dateExport { get; set; }

        public long? shippingFee { get; set; }

        public int? storeID { get; set; }

        public int? stateID { get; set; }

        public long? extraFeeForReceiver { get; set; }

        public string reasonExport { get; set; }

        public string orderNote { get; set; }

        public int? creatorID { get; set; }

        public virtual AccountModel AccountModel { get; set; }

        public virtual CustomerModel CustomerModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExportOrderInfoModel> ExportOrderInfoModels { get; set; }

        public virtual ExportOrderState ExportOrderState { get; set; }

        public virtual StoreModel StoreModel { get; set; }
    }
}
