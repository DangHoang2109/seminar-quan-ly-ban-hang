namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StoreInventoryModel")]
    public partial class StoreInventoryModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StoreInventoryModel()
        {
            ExportOrderInfoModels = new HashSet<ExportOrderInfoModel>();
        }

        public int id { get; set; }

        public int? storeID { get; set; }

        public int? importOrderProductID { get; set; }

        public int? stockProduct { get; set; }

        public int? creatorID { get; set; }

        public virtual AccountModel AccountModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExportOrderInfoModel> ExportOrderInfoModels { get; set; }

        public virtual ImportOrderInfoModel ImportOrderInfoModel { get; set; }

        public virtual StoreModel StoreModel { get; set; }
    }
}
