﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
namespace QLBH.Model
{
    /// <summary>
    /// Model abstract cho các mẫu order, truyền Model từ database vào
    /// </summary>
    public class OrderModel : baseViewModel
    {
        protected int? stateID;
        public List<SolidColorBrush> orderOutlineColor;
        public List<SolidColorBrush> orderBackgroundColor;
        public List<SolidColorBrush> orderTextStateColor;

        protected SolidColorBrush outlineColor;
        protected SolidColorBrush backgroundColor;
        protected SolidColorBrush textStateColor;

        public virtual SolidColorBrush OutlineColor
        {
            get
            {
                outlineColor = orderOutlineColor[(int)stateID-1];
                return outlineColor;
            }
            set
            {
                outlineColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush BackgroundColor
        {
            get {
                backgroundColor = orderBackgroundColor[(int)stateID-1];
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush TextStateColor
        {
            get
            {
                textStateColor = orderOutlineColor[(int)stateID-1];
                return textStateColor;
            }
            set
            {
                textStateColor = value;
                OnPropertyChanged();
            }
        }

        protected long? totalBill;
        public virtual long? TotalBill
        {
            get => totalBill;
            set
            {
                totalBill = value;
                OnPropertyChanged();
            }
        }

        protected int? totalProduct;
        public virtual int? TotalProduct
        {
            get => totalProduct;
            set
            {
                totalProduct = value;
                OnPropertyChanged();
            }
        }


        public OrderModel()
        {

        }

        protected virtual void CreateColorAsset()
        {

        }

    }

    public class QLBH_ImportOrderModel : OrderModel
    {

        private ImportOrderModel baseOrder;
        public virtual ImportOrderModel BaseOrder
        {
            get => baseOrder;
            set
            {
                baseOrder = value;
                OnPropertyChanged();
            }
        }

        public void ParseData(ImportOrderModel order)
        {
            this.BaseOrder = order;
            this.totalBill = 0;
            List<ImportOrderInfoModel> detail = new List<ImportOrderInfoModel>(order.ImportOrderInfoModels);
            for (int i = 0; i < detail.Count; i++)
            {
                this.totalBill += detail[i].priceImport * detail[i].amount;
            }
            this.TotalBill += order.shippingFee;
            this.stateID = order.stateID;

        }
        public QLBH_ImportOrderModel(ImportOrderModel order)
        {
            this.ParseData(order);

            CreateColorAsset();
        }

        protected override void CreateColorAsset()
        {
            base.CreateColorAsset();
            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkGoldenrod,
                Brushes.DarkCyan,
                Brushes.DarkCyan,
                Brushes.DarkGreen
            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightYellow,
                Brushes.LightCyan,
                Brushes.LightCyan,
                Brushes.LightGreen,
            };
        }
    }

    public class QLBH_ExportConverterDetailModel : baseViewModel
    {
        private ExportOrderInfoModel baseModel;
        public ExportOrderInfoModel BaseModel
        {
            get => baseModel;
            set
            {
                baseModel = value;
                OnPropertyChanged();
            }
        }

        private int sumAmount;
        public int SumAmount
        {
            get => sumAmount;
            set
            {
                sumAmount = value;
                OnPropertyChanged();
            }
        }

        private long? totalProfit;
        public long? TotalProfit
        {
            get => totalProfit;
            set
            {
                totalProfit = value;
                OnPropertyChanged();
            }
        }

        private long? totalRevenue;
        public long? TotalRevenue
        {
            get => totalRevenue;
            set
            {
                totalRevenue = value;
                OnPropertyChanged();
            }
        }

        public QLBH_ExportConverterDetailModel(ExportOrderInfoModel info)
        {
            this.BaseModel = info;
            this.SumAmount = (int)info.amount;
            this.TotalRevenue = (info.priceExport - info.priceDiscount) * info.amount;
            this.TotalProfit = TotalRevenue - (info.StoreInventoryModel.ImportOrderInfoModel.priceImport * this.sumAmount);

        }
    }

    public class QLBH_ExportOrderModel : OrderModel
    {

        private ExportOrderModel baseOrder;
        public virtual ExportOrderModel BaseOrder
        {
            get => baseOrder;
            set
            {
                baseOrder = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<QLBH_ExportConverterDetailModel> detailConverted;
        public ObservableCollection<QLBH_ExportConverterDetailModel> DetailConverted
        {
            get => detailConverted;
            set
            {
                detailConverted = value;
                OnPropertyChanged();
            }
        }

        private long? totalProfit;
        public long? TotalProfit
        {
            get => totalProfit;
            set
            {
                totalProfit = value;
                OnPropertyChanged();
            }
        }

        public void ParseData(ExportOrderModel order)
        {
            this.BaseOrder = order;
            this.totalBill = 0;
            this.totalProduct = 0;
            this.totalProfit = 0;

            this.detailConverted = new ObservableCollection<QLBH_ExportConverterDetailModel>();
            foreach(ExportOrderInfoModel info in order.ExportOrderInfoModels)
            {
                QLBH_ExportConverterDetailModel cacheInfo = this.detailConverted.ToList().Find(x => x.BaseModel.StoreInventoryModel.ImportOrderInfoModel.productID == info.StoreInventoryModel.ImportOrderInfoModel.productID);
                if (cacheInfo == null)
                {
                    cacheInfo = new QLBH_ExportConverterDetailModel(info);
                    this.DetailConverted.Add(cacheInfo);
                }
                else cacheInfo.SumAmount += (int)info.amount;

                this.TotalBill += cacheInfo.TotalRevenue;
                this.TotalProduct += info.amount;
                this.TotalProfit += cacheInfo.TotalProfit;
            }

            if(order.shippingFee != null)
            {
                this.TotalBill -= order.shippingFee;
                this.TotalProfit -= order.shippingFee;
            }
            if (order.extraFeeForReceiver != null)
            {
                this.TotalBill += order.extraFeeForReceiver;
                this.TotalProfit += order.extraFeeForReceiver;
            }

            this.stateID = order.stateID;

        }
        public QLBH_ExportOrderModel(ExportOrderModel order)
        {
            this.ParseData(order);
            this.CreateColorAsset();
        }

        protected override void CreateColorAsset()
        {
            base.CreateColorAsset();
            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkGoldenrod,
                Brushes.DarkCyan,
                Brushes.DarkCyan,
                Brushes.DarkGreen
            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightYellow,
                Brushes.LightCyan,
                Brushes.LightCyan,
                Brushes.LightGreen,
            };
        }
    }
}
