﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace QLBH.Model
{
    public class QLBH_ProductItemModel : baseViewModel
    {
        protected ProductModel baseProduct;
        public ProductModel BaseProduct { get => baseProduct; set
            {
                baseProduct = value;
                OnPropertyChanged();
            }
        }

        protected int productSold;
        public int ProductSold
        {
            get => productSold; set
            {
                productSold = value;
                OnPropertyChanged();
            }
        }

        protected int? stateID;
        public List<SolidColorBrush> orderOutlineColor;
        public List<SolidColorBrush> orderBackgroundColor;
        public List<SolidColorBrush> orderTextStateColor;

        protected SolidColorBrush outlineColor;
        protected SolidColorBrush backgroundColor;
        protected SolidColorBrush textStateColor;

        public virtual SolidColorBrush OutlineColor
        {
            get
            {
                outlineColor = orderOutlineColor[GetColorIndexById()];
                return outlineColor;
            }
            set
            {
                outlineColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush BackgroundColor
        {
            get
            {
                backgroundColor = orderBackgroundColor[GetColorIndexById()];
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush TextStateColor
        {
            get
            {
                textStateColor = orderOutlineColor[GetColorIndexById()];
                return textStateColor;
            }
            set
            {
                textStateColor = value;
                OnPropertyChanged();
            }
        }
        protected virtual int GetColorIndexById()
        {
            switch (stateID)
            {
                case 2:
                    return 0;
                case 5: return 1;
                case 6: return 2;
                default: return 1;
            }
        }

        public virtual void ParseData(ProductModel product)
        {
            this.BaseProduct = product;
            this.ProductSold = DataProvider.DataController.Instance.GetSoldProduct(product.id);

            this.stateID = product.stateID;
        }
        public virtual void CreateaColorAsset()
        {

            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkCyan,
                Brushes.DarkGreen
            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightCyan,
                Brushes.LightGreen,
            };
        }

        public QLBH_ProductItemModel(ProductModel product)
        {
            this.ParseData(product);
            this.CreateaColorAsset();
        }
    }
}
