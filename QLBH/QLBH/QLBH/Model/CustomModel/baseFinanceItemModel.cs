﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace QLBH.Model
{
    public class FinanceReceiptIcon
    {
        public const string PAYMENT = "MinusCircle";
        public const string RECEIPT = "PlusCircle";
        public const string COLOR_PAYMENT = "Red";
        public const string COLOR_RECEIPT = "DarkCyan";
    }
    public enum BillType
    {
        PAYMENT_FROM_ODER = 0,
        RECEIPT_FROM_ORDER = 1,
        PAYMENT = 2,
        RECEIPT = 3,

    }
    public class baseFinanceItemModel : baseViewModel
    {

        StoreCashModel baseModel;
        public StoreCashModel BaseModel { get => baseModel; set => baseModel = value; }

        public string IconColorCode { get; set; }
        public BillType BillType;

        private string typeIcon;
        public string TypeIcon { get => typeIcon; set { typeIcon = value; OnPropertyChanged(); } }
        private string billNote;
        public string BillNote { get => billNote; set { billNote = value; OnPropertyChanged(); } }
        private long? billValue;
        public long? BillValue { get => billValue; set { billValue = value; OnPropertyChanged(); } }

        protected int? stateID;
        public List<SolidColorBrush> orderOutlineColor;
        public List<SolidColorBrush> orderBackgroundColor;
        public List<SolidColorBrush> orderTextStateColor;

        protected SolidColorBrush outlineColor;
        protected SolidColorBrush backgroundColor;
        protected SolidColorBrush textStateColor;

        public virtual SolidColorBrush OutlineColor
        {
            get
            {
                outlineColor = orderOutlineColor[(int)stateID - 1];
                return outlineColor;
            }
            set
            {
                outlineColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush BackgroundColor
        {
            get
            {
                backgroundColor = orderBackgroundColor[(int)stateID - 1];
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush TextStateColor
        {
            get
            {
                textStateColor = orderOutlineColor[(int)stateID - 1];
                return textStateColor;
            }
            set
            {
                textStateColor = value;
                OnPropertyChanged();
            }
        }


        public baseFinanceItemModel(StoreCashModel m)
        {
            CreateColorAsset();

            this.BaseModel = m;

            this.billValue = Math.Abs((long)m.valueBill);

            this.typeIcon = m.valueBill < 0 ? FinanceReceiptIcon.PAYMENT : FinanceReceiptIcon.RECEIPT;
            this.IconColorCode = m.valueBill < 0 ? FinanceReceiptIcon.COLOR_PAYMENT : FinanceReceiptIcon.COLOR_RECEIPT;

            this.stateID = m.stateID;

            if(m.orderID == null)
            {
                this.BillType = m.valueBill < 0 ? BillType.PAYMENT : BillType.RECEIPT;
            }
            else
            {
                this.BillType = m.valueBill < 0 ? BillType.PAYMENT_FROM_ODER : BillType.RECEIPT_FROM_ORDER;
            }

            this.SetNote(m.detail);

        }

        protected virtual void CreateColorAsset()
        {
            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkGoldenrod,
                Brushes.DarkCyan,
                Brushes.DarkGreen
            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightYellow,
                Brushes.LightCyan,
                Brushes.LightGreen,
            };
        }

        protected virtual void SetNote(string orderNote)
        {
            switch (this.BillType)
            {
                case BillType.PAYMENT_FROM_ODER:
                    this.BillNote = string.Format("Chi tiền từ hóa đơn số {0}", this.baseModel.orderID);
                    break;
                case BillType.RECEIPT_FROM_ORDER:
                    this.BillNote = string.Format("Doanh thu từ hóa đơn số {0}", this.baseModel.orderID);
                    break;
                case BillType.PAYMENT:
                    this.BillNote = string.IsNullOrEmpty(this.baseModel.detail) ? "Không có mô tả" : this.baseModel.detail;
                    break;
                case BillType.RECEIPT:
                    this.BillNote = string.IsNullOrEmpty(this.baseModel.detail) ? "Không có mô tả" : this.baseModel.detail;
                    break;
            }
        }
    }
}
