﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace QLBH.Model
{
    public class QLBH_PersonItemModel : baseViewModel
    {
        protected int? stateID;

        public List<SolidColorBrush> orderOutlineColor;
        public List<SolidColorBrush> orderBackgroundColor;
        public List<SolidColorBrush> orderTextStateColor;

        protected SolidColorBrush outlineColor;
        protected SolidColorBrush backgroundColor;
        protected SolidColorBrush textStateColor;

        public virtual SolidColorBrush OutlineColor
        {
            get
            {
                outlineColor = orderOutlineColor[GetColorIndexById()];
                return outlineColor;
            }
            set
            {
                outlineColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush BackgroundColor
        {
            get
            {
                backgroundColor = orderBackgroundColor[GetColorIndexById()];
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush TextStateColor
        {
            get
            {
                textStateColor = orderOutlineColor[GetColorIndexById()];
                return textStateColor;
            }
            set
            {
                textStateColor = value;
                OnPropertyChanged();
            }
        }
        protected virtual int GetColorIndexById()
        {
            return 0;
        }

        public virtual void CreateaColorAsset()
        {
            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkCyan,
                Brushes.DarkGreen
            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightCyan,
                Brushes.LightGreen,
            };
        }
    }

    public class QLBH_SupplierItemModel : QLBH_PersonItemModel
    {
        protected SupplierModel baseModel;
        public SupplierModel BaseModel
        {
            get => baseModel; set
            {
                baseModel = value;
                OnPropertyChanged();
            }
        }
        public override void CreateaColorAsset()
        {
            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkGreen
            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightGreen,
            };
        }
        protected override int GetColorIndexById()
        {
            return (int)stateID - 2;
        }

        private long totalImportMoney;
        public long TotalImportMoney
        {
            get => totalImportMoney; set
            {
                totalImportMoney = value;
                OnPropertyChanged();
            }
        }

        public void ParseData(SupplierModel model)
        {
            this.BaseModel = model;
            this.stateID = model.supplierStateID;
            this.TotalImportMoney = DataProvider.DataController.Instance.GetTotalImportMoneySupplier(model.id);

        }

        public QLBH_SupplierItemModel(SupplierModel model)
        {
            this.ParseData(model);
            this.CreateaColorAsset();
        }
    }

    public class QLBH_CustomerItemModel : QLBH_PersonItemModel
    {
        protected CustomerModel baseModel;
        public CustomerModel BaseModel
        {
            get => baseModel; set
            {
                baseModel = value;
                OnPropertyChanged();
            }
        }

        public override void CreateaColorAsset()
        {
            this.orderOutlineColor = new List<SolidColorBrush>()
            {

            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {

            };
        }
        protected override int GetColorIndexById()
        {
            return (int)stateID - 2;
        }

        public void ParseData(CustomerModel model)
        {
            this.BaseModel = model;
            this.stateID = 0;
        }

        public QLBH_CustomerItemModel(CustomerModel model)
        {
            this.ParseData(model);
            this.CreateaColorAsset();
        }
    }

    public class QLBH_AccountItemModel : QLBH_PersonItemModel
    {
        protected AccountModel baseModel;
        public AccountModel BaseModel
        {
            get => baseModel; set
            {
                baseModel = value;
                OnPropertyChanged();
            }
        }

        public override void CreateaColorAsset()
        {

        }
        public void ParseData(AccountModel model)
        {
            this.BaseModel = model;

        }

        public QLBH_AccountItemModel(AccountModel model)
        {
            this.ParseData(model);
        }
    }
}
