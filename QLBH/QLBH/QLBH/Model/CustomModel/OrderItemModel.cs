﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLBH.Model
{
    //only show to display
    public class ProductDashBoardItemModel
    {
        private ProductModel baseProduct;
        public ProductModel BaseProduct { get => baseProduct; set => baseProduct = value; }

        private int currentStock;
        public int CurrentStock { get => currentStock; set => currentStock = value; }

        public ProductDashBoardItemModel(ProductModel p)
        {
            this.BaseProduct = p;
            this.currentStock = 0;
        }

        public ProductDashBoardItemModel(ProductModel p, int stock)
        {
            this.BaseProduct = p;
            this.currentStock = stock;
        }
    }
    //only show to display
    public class ProductAnnalyticsItemModel
    {
        private ProductModel baseModel;
        public ProductModel BaseModel { get => baseModel; set => baseModel = value; }

        private int totalSold;
        public int TotalSold { get => totalSold; set => totalSold = value; }

        private long totalRevenue;
        public long TotalRevenue { get => totalRevenue; set => totalRevenue = value; }

        private long totalProfit;
        public long TotalProfit { get => totalProfit; set => totalProfit = value; }

        public ProductAnnalyticsItemModel(ProductModel p)
        {
            this.BaseModel = p;
            this.TotalSold = 0;
            this.TotalRevenue = 0;
            this.TotalProfit = 0;
        }
        public ProductAnnalyticsItemModel(ProductModel p, int s, long r, long profit)
        {
            this.BaseModel = p;
            this.TotalSold = s;
            this.TotalRevenue = r;
            this.TotalProfit = profit;
        }
    }

    public class CategoryAnnalyticsItemModel
    {
        private CategoryModel baseModel;
        public CategoryModel BaseModel { get => baseModel; set => baseModel = value; }

        private long totalRevenue;
        public long TotalRevenue { get => totalRevenue; set => totalRevenue = value; }

        public CategoryAnnalyticsItemModel(CategoryModel p)
        {
            this.BaseModel = p;
            this.TotalRevenue = 0;
        }
        public CategoryAnnalyticsItemModel(CategoryModel p, long r)
        {
            this.BaseModel = p;
            this.TotalRevenue = r;
        }
    }


    public class OrderItemModel : baseViewModel
    {
        protected int? stt;
        protected string displayName;
        protected long? inputPrice;
        protected long? outputPrice;
        protected long? discountPrice;
        protected int? amount;
        protected long? totalPrice;
        protected int? preStock;

        public int? STT { get => stt; set { stt = value; OnPropertyChanged(); } }
        public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }
        public int? PreStock { get => preStock; set { preStock = value; OnPropertyChanged(); } }

        protected Action cbUpdateTotalOrder;
        public Action CbUpdateTotalOrder { get => cbUpdateTotalOrder; set => cbUpdateTotalOrder = value; }

        protected ProductModel productData;
        public ProductModel ProductData { get => productData; set => productData = value; }

        public virtual long? InputPrice { get => inputPrice;
            set
            {
                inputPrice = value;
                this.TotalPrice = (OutputPrice - InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public virtual long? OutputPrice { get => outputPrice;
            set
            {
                outputPrice = value;
                this.TotalPrice = (OutputPrice - InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public virtual long? DiscountPrice { get => discountPrice;
            set
            {
                discountPrice = value;
                this.TotalPrice = (OutputPrice - InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public virtual int? Amount { get => amount;
            set
            {
                amount = value;
                this.TotalPrice = (OutputPrice - InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public virtual long? TotalPrice { get => totalPrice;
            set
            {
                totalPrice = value;
                CbUpdateTotalOrder?.Invoke();
                OnPropertyChanged();
            }
        }


        public OrderItemModel(int? stt, string displayName, int? amount, ProductModel product)
        {
            this.STT = stt;
            this.DisplayName = displayName;
            this.Amount = amount;
            this.ProductData = product;

            this.InputPrice = 0;
            this.OutputPrice = 0;
            this.TotalPrice = 0;
            this.DiscountPrice = 0;

        }
        public virtual void SetPrice(long inPrice = 0, long outPrice = 0, long discount = 0)
        {
            this.InputPrice = inPrice;
            this.OutputPrice = outPrice;
            this.DiscountPrice = discount;
        }
        public virtual void SetPreStock(int? preStock)
        {
            this.PreStock = preStock;
        }
    }

    public class ImportOrderItemModel : OrderItemModel
    {
        public ImportOrderItemModel(int stt, string displayName, int amount, ProductModel product) : base( stt,  displayName,  amount, product)
        {

        }
        public ImportOrderItemModel(OrderItemModel baseItem) : base(baseItem.STT, baseItem.DisplayName, baseItem.Amount, baseItem.ProductData)
        {
            this.cbUpdateTotalOrder = baseItem.CbUpdateTotalOrder;
        }

        public override long? InputPrice
        {
            get => inputPrice;
            set
            {
                if (value < 0) value = 0;

                inputPrice = value;
                this.TotalPrice = (InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public override long? OutputPrice
        {
            get => outputPrice;
            set
            {
                outputPrice = value;
                this.TotalPrice = (InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public override long? DiscountPrice
        {
            get => discountPrice;
            set
            {
                discountPrice = value;
                this.TotalPrice = (InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

        public override int? Amount
        {
            get => amount;
            set
            {
                if (value < 0) value = 0;

                amount = value;
                this.TotalPrice = (InputPrice - DiscountPrice) * Amount;
                OnPropertyChanged();
            }
        }

    }

    public class ExportOrderItemModel : OrderItemModel
    {
        public ExportOrderItemModel(int stt, string displayName, int amount, ProductModel product) : base(stt, displayName, amount, product)
        {

        }
        public ExportOrderItemModel(OrderItemModel baseItem) : base(baseItem.STT, baseItem.DisplayName, baseItem.Amount, baseItem.ProductData)
        {
            this.cbUpdateTotalOrder = baseItem.CbUpdateTotalOrder;
            
        }

        public override long? OutputPrice
        {
            get => outputPrice;
            set
            {
                outputPrice = value;
                this.TotalPrice = (outputPrice - DiscountPrice) * Amount;
                this.cbUpdateTotalOrder?.Invoke();
                OnPropertyChanged();
            }
        }

        public override long? DiscountPrice
        {
            get => discountPrice;
            set
            {
                discountPrice = value;
                this.TotalPrice = (outputPrice - discountPrice) * Amount;
                this.cbUpdateTotalOrder?.Invoke();
                OnPropertyChanged();
            }
        }

        public override int? Amount
        {
            get => amount;
            set
            {
                if (value < 0) value = 0;

                amount = value;
                this.TotalPrice = (OutputPrice - DiscountPrice) * amount;
                this.cbUpdateTotalOrder?.Invoke();
                OnPropertyChanged();
            }
        }

    }

}
