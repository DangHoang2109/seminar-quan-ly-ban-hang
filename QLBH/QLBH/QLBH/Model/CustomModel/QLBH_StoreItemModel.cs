﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace QLBH.Model
{
    public class QLBH_StoreItemModel : baseViewModel
    {
        protected StoreModel baseModel;
        public StoreModel BaseModel
        {
            get => baseModel; set
            {
                baseModel = value;
                OnPropertyChanged();
            }
        }
        protected int? stateID;

        public List<SolidColorBrush> orderOutlineColor;
        public List<SolidColorBrush> orderBackgroundColor;
        public List<SolidColorBrush> orderTextStateColor;

        protected SolidColorBrush outlineColor;
        protected SolidColorBrush backgroundColor;
        protected SolidColorBrush textStateColor;

        public virtual SolidColorBrush OutlineColor
        {
            get
            {
                outlineColor = orderOutlineColor[GetColorIndexById()];
                return outlineColor;
            }
            set
            {
                outlineColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush BackgroundColor
        {
            get
            {
                backgroundColor = orderBackgroundColor[GetColorIndexById()];
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                OnPropertyChanged();
            }
        }
        public virtual SolidColorBrush TextStateColor
        {
            get
            {
                textStateColor = orderOutlineColor[GetColorIndexById()];
                return textStateColor;
            }
            set
            {
                textStateColor = value;
                OnPropertyChanged();
            }
        }
        protected virtual int GetColorIndexById()
        {
            return (int)stateID - 2;
        }

        public virtual void CreateaColorAsset()
        {
            this.orderOutlineColor = new List<SolidColorBrush>()
            {
                Brushes.Red,
                Brushes.DarkGreen,
                Brushes.DarkGoldenrod,

            };

            this.orderBackgroundColor = new List<SolidColorBrush>()
            {
                Brushes.LightPink,
                Brushes.LightGreen,
                Brushes.LightYellow

            };
        }

        public void ParseData(StoreModel model)
        {
            this.BaseModel = model;
            this.stateID = model.storeStateID;
        }

        public QLBH_StoreItemModel(StoreModel model)
        {
            this.ParseData(model);
            this.CreateaColorAsset();
        }
    }
}
