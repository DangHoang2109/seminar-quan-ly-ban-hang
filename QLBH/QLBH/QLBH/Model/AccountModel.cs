namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccountModel")]
    public partial class AccountModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccountModel()
        {
            ExportOrderModels = new HashSet<ExportOrderModel>();
            ImportOrderModels = new HashSet<ImportOrderModel>();
            StoreCashModels = new HashSet<StoreCashModel>();
            StoreInventoryModels = new HashSet<StoreInventoryModel>();
        }

        public int id { get; set; }

        public string displayName { get; set; }

        public string userPassword { get; set; }

        public string displayEmail { get; set; }

        public int? roleID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? startUsingDate { get; set; }

        public virtual RoleModel RoleModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExportOrderModel> ExportOrderModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportOrderModel> ImportOrderModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoreCashModel> StoreCashModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoreInventoryModel> StoreInventoryModels { get; set; }
    }
}
