namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SupplierModel")]
    public partial class SupplierModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SupplierModel()
        {
            ImportOrderModels = new HashSet<ImportOrderModel>();
        }

        public int id { get; set; }

        public string displayName { get; set; }

        public string displayAddress { get; set; }

        public string displayEmail { get; set; }

        public string displayPhone { get; set; }

        public string displayWebsite { get; set; }

        public long? totalPriceSale { get; set; }

        [Column(TypeName = "date")]
        public DateTime? startContractDate { get; set; }

        public int? supplierStateID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportOrderModel> ImportOrderModels { get; set; }

        public virtual SupplierState SupplierState { get; set; }
    }
}
