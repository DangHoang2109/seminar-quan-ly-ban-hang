namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ImportOrderInfoModel")]
    public partial class ImportOrderInfoModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ImportOrderInfoModel()
        {
            StoreInventoryModels = new HashSet<StoreInventoryModel>();
        }

        public int id { get; set; }

        public int? productID { get; set; }

        public int? amount { get; set; }

        public long? priceImport { get; set; }

        public int? importOrderID { get; set; }

        public long? realRetailPrice { get; set; }

        public virtual ImportOrderModel ImportOrderModel { get; set; }

        public virtual ProductModel ProductModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoreInventoryModel> StoreInventoryModels { get; set; }
    }
}
