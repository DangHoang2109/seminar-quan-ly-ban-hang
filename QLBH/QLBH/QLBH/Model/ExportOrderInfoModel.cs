namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExportOrderInfoModel")]
    public partial class ExportOrderInfoModel
    {
        public int id { get; set; }

        public int? amount { get; set; }

        public long? priceDiscount { get; set; }

        public int? storeInventoryID { get; set; }

        public int? exportOrderID { get; set; }

        public long? priceExport { get; set; }

        public int? productID { get; set; }

        public virtual ExportOrderModel ExportOrderModel { get; set; }

        public virtual ProductModel ProductModel { get; set; }

        public virtual StoreInventoryModel StoreInventoryModel { get; set; }
    }
}
