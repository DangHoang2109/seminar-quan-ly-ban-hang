namespace QLBH.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class QLBHEntity : DbContext
    {
        public QLBHEntity()
            : base("name=QLBHEntity")
        {
        }

        public virtual DbSet<AccountModel> AccountModels { get; set; }
        public virtual DbSet<CashFlowState> CashFlowStates { get; set; }
        public virtual DbSet<CategoryModel> CategoryModels { get; set; }
        public virtual DbSet<CustomerModel> CustomerModels { get; set; }
        public virtual DbSet<ExportOrderInfoModel> ExportOrderInfoModels { get; set; }
        public virtual DbSet<ExportOrderModel> ExportOrderModels { get; set; }
        public virtual DbSet<ExportOrderState> ExportOrderStates { get; set; }
        public virtual DbSet<ImportOrderInfoModel> ImportOrderInfoModels { get; set; }
        public virtual DbSet<ImportOrderModel> ImportOrderModels { get; set; }
        public virtual DbSet<ImportOrderState> ImportOrderStates { get; set; }
        public virtual DbSet<ProductModel> ProductModels { get; set; }
        public virtual DbSet<ProductState> ProductStates { get; set; }
        public virtual DbSet<RoleModel> RoleModels { get; set; }
        public virtual DbSet<StoreCashModel> StoreCashModels { get; set; }
        public virtual DbSet<StoreInventoryModel> StoreInventoryModels { get; set; }
        public virtual DbSet<StoreModel> StoreModels { get; set; }
        public virtual DbSet<StoreState> StoreStates { get; set; }
        public virtual DbSet<SupplierModel> SupplierModels { get; set; }
        public virtual DbSet<SupplierState> SupplierStates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountModel>()
                .HasMany(e => e.ExportOrderModels)
                .WithOptional(e => e.AccountModel)
                .HasForeignKey(e => e.creatorID);

            modelBuilder.Entity<AccountModel>()
                .HasMany(e => e.ImportOrderModels)
                .WithOptional(e => e.AccountModel)
                .HasForeignKey(e => e.creatorID);

            modelBuilder.Entity<AccountModel>()
                .HasMany(e => e.StoreCashModels)
                .WithOptional(e => e.AccountModel)
                .HasForeignKey(e => e.creatorID);

            modelBuilder.Entity<AccountModel>()
                .HasMany(e => e.StoreInventoryModels)
                .WithOptional(e => e.AccountModel)
                .HasForeignKey(e => e.creatorID);

            modelBuilder.Entity<CashFlowState>()
                .HasMany(e => e.StoreCashModels)
                .WithOptional(e => e.CashFlowState)
                .HasForeignKey(e => e.stateID);

            modelBuilder.Entity<CategoryModel>()
                .HasMany(e => e.ProductModels)
                .WithOptional(e => e.CategoryModel)
                .HasForeignKey(e => e.categoryID);

            modelBuilder.Entity<CustomerModel>()
                .HasMany(e => e.ExportOrderModels)
                .WithOptional(e => e.CustomerModel)
                .HasForeignKey(e => e.customerID);

            modelBuilder.Entity<ExportOrderModel>()
                .HasMany(e => e.ExportOrderInfoModels)
                .WithOptional(e => e.ExportOrderModel)
                .HasForeignKey(e => e.exportOrderID);

            modelBuilder.Entity<ExportOrderState>()
                .HasMany(e => e.ExportOrderModels)
                .WithOptional(e => e.ExportOrderState)
                .HasForeignKey(e => e.stateID);

            modelBuilder.Entity<ImportOrderInfoModel>()
                .HasMany(e => e.StoreInventoryModels)
                .WithOptional(e => e.ImportOrderInfoModel)
                .HasForeignKey(e => e.importOrderProductID);

            modelBuilder.Entity<ImportOrderModel>()
                .HasMany(e => e.ImportOrderInfoModels)
                .WithOptional(e => e.ImportOrderModel)
                .HasForeignKey(e => e.importOrderID);

            modelBuilder.Entity<ImportOrderState>()
                .HasMany(e => e.ImportOrderModels)
                .WithOptional(e => e.ImportOrderState)
                .HasForeignKey(e => e.stateID);

            modelBuilder.Entity<ProductModel>()
                .HasMany(e => e.ExportOrderInfoModels)
                .WithOptional(e => e.ProductModel)
                .HasForeignKey(e => e.productID);

            modelBuilder.Entity<ProductModel>()
                .HasMany(e => e.ImportOrderInfoModels)
                .WithOptional(e => e.ProductModel)
                .HasForeignKey(e => e.productID);

            modelBuilder.Entity<ProductState>()
                .HasMany(e => e.ProductModels)
                .WithOptional(e => e.ProductState)
                .HasForeignKey(e => e.stateID);

            modelBuilder.Entity<RoleModel>()
                .HasMany(e => e.AccountModels)
                .WithOptional(e => e.RoleModel)
                .HasForeignKey(e => e.roleID);

            modelBuilder.Entity<StoreInventoryModel>()
                .HasMany(e => e.ExportOrderInfoModels)
                .WithOptional(e => e.StoreInventoryModel)
                .HasForeignKey(e => e.storeInventoryID);

            modelBuilder.Entity<StoreModel>()
                .HasMany(e => e.ExportOrderModels)
                .WithOptional(e => e.StoreModel)
                .HasForeignKey(e => e.storeID);

            modelBuilder.Entity<StoreModel>()
                .HasMany(e => e.ImportOrderModels)
                .WithOptional(e => e.StoreModel)
                .HasForeignKey(e => e.storeID);

            modelBuilder.Entity<StoreModel>()
                .HasMany(e => e.StoreCashModels)
                .WithOptional(e => e.StoreModel)
                .HasForeignKey(e => e.storeID);

            modelBuilder.Entity<StoreModel>()
                .HasMany(e => e.StoreInventoryModels)
                .WithOptional(e => e.StoreModel)
                .HasForeignKey(e => e.storeID);

            modelBuilder.Entity<SupplierModel>()
                .HasMany(e => e.ImportOrderModels)
                .WithOptional(e => e.SupplierModel)
                .HasForeignKey(e => e.supplierID);
        }
    }
}
