namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StoreModel")]
    public partial class StoreModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StoreModel()
        {
            ExportOrderModels = new HashSet<ExportOrderModel>();
            ImportOrderModels = new HashSet<ImportOrderModel>();
            StoreCashModels = new HashSet<StoreCashModel>();
            StoreInventoryModels = new HashSet<StoreInventoryModel>();
        }

        public int id { get; set; }

        public string displayName { get; set; }

        public string displayAddress { get; set; }

        public long? totalMoneyinStore { get; set; }

        public string displayWebsite { get; set; }

        public int? storeStateID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExportOrderModel> ExportOrderModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportOrderModel> ImportOrderModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoreCashModel> StoreCashModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoreInventoryModel> StoreInventoryModels { get; set; }

        public virtual StoreState StoreState { get; set; }
    }
}
