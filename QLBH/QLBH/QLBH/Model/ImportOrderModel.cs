namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ImportOrderModel")]
    public partial class ImportOrderModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ImportOrderModel()
        {
            ImportOrderInfoModels = new HashSet<ImportOrderInfoModel>();
        }

        public int id { get; set; }

        public int? supplierID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dateImport { get; set; }

        public long? shippingFee { get; set; }

        public int? storeID { get; set; }

        public int? stateID { get; set; }

        public string orderNote { get; set; }

        public int? creatorID { get; set; }

        public virtual AccountModel AccountModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportOrderInfoModel> ImportOrderInfoModels { get; set; }

        public virtual ImportOrderState ImportOrderState { get; set; }

        public virtual StoreModel StoreModel { get; set; }

        public virtual SupplierModel SupplierModel { get; set; }
    }
}
