namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerModel")]
    public partial class CustomerModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CustomerModel()
        {
            ExportOrderModels = new HashSet<ExportOrderModel>();
        }

        public int id { get; set; }

        public string displayName { get; set; }

        public string displayAddress { get; set; }

        public string displayEmail { get; set; }

        public string displayPhone { get; set; }

        public long? totalBuyMoney { get; set; }

        public long? totalDebtMoney { get; set; }

        [Column(TypeName = "date")]
        public DateTime? startBuyingDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExportOrderModel> ExportOrderModels { get; set; }
    }
}
