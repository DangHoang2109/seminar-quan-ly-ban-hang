namespace QLBH.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductModel")]
    public partial class ProductModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductModel()
        {
            ExportOrderInfoModels = new HashSet<ExportOrderInfoModel>();
            ImportOrderInfoModels = new HashSet<ImportOrderInfoModel>();
        }

        public int id { get; set; }

        public string displayName { get; set; }

        public int? categoryID { get; set; }

        public int? stateID { get; set; }

        public long? recommendRetailPrice { get; set; }

        public string SKU { get; set; }

        public int? recommendStock { get; set; }

        public int? weight { get; set; }

        public virtual CategoryModel CategoryModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExportOrderInfoModel> ExportOrderInfoModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportOrderInfoModel> ImportOrderInfoModels { get; set; }

        public virtual ProductState ProductState { get; set; }
    }
}
