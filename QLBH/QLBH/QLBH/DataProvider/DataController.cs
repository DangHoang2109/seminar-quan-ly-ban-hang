﻿using QLBH.Model;
using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace QLBH.DataProvider
{
    public class DataDefine
    {
        public static int DELETED_STATE_ID = 1;
    }
    public class DataAccountErrorDefine
    {
        public const int SUCCESS = 0;
        public const int ACCOUNT_NOT_EXIST = 1;
        public const int WRONG_PASS = 2;
    }
    public class DataRoleDefine
    {
        public const int STAFF = 3;
        public const int STORE_MANAGER = 2;
        public const int OWNER = 1;
        public const int SYSTEM_ADMIN = 4;
    }
    public class DataImportOrderDefine
    {
        public static int DRAFT_IMPORT_ORDER_STATE_ID = 2;
        public static int COMPLETED_STATE_ID = 5;
        public static int CONFIRMED_STATE_ID = 3;
    }
    public class DataExportOrderDefine
    {
        public static int DRAFT_EXPORT_ORDER_STATE_ID = 2;
        public static int COMPLETED_STATE_ID = 5;
        public static int DELIVERING_STATE_ID = 4;
        public static int CANCEL_STATE_ID = 1;
        public static int PREPARING_STATE_ID = 3;
        public static int DELETED_STATE_ID = 6;

        public const string PREFIX_EXPORT_ORDER = "Hóa đơn xuất kho ";
        public const string PREFIX_CORRECTLY_ORDER = "Hóa đơn bù trừ ";
        public const string PREFIX_SALE_ORDER = "Hóa đơn bán hàng ";

    }
    public class DataCashFlowDefine
    {
        public static string PREFIX_NOTE_ORDER = "Thanh toán cho phiếu nhập hàng";
        public static int CANCEL_STATE = 1;
        public static int PENDING_STATE = 3;
        public static int COMPLETE_STATE = 4;
    }

    class DataController
    {
        public QLBHEntity DB;

        private static DataController ins;
        public static DataController Instance
        {
            get
            {
                if (ins == null) ins = new DataController();
                return ins;
            }
            set
            {
                ins = value;
            }
        }
        private List<StoreInventoryModel> allStoreInventory;


        private DataController()
        {
            this.DB = new QLBHEntity();

        }

        public AccountModel CurrentUser => MainViewModel.Ins.Account;

        public List<ProductDataForExport> GetNearestImportOrderIDByProduct(int productID, int? stock)
        {
            List<ProductDataForExport> result = new List<ProductDataForExport>();
            //danh sách các phiếu nhập kho còn tồn
            List<StoreInventoryModel> storeInventoryModels = new List<StoreInventoryModel>(this.DB.StoreInventoryModels);
            //sort ds
            storeInventoryModels.Sort((x, y) => x.id.CompareTo(y.id));
            //compare tồn của phiếu cũ nhất với stock cần, 
            int? remaining = stock;
            int index = storeInventoryModels.Count - 1;
            
            while (remaining > 0)
            {
                if (storeInventoryModels[index].stockProduct == 0)
                {
                    index--;
                    continue;
                }

                ProductDataForExport newDataItem = new ProductDataForExport()
                {
                    productID = productID,
                    amount = remaining >= storeInventoryModels[index].stockProduct ? storeInventoryModels[index].stockProduct : remaining,
                    storeInventoryInfo = storeInventoryModels[index]
                };
                result.Add(newDataItem);

                storeInventoryModels[index].stockProduct -= newDataItem.amount;
                remaining -= newDataItem.amount;


                //if (storeInventoryModels[index].stockProduct <= 0) this.DB.StoreInventoryModels.Remove(storeInventoryModels[index]);
                index--;
            }

            return result;
        }

        /// <summary>
        /// lấy x sản phẩm này trong hóa đơn nhập kho cũ nhất -> hàng ở trong kho lâu nhất và vẫn còn tồn
        /// </summary>
        /// <param name="productID"> ID sản phẩm cần lấy</param>
        /// <returns>id của đơn nhập kho cũ nhất</returns>
        public List<ProductDataForExport> GetFarestImportOrderIDByProduct(int productID, int? stock)
        {
            List<ProductDataForExport> result = new List<ProductDataForExport>();
            //danh sách các phiếu nhập kho còn tồn
            List<StoreInventoryModel> storeInventoryModels = new List<StoreInventoryModel>(this.DB.StoreInventoryModels);
            //sort ds
            storeInventoryModels.Sort((x, y) => x.id.CompareTo(y.id));
            //compare tồn của phiếu cũ nhất với stock cần, 
            int? remaining = stock;
            int index = 0;
            while(remaining > 0)
            {
                if (storeInventoryModels[index].stockProduct == 0)
                {
                    index++;
                    continue;
                }

                ProductDataForExport newDataItem = new ProductDataForExport()
                {
                    productID = productID,
                    amount = remaining >= storeInventoryModels[index].stockProduct ? storeInventoryModels[index].stockProduct : remaining,
                    storeInventoryInfo = storeInventoryModels[index]
                };
                result.Add(newDataItem);

                storeInventoryModels[index].stockProduct -= newDataItem.amount;
                remaining -= newDataItem.amount;

                index++; //phải cộng index vì list storeinventory là 1 bản cache ra, ko hề xóa của bẻn này

            }

            return result;
        }

        public ImportOrderModel GetImportOrderByID(int id)
        {
            return this.DB.ImportOrderModels.Find(id);
        }
        public ProductModel GetProductByID(int id)
        {
            return this.DB.ProductModels.Find(id);
        }
        public SupplierModel GetSupplierByID(int id)
        {
            return this.DB.SupplierModels.Find(id);
        }
        public StoreModel GetStoreByID(int id)
        {
            return this.DB.StoreModels.Find(id);
        }
        public StoreCashModel GetCashBillByOrderID(int id, bool isReceiptType)
        {
            return this.DB.StoreCashModels
                .ToList().Find(x => x.orderID == id && (isReceiptType ? x.valueBill > 0 : x.valueBill < 0));
        }
        public StoreCashModel GetCashBillByBillID(int id)
        {
            return this.DB.StoreCashModels
                .ToList().Find(x => x.orderID == id);
        }

        public AccountModel GetAccountByUsername(string Username)
        {
            return this.DB.AccountModels
                .ToList().Find(x => x.displayName.Equals(Username));
        }
        public AccountModel GetAccountByID(int id)
        {
            return this.DB.AccountModels.Find(id);
        }
        public ExportOrderModel GetExportOrderByID(int id)
        {
            return this.DB.ExportOrderModels.Find(id);
        }
        public CustomerModel GetCustomerByID(int id)
        {
            return this.DB.CustomerModels.Find(id);
        }

        public int GetSoldProduct(int id)
        {
            int? res = this.DB.ExportOrderInfoModels
                .Where(x => x.productID == id && x.ExportOrderModel.stateID == DataExportOrderDefine.COMPLETED_STATE_ID)
                .Sum(x => x.amount);

            return res == null ? 0 : (int)res;
        }
        public long GetTotalImportMoneySupplier(int idSupplier)
        {
            long? res = this.DB.ImportOrderInfoModels
                .Where(x => x.ImportOrderModel.supplierID == idSupplier && x.ImportOrderModel.stateID == DataImportOrderDefine.COMPLETED_STATE_ID)
                .Sum(x => x.priceImport * x.amount);

            return res == null ? 0 : (long)res;
        }

        public int GetExportDummyID()
        {
            return DataInstance.Instance.data_Customer.ToList().Find(x => x.displayName.Equals("ExportToken")).id;
        }
        public int GetCustomerIDByPhone(string phone)
        {
            return DataInstance.Instance.data_Customer.ToList().Find(x => x.displayPhone.Equals(phone)).id;
        }

        public int? GetProductPrestock(int productID, int storeCheck)
        {
            this.allStoreInventory = new List<StoreInventoryModel>(this.DB.StoreInventoryModels);
            List<StoreInventoryModel> currentStoreInventory = new List<StoreInventoryModel>(this.allStoreInventory.FindAll(x => x.storeID == storeCheck));

            List<StoreInventoryModel> stockProduct = new List<StoreInventoryModel>(currentStoreInventory.FindAll(x => x.ImportOrderInfoModel.productID == productID));

            int? totalStock = 0;
            if (stockProduct == null || stockProduct.Count == 0) return totalStock; //actually 0

            for (int i = 0; i < stockProduct.Count; i++)
            {
                if(stockProduct[i].stockProduct != null) totalStock += stockProduct[i].stockProduct;
            }

            return totalStock;
        }
    }
    public class ProductDataForExport
    {
        public int? productID;
        public int? amount;
        public StoreInventoryModel storeInventoryInfo;

    }

    public class DataInstance : baseViewModel
    {
        public ObservableCollection<ProductModel> data_Product;
        public ObservableCollection<ProductState> data_ProductState;
        private ObservableCollection<QLBH_ProductItemModel> data_QLBHProduct;

        public ObservableCollection<StoreModel> data_Stores;
        public ObservableCollection<QLBH_StoreItemModel> data_QLBHStores;
        public ObservableCollection<StoreState> data_StoreState;

        public ObservableCollection<SupplierModel> data_Supplier;
        public ObservableCollection<QLBH_SupplierItemModel> data_QLBHSupplier;
        public ObservableCollection<SupplierState> data_SupplierState;

        public ObservableCollection<CustomerModel> data_Customer;
        public ObservableCollection<QLBH_CustomerItemModel> data_QLBHCustomer;

        public ObservableCollection<ImportOrderModel> data_ImportOrder;
        public ObservableCollection<QLBH_ImportOrderModel> Data_QLBHImportOrder
        {
            get => data_QLBHImportOrder;
            set
            {
                data_QLBHImportOrder = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<QLBH_ImportOrderModel> data_QLBHImportOrder;
        public ObservableCollection<ImportOrderState> data_ImportOrderState;

        public ObservableCollection<ExportOrderModel> data_ExportOrder;
        public ObservableCollection<QLBH_ExportOrderModel> Data_QLBHExportOrder
        {
            get => data_QLBHExportOrder;
            set
            {
                data_QLBHExportOrder = value;
                UpdateRetailData();
                OnPropertyChanged();
            }
        }
        private ObservableCollection<QLBH_ExportOrderModel> data_QLBHExportOrder;
        public ObservableCollection<QLBH_ExportOrderModel> Data_QLBHCorrectlyStockOrder
        {
            get => data_QLBHCorrectlyStockOrder;
            set
            {
                data_QLBHCorrectlyStockOrder = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<QLBH_ExportOrderModel> data_QLBHCorrectlyStockOrder;
        public ObservableCollection<QLBH_ExportOrderModel> Data_QLBHRetailOrder
        {
            get => data_QLBHRetailOrder;
            set
            {
                data_QLBHRetailOrder = value;
                DashboardViewModel.Ins.OnValueChangeData();
                AnnalyticsViewModel.Ins.CallChangeValue();
                OnPropertyChanged();
            }
        }
        private ObservableCollection<QLBH_ExportOrderModel> data_QLBHRetailOrder;
        public ObservableCollection<ExportOrderState> data_ExportOrderState;

        public ObservableCollection<ProductModel> Data_Product
        {
            get => data_Product;
            set
            {
                data_Product = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<QLBH_ProductItemModel> Data_QLBHProduct
        {
            get => data_QLBHProduct;
            set
            {
                data_QLBHProduct = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<CategoryModel> data_Category;

        public ObservableCollection<baseFinanceItemModel> data_QLBHCashBill;
        public ObservableCollection<StoreCashModel> data_CashBill;
        public ObservableCollection<CashFlowState> data_CashFlowState;

        public ObservableCollection<QLBH_AccountItemModel> data_QLBHAccount;
        public ObservableCollection<AccountModel> data_Account;
        public ObservableCollection<RoleModel> data_Role;

        private static DataInstance ins;
        public static DataInstance Instance
        {
            get
            {
                if (ins == null) ins = new DataInstance();
                return ins;
            }
            set
            {
                ins = value;
            }
        }

        public DataInstance()
        {
            QLBHEntity DB = DataController.Instance.DB;

            #region Product
            this.data_Product = new ObservableCollection<ProductModel>(DB.ProductModels);
            this.Data_QLBHProduct = new ObservableCollection<QLBH_ProductItemModel>();
            for (int i = data_Product.Count - 1; i >= 0; i--)
            {
                this.Data_QLBHProduct.Add(new QLBH_ProductItemModel(data_Product[i]));
            }
            this.data_ProductState = new ObservableCollection<ProductState>();
            foreach (ProductState state in DB.ProductStates)
            {
                if (state.id != DataDefine.DELETED_STATE_ID) this.data_ProductState.Add(state);
            }

            this.data_Category = new ObservableCollection<CategoryModel>(DB.CategoryModels);
            #endregion Product

            #region Stores
            this.data_Stores = new ObservableCollection<StoreModel>(DB.StoreModels);
            this.data_QLBHStores = new ObservableCollection<QLBH_StoreItemModel>();
            for (int i = data_Stores.Count - 1; i >= 0; i--)
            {
                this.data_QLBHStores.Add(new QLBH_StoreItemModel(data_Stores[i]));
            }
            this.data_StoreState = new ObservableCollection<StoreState>();
            foreach (StoreState state in DB.StoreStates)
            {
                if (state.id != DataDefine.DELETED_STATE_ID) this.data_StoreState.Add(state);
            }
            #endregion Stores

            #region Supplier
            this.data_Supplier = new ObservableCollection<SupplierModel>(DB.SupplierModels);
            this.data_QLBHSupplier = new ObservableCollection<QLBH_SupplierItemModel>();
            for (int i = data_Supplier.Count - 1; i >= 0; i--)
            {
                this.data_QLBHSupplier.Add(new QLBH_SupplierItemModel(data_Supplier[i]));
            }
            this.data_SupplierState = new ObservableCollection<SupplierState>();
            foreach (SupplierState state in DB.SupplierStates)
            {
                if (state.id != DataDefine.DELETED_STATE_ID) this.data_SupplierState.Add(state);
            }
            #endregion Supplier

            #region Customer
            this.data_Customer = new ObservableCollection<CustomerModel>(DB.CustomerModels);
            this.data_QLBHCustomer = new ObservableCollection<QLBH_CustomerItemModel>();
            for (int i = data_Customer.Count - 1; i >= 0; i--)
            {
                this.data_QLBHCustomer.Add(new QLBH_CustomerItemModel(data_Customer[i]));
            }
            #endregion Customer

            #region Import Order
            this.data_ImportOrder = new ObservableCollection<ImportOrderModel>(DB.ImportOrderModels);

            this.data_QLBHImportOrder = new ObservableCollection<QLBH_ImportOrderModel>();
            for (int i = data_ImportOrder.Count-1; i >= 0; i--)
            {
                this.data_QLBHImportOrder.Add(new QLBH_ImportOrderModel(data_ImportOrder[i]));
            }

            this.data_ImportOrderState = new ObservableCollection<ImportOrderState>(DB.ImportOrderStates);
            #endregion Import Order

            //về bản chất, phiếu xuất kho là các export order trong database gốc có reason export là "Hóa đơn xuất kho số"; các correctly order trong database gốc là "Hóa đơn bù trừ số"

            #region Export Order
            this.data_ExportOrder = new ObservableCollection<ExportOrderModel>(DB.ExportOrderModels);

            this.data_QLBHExportOrder = new ObservableCollection<QLBH_ExportOrderModel>();
            for (int i = data_ExportOrder.Count - 1; i >= 0; i--)
            {
                if(data_ExportOrder[i].reasonExport.StartsWith(DataExportOrderDefine.PREFIX_EXPORT_ORDER) && data_ExportOrder[i].stateID != DataExportOrderDefine.DELETED_STATE_ID)
                    this.data_QLBHExportOrder.Add(new QLBH_ExportOrderModel(data_ExportOrder[i]));
            }

            this.data_ExportOrderState = new ObservableCollection<ExportOrderState>(DB.ExportOrderStates);
            #endregion

            #region Correctly Stock Order

            #endregion

            #region Retail Order
            this.data_QLBHRetailOrder = new ObservableCollection<QLBH_ExportOrderModel>();
            UpdateRetailData();

            #endregion

            #region Bill Cash
            this.data_CashBill = new ObservableCollection<StoreCashModel>(DB.StoreCashModels);
            data_QLBHCashBill = new ObservableCollection<baseFinanceItemModel>();

            for (int i = data_CashBill.Count - 1; i >= 0; i--)
            {
                baseFinanceItemModel m = new baseFinanceItemModel(data_CashBill[i]);
                data_QLBHCashBill.Add(m);
            }

            this.data_CashFlowState = new ObservableCollection<CashFlowState>();
            foreach (CashFlowState state in DB.CashFlowStates)
            {
                this.data_CashFlowState.Add(state);
            }

            data_QLBHCashBill.ToList().Sort((x, y) => DateTime.Compare((DateTime)y.BaseModel.dateCreate, (DateTime)x.BaseModel.dateCreate));
            #endregion

            #region Account
            this.data_Account = new ObservableCollection<AccountModel>(DB.AccountModels);
            this.data_Role = new ObservableCollection<RoleModel>(DB.RoleModels);
            this.data_QLBHAccount = new ObservableCollection<QLBH_AccountItemModel>();
            foreach(AccountModel a in data_Account)
            {
                this.data_QLBHAccount.Add(new QLBH_AccountItemModel(a));
            }
            #endregion
        }

        public void ChangeObCollection(ObservableCollection<QLBH_ExportOrderModel> c, IEnumerable<QLBH_ExportOrderModel> content)
        {
            c.Clear();
            foreach (QLBH_ExportOrderModel item in content)
            {
                c.Add(item);
            }
        }
        public void UpdateRetailData()
        {
            List<QLBH_ExportOrderModel> r = new List<QLBH_ExportOrderModel>();
            this.data_ExportOrder.Where(x => x.reasonExport.StartsWith(DataExportOrderDefine.PREFIX_SALE_ORDER) && x.stateID != DataExportOrderDefine.DELETED_STATE_ID)
                .ToList().ForEach(order =>
            {
                r.Add(new QLBH_ExportOrderModel(order));
            });
            this.ChangeObCollection(this.Data_QLBHRetailOrder, r);
        }
    }
}
