﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLBH.Model;
using QLBH.DataProvider;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using System.Collections.ObjectModel;
using System.Windows;
namespace QLBH.ViewModel
{
    public class AnnalyticIconDefine
    {
        public const string UPARROW = "ArrowUpBold";
        public const string DOWNARROW = "ArrowDownBold";
    }
    public class AnnalyticsViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdExportFile { get; set; }
        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private int displayRangeDay;
        public int DisplayRangeDay
        {
            get => displayRangeDay;
            set
            {
                displayRangeDay = value;
                OnPropertyChanged();
            }
        }

        private List<QLBH_ExportOrderModel> data_saleOrder;
        private List<CustomerModel> data_customers;

        #region Filtering
        private DateTime fromDate;
        public DateTime FromDate
        {
            get => fromDate;
            set
            {
                fromDate = value;
                this.UpdateSummaryMetric();
                OnPropertyChanged();
            }
        }

        private DateTime toDate;
        public DateTime ToDate
        {
            get => toDate;
            set
            {
                toDate = value;
                this.UpdateSummaryMetric();
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                this.UpdateSummaryMetric();
                OnPropertyChanged();
            }
        }
        #endregion

        #region KPI Metric

        //Doanh thu
        private long displayRevenue ;
        public long DisplayRevenue
        {
            get => displayRevenue;
            set
            {
                displayRevenue = value;
                OnPropertyChanged();
            }
        }

        private long displayRevenuePercent ;
        public long DisplayRevenuePercent
        {
            get => displayRevenuePercent;
            set
            {
                displayRevenuePercent = value;
                OnPropertyChanged();
            }
        }

        private string displayRevenueIcon;
        public string DisplayRevenueIcon
        {
            get => displayRevenueIcon;
            set
            {
                displayRevenueIcon = value;
                OnPropertyChanged();
            }
        }
        //Doanh thu

        //Lợi nhuận
        private long displayProfit;
        public long DisplayProfit
        {
            get => displayProfit;
            set
            {
                displayProfit = value;
                OnPropertyChanged();
            }
        }

        private long displayProfitPercent;
        public long DisplayProfitPercent
        {
            get => displayProfitPercent;
            set
            {
                displayProfitPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayProfitIcon;
        public string DisplayProfitIcon
        {
            get => displayProfitIcon;
            set
            {
                displayProfitIcon = value;
                OnPropertyChanged();
            }
        }
        //Lợi nhuận

        //Đơn hàng
        private long displayOrders;
        public long DisplayOrders
        {
            get => displayOrders;
            set
            {
                displayOrders = value;
                OnPropertyChanged();
            }
        }

        private long displayOrdersPercent;
        public long DisplayOrdersPercent
        {
            get => displayOrdersPercent;
            set
            {
                displayOrdersPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayOrdersIcon;
        public string DisplayOrdersIcon
        {
            get => displayOrdersIcon;
            set
            {
                displayOrdersIcon = value;
                OnPropertyChanged();
            }
        }
        //Đơn hàng

        //Khách hàng
        private long displayCustomer;
        public long DisplayCustomer
        {
            get => displayCustomer;
            set
            {
                displayCustomer = value;
                OnPropertyChanged();
            }
        }

        private long displayCustomerPercent;
        public long DisplayCustomerPercent
        {
            get => displayCustomerPercent;
            set
            {
                displayCustomerPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayCustomerIcon;
        public string DisplayCustomerIcon
        {
            get => displayCustomerIcon;
            set
            {
                displayCustomerIcon = value;
                OnPropertyChanged();
            }
        }
        //Khách hàng

        //Sản phẩm
        private long displaySoldProduct;
        public long DisplaySoldProduct
        {
            get => displaySoldProduct;
            set
            {
                displaySoldProduct = value;
                OnPropertyChanged();
            }
        }

        private long displaySoldProductPercent;
        public long DisplaySoldProductPercent
        {
            get => displaySoldProductPercent;
            set
            {
                displaySoldProductPercent = value;
                OnPropertyChanged();
            }
        }

        private string displaySoldProductIcon;
        public string DisplaySoldProductIcon
        {
            get => displaySoldProductIcon;
            set
            {
                displaySoldProductIcon = value;
                OnPropertyChanged();
            }
        }
        //Sản phẩm
        #endregion

        #region Customer Metric

        //Tổng khách hàng
        private long displayTotalCustomer;
        public long DisplayTotalCustomer
        {
            get => displayTotalCustomer;
            set
            {
                displayTotalCustomer = value;
                OnPropertyChanged();
            }
        }

        private long displayTotalCustomerPercent;
        public long DisplayTotalCustomerPercent
        {
            get => displayTotalCustomerPercent;
            set
            {
                displayTotalCustomerPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayTotalCustomerIcon;
        public string DisplayTotalCustomerIcon
        {
            get => displayTotalCustomerIcon;
            set
            {
                displayTotalCustomerIcon = value;
                OnPropertyChanged();
            }
        }
        //Tổng khách hàng

        //Khách hàng mới
        private long displayNewCustomer;
        public long DisplayNewCustomer
        {
            get => displayNewCustomer;
            set
            {
                displayNewCustomer = value;
                OnPropertyChanged();
            }
        }

        private long displayNewCustomerPercent;
        public long DisplayNewCustomerPercent
        {
            get => displayNewCustomerPercent;
            set
            {
                displayNewCustomerPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayNewCustomerIcon;
        public string DisplayNewCustomerIcon
        {
            get => displayNewCustomerIcon;
            set
            {
                displayNewCustomerIcon = value;
                OnPropertyChanged();
            }
        }
        //Khách hàng mới

        //Khách hàng cũ
        private long displayOldCustomer;
        public long DisplayOldCustomer
        {
            get => displayOldCustomer;
            set
            {
                displayOldCustomer = value;
                OnPropertyChanged();
            }
        }

        private long displayOldCustomerPercent;
        public long DisplayOldCustomerPercent
        {
            get => displayOldCustomerPercent;
            set
            {
                displayOldCustomerPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayOldCustomerIcon;
        public string DisplayOldCustomerIcon
        {
            get => displayOldCustomerIcon;
            set
            {
                displayOldCustomerIcon = value;
                OnPropertyChanged();
            }
        }
        //Khách hàng cũ

        //Tỉ lệ quay lại
        private long displayReturnCustomerPercent;
        public long DisplayReturnCustomerPercent
        {
            get => displayReturnCustomerPercent;
            set
            {
                displayReturnCustomerPercent = value;
                OnPropertyChanged();
            }
        }

        private long displayReturnCustomerPercentPercent;
        public long DisplayReturnCustomerPercentPercent
        {
            get => displayReturnCustomerPercentPercent;
            set
            {
                displayReturnCustomerPercentPercent = value;
                OnPropertyChanged();
            }
        }

        private string displayReturnCustomerPercentIcon;
        public string DisplayReturnCustomerPercentIcon
        {
            get => displayReturnCustomerPercentIcon;
            set
            {
                displayReturnCustomerPercentIcon = value;
                OnPropertyChanged();
            }
        }
        //Khách hàng
        #endregion

        #region Ranking Product Metric
        private int rankingProductTabIndex;
        public int RankingProductTabIndex
        {
            get => rankingProductTabIndex;
            set
            {
                rankingProductTabIndex = value;

                this.UpdateRankingProductMetric();
                OnPropertyChanged();
            }
        }

        private List<ProductAnnalyticsItemModel> data_RankingProducts;

        private ObservableCollection<ProductAnnalyticsItemModel> displayRankingProducts;
        public ObservableCollection<ProductAnnalyticsItemModel> DisplayRankingProducts
        {
            get => displayRankingProducts;
            set
            {
                displayRankingProducts = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Ranking Category Metric
        private int rankingCategoryTabIndex;
        public int RankingCategoryTabIndex
        {
            get => rankingCategoryTabIndex;
            set
            {
                rankingCategoryTabIndex = value;
                this.UpdateRankingCategoryMetric();
                OnPropertyChanged();
            }
        }

        private List<CategoryAnnalyticsItemModel> data_rankingCategory;

        private ObservableCollection<CategoryAnnalyticsItemModel> displayRankingCategory;
        public ObservableCollection<CategoryAnnalyticsItemModel> DisplayRankingCategory
        {
            get => displayRankingCategory;
            set
            {
                displayRankingCategory = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #endregion

        private static AnnalyticsViewModel ins;
        public static AnnalyticsViewModel Ins
        {
            get
            {

                if (ins == null) ins = new AnnalyticsViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public AnnalyticsViewModel()
        {
            PrepareData();

            PrepareComand();

        }

        private void PrepareComand()
        {
            cmdExportFile = new RelayCommand<object>((p) => { return false; }, (p) =>
            {
            });
        }

        private void PrepareData()
        {
            this.DisplayRankingProducts = new ObservableCollection<ProductAnnalyticsItemModel>();
            this.DisplayRankingCategory = new ObservableCollection<CategoryAnnalyticsItemModel>();

            this.data_saleOrder = new List<QLBH_ExportOrderModel>();
            this.data_customers = new List<CustomerModel>();
            this.data_RankingProducts = new List<ProductAnnalyticsItemModel>();
            this.data_rankingCategory = new List<CategoryAnnalyticsItemModel>();

            this.ListStore = DataInstance.Instance.data_Stores;
            //filtering
            this.SelectedStore = this.ListStore.FirstOrDefault();
            this.FromDate = DateTime.UtcNow;
            this.ToDate = DateTime.UtcNow;

            this.RankingProductTabIndex = 0;
            this.RankingCategoryTabIndex = 0;
        }

        #region Validation

        private bool IsFilterFieldFull()
        {
            return this.SelectedStore != null && this.FromDate != null && this.ToDate != null;
        }

        #endregion Validation

        #region Ulti
        public void ChangeObCollection(ObservableCollection<ProductAnnalyticsItemModel> c, IEnumerable<ProductAnnalyticsItemModel> content)
        {
            c.Clear();
            foreach (ProductAnnalyticsItemModel item in content)
            {
                c.Add(item);
            }
        }

        public void ChangeObCollection(ObservableCollection<CategoryAnnalyticsItemModel> c, IEnumerable<CategoryAnnalyticsItemModel> content)
        {
            c.Clear();
            foreach (CategoryAnnalyticsItemModel item in content)
            {
                c.Add(item);
            }
        }

        public string GetIconMetric(long value)
        {
            return value >= 0 ? AnnalyticIconDefine.UPARROW : AnnalyticIconDefine.DOWNARROW;
        }

        public void UpdateElementToRankingProductData(QLBH_ExportConverterDetailModel product)
        {
            ProductAnnalyticsItemModel model = this.data_RankingProducts.Find(x => x.BaseModel.id == product.BaseModel.ProductModel.id);
            if(model == null)
            {
                model = new ProductAnnalyticsItemModel(product.BaseModel.ProductModel);
                this.data_RankingProducts.Add(model);
            }

            model.TotalRevenue += (long)product.TotalRevenue;
            model.TotalSold += product.SumAmount;
            model.TotalProfit += (long)product.TotalProfit;
        }
        #endregion

        #region Set Data
        private void UpdateSummaryMetric()
        {
            if (!IsFilterFieldFull()) return;

            this.DisplayRangeDay = (int)this.ToDate.Subtract(this.FromDate).TotalDays;
            this.data_saleOrder = new List<QLBH_ExportOrderModel>(DataInstance.Instance.Data_QLBHRetailOrder.Where(x => AppUtil.IsDayBetwwenRange(x.BaseOrder.dateExport, this.FromDate, this.ToDate)));
            data_customers.Clear();
            data_saleOrder.ForEach((order) =>
            {
                data_customers.Add(order.BaseOrder.CustomerModel);

                order.DetailConverted.ToList().ForEach((pItem) =>
                {
                    this.UpdateElementToRankingProductData(pItem);
                });
            });

            this.UpdateKPIMetric();
            this.UpdateCustomerMetric();
        }

        private void UpdateKPIMetric()
        {
            if (!IsFilterFieldFull()) return;

            this.DisplayRevenue = (long)this.data_saleOrder.Sum(x => x.TotalBill);
            this.DisplayRevenuePercent = 0;
            this.DisplayRevenueIcon = this.GetIconMetric(DisplayRevenuePercent);

            this.DisplayProfit = (long)this.data_saleOrder.Sum(x => x.TotalProfit);
            this.DisplayProfitPercent = 0;
            this.DisplayProfitIcon = this.GetIconMetric(DisplayProfitPercent);

            this.DisplayOrders = (long)this.data_saleOrder.Count;
            this.DisplayOrdersPercent = 0;
            this.DisplayOrdersIcon = this.GetIconMetric(DisplayOrdersPercent);

            this.DisplayCustomer = this.data_saleOrder.Select(x => x.BaseOrder.customerID).AsParallel().Distinct().Count();
            this.DisplayCustomerPercent = 0;
            this.DisplayCustomerIcon = this.GetIconMetric(DisplayCustomerPercent);

            this.DisplaySoldProduct = (long)this.data_saleOrder.Sum(x => x.TotalProduct);
            this.DisplaySoldProductPercent = 0;
            this.DisplaySoldProductIcon = this.GetIconMetric(DisplaySoldProductPercent);
        }

        private void UpdateCustomerMetric()
        {
            if (!IsFilterFieldFull()) return;

            this.DisplayTotalCustomer = this.DisplayCustomer;
            this.DisplayTotalCustomerPercent = 0;
            this.DisplayTotalCustomerIcon = this.GetIconMetric(DisplayTotalCustomerPercent);

            this.DisplayNewCustomer = this.data_saleOrder.Select(x => x.BaseOrder.CustomerModel).AsParallel().Distinct().Where(y => AppUtil.IsDayBetwwenRange(y.startBuyingDate, FromDate, ToDate)).Count();
            this.DisplayNewCustomerPercent = 0;
            this.DisplayNewCustomerIcon = this.GetIconMetric(DisplayNewCustomerPercent);

            this.DisplayOldCustomer = DisplayTotalCustomer - DisplayNewCustomer;
            this.DisplayOldCustomerPercent = 0;
            this.DisplayOldCustomerIcon = this.GetIconMetric(DisplayOldCustomerPercent);

            this.DisplayReturnCustomerPercent = DisplayTotalCustomer == 0 ? 0 : (DisplayNewCustomer / DisplayTotalCustomer )*100;
            this.DisplayReturnCustomerPercentPercent = 0;
            this.DisplayReturnCustomerPercentIcon = this.GetIconMetric(DisplayReturnCustomerPercentPercent);
        }

        private void UpdateRankingProductMetric()
        {
            if (!IsFilterFieldFull()) return;

            switch (this.RankingProductTabIndex)
            {
                case 0: //rank by revenue
                    this.data_RankingProducts.Sort((x, y) => x.TotalRevenue.CompareTo(y.TotalRevenue));
                    break;
                case 1: //rank by profit
                    this.data_RankingProducts.Sort((x, y) => x.TotalSold.CompareTo(y.TotalSold));
                    break;
                case 2: //rank by solt
                    this.data_RankingProducts.Sort((x, y) => x.TotalSold.CompareTo(y.TotalSold));
                    break;
            }

            this.ChangeObCollection(this.DisplayRankingProducts, this.data_RankingProducts);

        }

        private void UpdateRankingCategoryMetric()
        {
            if (!IsFilterFieldFull()) return;

        }

        public void CallChangeValue()
        {
            this.UpdateSummaryMetric();
            this.UpdateRankingCategoryMetric();
            this.UpdateRankingProductMetric();
        }
        #endregion Set Data
    }
}
