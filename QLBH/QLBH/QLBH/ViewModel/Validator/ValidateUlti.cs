﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QLBH.ViewModel.Validator
{
    class ValidateUlti
    {
        private static ValidateUlti ins;
        public static ValidateUlti Ins
        {
            get
            {

                if (ins == null) ins = new ValidateUlti();
                return ins;
            }

            set { ins = value; }
        }

        public void ShowError(MaterialDesignThemes.Wpf.SnackbarMessageQueue snackbar, string error)
        {
            snackbar.Enqueue(error);
        }
        public void Validate_AmountProduct(object sender, System.Windows.RoutedEventArgs e)
        {
            if (ValidateNumber.Is_PositiveNumber((sender as TextBox).Text, false))
                Console.Write("input số lớn hơn 0");
        }


    }
    public class ValidateNumber
    {
        public static bool Is_PositiveNumber(string txt, bool isIncludeZero = true)
        {
            int r = 0;

            bool result = int.TryParse(txt, out r);
            if (!result) return false;

            if (isIncludeZero && r == 0) return true;

            return r > 0;

        }

    }
}
