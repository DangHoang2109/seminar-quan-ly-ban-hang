﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.DataProvider;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class RetailSaleOrderViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }
        public ICommand cmdAddDraftOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        public ICommand cmdSearchOrder { get; set; }
        public ICommand cmdFindCustomer { get; set; }

        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<ExportOrderState> orderState;
        public ObservableCollection<ExportOrderState> OrderState
        {
            get => orderState;
            set
            {
                orderState = value;
                OnPropertyChanged();
            }
        }
        private ExportOrderState selectedOrderState;
        public ExportOrderState SelectedOrderState
        {
            get => selectedOrderState;
            set
            {
                selectedOrderState = value;
                OnPropertyChanged();
            }
        }

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get => selectedTabIndex;
            set
            {
                selectedTabIndex = value;
                this.On_SwitchingTab();
                OnPropertyChanged();
            }
        }

        private Visibility visibility_SearchBarProduct;
        public Visibility Visibility_SearchBarProduct
        {
            get => visibility_SearchBarProduct;
            set
            {
                visibility_SearchBarProduct = value;
                OnPropertyChanged();
            }
        }
        private Visibility visibility_SearchBarOrderID;
        public Visibility Visibility_SearchBarOrderID
        {
            get => visibility_SearchBarOrderID;
            set
            {
                visibility_SearchBarOrderID = value;
                OnPropertyChanged();
            }
        }

        #region Order Management
        private ObservableCollection<QLBH_ExportOrderModel> listOrders;
        public ObservableCollection<QLBH_ExportOrderModel> ListOrders
        {
            get => listOrders;
            set
            {
                listOrders = value;
                OnPropertyChanged();
            }
        }

        private QLBH_ExportOrderModel selectedOrderInSearch;
        public QLBH_ExportOrderModel SelectedOrderInSearch
        {
            get => selectedOrderInSearch;
            set
            {
                selectedOrderInSearch = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            set
            {
                editingOrderID = value;
                OrderStateWindow = editingOrderID == -1 ? "Thêm Phiếu Mới" : "Sửa Phiếu Đã Có";
            }
        }

        private string orderStateWindow;
        public string OrderStateWindow
        {
            get
            {
                return orderStateWindow;
            }
            set
            {
                orderStateWindow = value;
                OnPropertyChanged();
            }
        }
        #endregion Order Management

        #region Add new Order
        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderItemModel> chosingProducts;
        public ObservableCollection<OrderItemModel> ChosingProducts
        {
            get => chosingProducts;
            set
            {
                chosingProducts = value;

                UpdateTotalOrderPrice();

                OnPropertyChanged();
            }
        }

        private string displayTotalOrderPrice;
        public string DisplayTotalOrderPrice { get => displayTotalOrderPrice; set { displayTotalOrderPrice = value; OnPropertyChanged(); } }

        private ProductModel selectedItemsInComboBox;
        public ProductModel SelectedItemsInComboBox
        {
            get => selectedItemsInComboBox;
            set
            {
                selectedItemsInComboBox = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                UpdateStockAllChosingProduct();
                OnPropertyChanged();
            }
        }

        private string displayImportOrderNote;
        public string DisplayImportOrderNote { get => displayImportOrderNote; set { displayImportOrderNote = value; OnPropertyChanged(); } }

        //phí bên nhận trả
        private long displayExtraFeeForReceiver;
        public long DisplayExtraFeeForReceiver
        {
            get => displayExtraFeeForReceiver;
            set
            {
                displayExtraFeeForReceiver = value;
                UpdateTotalOrderPrice();

                OnPropertyChanged();
            }
        }

        private string displayCustomerName;
        public string DisplayCustomerName { get => displayCustomerName; set { displayCustomerName = value; OnPropertyChanged(); } }

        private string displayCustomerPhone;
        public string DisplayCustomerPhone { get => displayCustomerPhone; set { displayCustomerPhone = value; OnPropertyChanged(); } }

        private string displayCustomerAddress;
        public string DisplayCustomerAddress { get => displayCustomerAddress; set { displayCustomerAddress = value; OnPropertyChanged(); } }

        private string displayCustomerEmail;
        public string DisplayCustomerEmail { get => displayCustomerEmail; set { displayCustomerEmail = value; OnPropertyChanged(); } }
        #endregion Add new Order

        #endregion

        private static RetailSaleOrderViewModel ins;
        public static RetailSaleOrderViewModel Ins
        {
            get
            {
                if (ins == null) ins = new RetailSaleOrderViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public RetailSaleOrderViewModel()
        {
            PrepareData();

            PrepareCommand();
        }

        private void PrepareCommand()
        {
            cmdAddProduct = new RelayCommand<object>((p) => { return IsCanEditListProduct && this.SelectedStore != null; }, (p) =>
            {
                On_AddProduct(this.SelectedItemsInComboBox);
            });

            cmdDeleteProduct = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                OrderItemModel removeOItem = (OrderItemModel)p;
                this.ChosingProducts.Remove(removeOItem);
                this.UpdateTotalOrderPrice();

            });

            cmdDeleteOrder = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                QLBH_ExportOrderModel qlbhOrder = p as QLBH_ExportOrderModel;
                if (qlbhOrder != null)
                {
                    int orderID = qlbhOrder.BaseOrder.id;

                    //đổi state về deleted 
                    QLBH_ExportOrderModel viewOrder = DataInstance.Instance.Data_QLBHRetailOrder.ToList().Find(x => x.BaseOrder.id == orderID);
                    DataInstance.Instance.Data_QLBHRetailOrder.Remove(viewOrder);

                    DataInstance.Instance.data_ExportOrder.ToList().Find(x => x.id == orderID).stateID = DataExportOrderDefine.DELETED_STATE_ID;

                    AppUtil.SaveData();
                }
            });

            cmdEditOrder = new RelayCommand<object>((p) => { return IsOrderCanEdit(p); }, (p) =>
            {
                //parse order data
                ParseOrderData(p);
                //switch tab
                this.SelectedTabIndex = 0;
            });

            cmdAddOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedOrderState != null; }, (p) =>
            {
                if (this.editingOrderID == -1) AddNewRetailOrder();
                else this.EditExistExportOrder();
            });

            cmdAddDraftOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedOrderState != null && this.SelectedOrderState.id == DataProvider.DataImportOrderDefine.DRAFT_IMPORT_ORDER_STATE_ID; }, (p) =>
            {
                AddNewRetailOrder();
            });

            cmdSearchOrder = new RelayCommand<object>((p) => { return SelectedOrderInSearch != null; }, (p) =>
            {
                //parse order data
                ParseOrderData(SelectedOrderInSearch);
                //switch tab
                this.SelectedTabIndex = 0;
            });

            cmdFindCustomer = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                this.On_FindCustomer();
            });
        }

        private void PrepareData()
        {
            this.products = DataInstance.Instance.data_Product;
            this.ChosingProducts = new ObservableCollection<OrderItemModel>();

            this.ListStore = DataInstance.Instance.data_Stores;
            this.SelectedStore = this.ListStore.FirstOrDefault();

            this.DisplayTotalOrderPrice = "0";
            this.SelectedTabIndex = 0;
            this.EditingOrderID = -1;

            this.OrderState = DataInstance.Instance.data_ExportOrderState;
            this.SelectedOrderState = this.OrderState.ToList().Find(x => x.id == DataExportOrderDefine.DRAFT_EXPORT_ORDER_STATE_ID);

            this.ListOrders = DataInstance.Instance.Data_QLBHRetailOrder;
            IsCanEditListProduct = true;
        }

        private bool IsProductCanEdit()
        {
            return this.SelectedOrderState.id < 4;
        }
        long? totalBillPrice;
        private bool isCanEditListProduct;
        public bool IsCanEditListProduct
        {
            get => isCanEditListProduct;
            set
            {
                isCanEditListProduct = value;
                OnPropertyChanged();
            }
        }
        private void AddNewCashOutOrder(ExportOrderModel newOrder)
        {
            if (this.SelectedOrderState.id >= DataExportOrderDefine.PREPARING_STATE_ID)
            {
                //tạo phiếu chi
                StoreCashModel bill = new StoreCashModel()
                {
                    detail = newOrder.orderNote,
                    dateCreate = DateTime.UtcNow,
                    valueBill = this.totalBillPrice + this.DisplayExtraFeeForReceiver,
                    storeID = newOrder.storeID,
                    stateID = newOrder.stateID == DataImportOrderDefine.COMPLETED_STATE_ID ? DataCashFlowDefine.COMPLETE_STATE : DataCashFlowDefine.PENDING_STATE,
                    orderID = newOrder.id,
                    creatorID = DataController.Instance.CurrentUser.id
                };
                DataController.Instance.DB.StoreCashModels.Add(bill);
                DataInstance.Instance.data_CashBill.Add(bill);
                DataInstance.Instance.data_QLBHCashBill.Insert(0, new baseFinanceItemModel(bill));

                StoreModel store = DataController.Instance.GetStoreByID((int)newOrder.storeID);
                store.totalMoneyinStore += bill.valueBill;

                AppUtil.SaveData();

            }
        }
        private void ParseOrderData(object p)
        {
            QLBH_ExportOrderModel editOrder = (QLBH_ExportOrderModel)p;
            ExportOrderModel baseOrder = editOrder.BaseOrder;

            this.EditingOrderID = baseOrder.id;
            //parse order data
            this.SelectedStore = baseOrder.StoreModel;
            this.DisplayImportOrderNote = baseOrder.orderNote;
            this.DisplayExtraFeeForReceiver = (long)baseOrder.extraFeeForReceiver;
            this.SelectedOrderState = baseOrder.ExportOrderState;
            this.DisplayCustomerAddress = baseOrder.CustomerModel.displayAddress;
            this.DisplayCustomerPhone = baseOrder.CustomerModel.displayPhone;
            this.DisplayCustomerName = baseOrder.CustomerModel.displayName;


            List<ExportOrderInfoModel> details = (baseOrder.ExportOrderInfoModels).ToList();
            this.ChosingProducts.Clear();
            for (int i = 0; i < details.Count; i++)
            {
                if (details[i].storeInventoryID == null)
                {
                    //đơn hủy parse lại, ko có inventory id
                    //add như 1 product bth 
                    this.On_AddProduct(details[i].ProductModel);
                    continue;
                }

                this.On_AddProduct(details[i].StoreInventoryModel.ImportOrderInfoModel.ProductModel);
                this.ChosingProducts[i].SetPrice(
                    inPrice: (long)details[i].StoreInventoryModel.ImportOrderInfoModel.priceImport,
                    outPrice: (long)details[i].priceExport,
                    discount: 0
                    );

                this.ChosingProducts[i].Amount = details[i].amount;
            }
        }

        private void UpdateStockAllChosingProduct()
        {
            foreach (OrderItemModel itemModel in this.ChosingProducts)
            {
                itemModel.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(itemModel.ProductData.id, this.SelectedStore.id));
            }
        }

        private bool IsOrderCanEdit(object p)
        {
            QLBH_ExportOrderModel editOrder = (QLBH_ExportOrderModel)p;
            ExportOrderModel baseOrder = editOrder.BaseOrder;

            return (baseOrder.stateID != 5);

        }

        public void EditExistExportOrder()
        {
            ExportOrderModel newOrder = DataController.Instance.GetExportOrderByID(this.editingOrderID);
            newOrder.orderNote = this.DisplayImportOrderNote;
            newOrder.storeID = this.SelectedStore.id;
            newOrder.extraFeeForReceiver = this.DisplayExtraFeeForReceiver;
            newOrder.stateID = this.SelectedOrderState.id;
            newOrder.customerID = DataController.Instance.GetCustomerIDByPhone(this.DisplayCustomerPhone);

            long oldBillPrice = 0;

            foreach (ExportOrderInfoModel info in newOrder.ExportOrderInfoModels.ToList())
            {
                //cộng lại tồn của những bill này trước rồi mới xóa
                if (info.storeInventoryID != null)
                    DataController.Instance.DB.StoreInventoryModels.Find(info.storeInventoryID).stockProduct += info.amount;

                DataController.Instance.DB.ExportOrderInfoModels.Remove(info);

                oldBillPrice += (long)(info.amount * (info.priceExport - info.priceDiscount));
            }
            //replace data instance

            ExportOrderModel instanceModel = DataInstance.Instance.data_ExportOrder.ToList().Find(x => x.id == newOrder.id);
            instanceModel.orderNote = this.DisplayImportOrderNote;
            instanceModel.storeID = this.SelectedStore.id;
            instanceModel.extraFeeForReceiver = this.DisplayExtraFeeForReceiver;
            instanceModel.stateID = this.SelectedOrderState.id;
            instanceModel.customerID = DataController.Instance.GetCustomerIDByPhone(this.DisplayCustomerPhone);

            int index = DataInstance.Instance.Data_QLBHRetailOrder.ToList().FindIndex(x => x.BaseOrder.id == newOrder.id);
            long newBill = AddOrderDetail(newOrder);

            DataInstance.Instance.Data_QLBHRetailOrder.RemoveAt(index);
            DataInstance.Instance.Data_QLBHRetailOrder.Insert(index, new QLBH_ExportOrderModel(newOrder));

            AppUtil.SaveData();

            if (this.SelectedOrderState.id >= DataExportOrderDefine.PREPARING_STATE_ID)
            {
                //tạo phiếu chi nếu trong data chưa có
                StoreCashModel bill = DataController.Instance.GetCashBillByOrderID(newOrder.id, true);
                if (bill == null)
                {
                    AddNewCashOutOrder(newOrder);
                }
                else
                {
                    if (bill.stateID == DataCashFlowDefine.PENDING_STATE && newOrder.stateID == DataImportOrderDefine.COMPLETED_STATE_ID)
                        bill.stateID = DataCashFlowDefine.COMPLETE_STATE;
                }
                AppUtil.SaveData();

            }

            this.BoundMessageQueue.Enqueue("Sửa hóa đơn bán lẻ thành công");

            CustomerModel customer = instanceModel.CustomerModel;
            customer.totalBuyMoney = customer.totalBuyMoney - oldBillPrice + newBill;

            ClearOrderData();
            this.EditingOrderID = -1;
        }

        public void UpdateTotalOrderPrice()
        {
            totalBillPrice = 0;
            for (int i = 0; i < this.chosingProducts.Count; i++)
            {
                totalBillPrice += this.chosingProducts[i].TotalPrice;
            }
            totalBillPrice += this.DisplayExtraFeeForReceiver;

            this.DisplayTotalOrderPrice = AppUtil.FormatMoneyDot(totalBillPrice);
        }

        public void AddNewRetailOrder()
        {
            //add customer nếu chưa có data, hàm ko làm gì nếu user đã từng mua
            CustomerModel customer = this.On_AddNewCustomer();

            ExportOrderModel newOrder = new ExportOrderModel()
            {
                customerID = customer.id,
                dateExport = DateTime.UtcNow,
                extraFeeForReceiver = this.DisplayExtraFeeForReceiver,
                storeID = this.SelectedStore.id,
                orderNote = this.DisplayImportOrderNote,
                reasonExport = string.Format("{1}{0}", DateTime.UtcNow.ToShortDateString(), DataExportOrderDefine.PREFIX_SALE_ORDER),
                stateID = this.SelectedOrderState.id,
                creatorID = DataController.Instance.CurrentUser.id,
                shippingFee = 0
            };

            DataController.Instance.DB.ExportOrderModels.Add(newOrder);
            DataInstance.Instance.data_ExportOrder.Add(newOrder);
            long totalBill = AddOrderDetail(newOrder);

            DataInstance.Instance.Data_QLBHRetailOrder.Insert(0, new QLBH_ExportOrderModel(newOrder));

            customer.totalBuyMoney += totalBill;

            AppUtil.SaveData();

            AddNewCashOutOrder(newOrder);

            this.BoundMessageQueue.Enqueue("Thêm hóa đơn bán lẻ thành công");
        }

        private long AddOrderDetail(ExportOrderModel newOrder)
        {
            long totalBill = 0;
            for (int i = 0; i < this.ChosingProducts.Count; i++)
            {
                if (this.SelectedOrderState.id == DataExportOrderDefine.CANCEL_STATE_ID)
                {
                    ExportOrderInfoModel newInfoItem = new ExportOrderInfoModel()
                    {
                        amount = ChosingProducts[i].Amount,
                        priceDiscount = ChosingProducts[i].DiscountPrice, //xuất kho ko có discount
                        exportOrderID = newOrder.id,
                        priceExport = this.ChosingProducts[i].OutputPrice,
                        storeInventoryID = null,
                        productID = ChosingProducts[i].ProductData.id
                    };
                    DataController.Instance.DB.ExportOrderInfoModels.Add(newInfoItem);
                    continue;
                }

                List<ProductDataForExport> export = DataController.Instance.GetFarestImportOrderIDByProduct(this.ChosingProducts[i].ProductData.id, this.ChosingProducts[i].Amount);
                if (export == null) continue;

                for (int ii = 0; ii < export.Count; ii++)
                {
                    ExportOrderInfoModel newInfoItem = new ExportOrderInfoModel()
                    {
                        amount = export[ii].amount,
                        priceDiscount = ChosingProducts[i].DiscountPrice, //xuất kho ko có discount
                        exportOrderID = newOrder.id,
                        priceExport = this.ChosingProducts[i].OutputPrice,
                        storeInventoryID = export[ii].storeInventoryInfo.id,
                        productID = ChosingProducts[i].ProductData.id
                    };
                    totalBill += (long)(newInfoItem.amount * (newInfoItem.priceExport - newInfoItem.priceDiscount));
                    DataController.Instance.DB.ExportOrderInfoModels.Add(newInfoItem);
                }
            }
            AppUtil.SaveData();
            return totalBill;
        }

        private void ClearOrderData()
        {
            this.ChosingProducts.Clear();

            DisplayImportOrderNote = "";
            DisplayExtraFeeForReceiver = 0;

        }

        public void On_SwitchingTab()
        {
            switch (this.SelectedTabIndex)
            {
                case 0:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
                case 1:
                    this.Visibility_SearchBarOrderID = Visibility.Visible;
                    this.Visibility_SearchBarProduct = Visibility.Collapsed;
                    break;
                default:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
            }
        }

        public void On_AddProduct(ProductModel p)
        {
            if (p == null) return;

            OrderItemModel item = this.ChosingProducts.ToList().Find(x => x.ProductData.id == p.id);
            if (item != null)
            {
                item.Amount += 1;
                return;
            }

            if (item == null || this.ChosingProducts.Count == 0)
            {
                ExportOrderItemModel newOEtem = new ExportOrderItemModel(
                        stt: this.ChosingProducts.Count + 1,
                        displayName: p.displayName,
                        amount: 1,
                        product: p);

                newOEtem.OutputPrice = p.recommendRetailPrice;

                newOEtem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
                newOEtem.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(p.id, this.SelectedStore.id));
                this.ChosingProducts.Add(newOEtem);
            }

            this.UpdateTotalOrderPrice();
        }

        public void On_FindCustomer()
        {
            List<CustomerModel> listCustomer = DataInstance.Instance.data_Customer.ToList();

            CustomerModel model = listCustomer.Find(x => x.displayPhone == this.DisplayCustomerPhone);
            if(model != null)
            {
                //khách cũ từng mua
                this.DisplayCustomerAddress = model.displayAddress;
                this.DisplayCustomerName = model.displayName;
                this.DisplayCustomerEmail = model.displayEmail;
            }
        }

        public CustomerModel On_AddNewCustomer()
        {
            List<CustomerModel> listCustomer = DataInstance.Instance.data_Customer.ToList();

            CustomerModel model = listCustomer.Find(x => x.displayPhone == this.DisplayCustomerPhone);

            if (model == null)
            {
                //khách mới thì add vào
                model = new CustomerModel()
                {
                    displayName = this.displayCustomerName,
                    displayAddress = this.displayCustomerAddress,
                    displayPhone = this.displayCustomerPhone,
                    displayEmail = this.displayCustomerEmail,
                    totalBuyMoney = this.totalBillPrice,
                    totalDebtMoney = 0,
                    startBuyingDate = DateTime.UtcNow
                };

                DataController.Instance.DB.CustomerModels.Add(model);
                DataInstance.Instance.data_Customer.Add(model);
                DataInstance.Instance.data_QLBHCustomer.Add(new QLBH_CustomerItemModel(model));

                AppUtil.SaveData();
            }

            return model;
        }
    }
}