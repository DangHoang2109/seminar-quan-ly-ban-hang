﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class RightSideMenuViewModel : baseViewModel
    {
        #region Command
        private static RightSideMenuViewModel ins;
        public static RightSideMenuViewModel Ins
        {
            get
            {
                if (ins == null) ins = new RightSideMenuViewModel();
                return ins;
            }
            set => ins = value;
        }

        public ICommand cmdPressBackWindowButton { get; set; }
        public ICommand cmdPressLogoutButton { get; set; }

        #endregion

        #region Variable
        private AccountModel account;
        public AccountModel Account
        {
            get => account;
            set
            {
                account = value;
                FrontWindowStateTitle = account == null ? "Đăng nhập" : "Cập nhật tài khoản";
                BackWindowStateTitle = account == null ? "Đăng nhập" : "Cập nhật tài khoản";

                OnPropertyChanged();
            }
        }

        private string frontWindowStateTitle;
        public string FrontWindowStateTitle
        {
            get => frontWindowStateTitle;
            set
            {
                frontWindowStateTitle = value;
                OnPropertyChanged();
            }
        }
        private string backWindowStateTitle;
        public string BackWindowStateTitle
        {
            get => backWindowStateTitle;
            set
            {
                backWindowStateTitle = value;
                OnPropertyChanged();
            }
        }

        private Visibility visibility_ConfirmPass;
        public Visibility Visibility_ConfirmPass
        {
            get => visibility_ConfirmPass;
            set
            {
                visibility_ConfirmPass = value;
                OnPropertyChanged();
            }
        }

        private string username;
        public string Username
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
            }
        }

        private string userPassword;
        public string UserPassword
        {
            get => userPassword;
            set
            {
                userPassword = value;
                OnPropertyChanged();
            }
        }

        private string confirmPassword;
        public string ConfirmPassword
        {
            get => confirmPassword;
            set
            {
                confirmPassword = value;
                OnPropertyChanged();
            }
        }

        private string userRole;
        public string UserRole
        {
            get => userRole;
            set
            {
                userRole = value;
                OnPropertyChanged();
            }
        }

        private bool isCanTypeUserName = true;
        public bool IsCanTypeUserName
        {
            get => isCanTypeUserName;
            set
            {
                isCanTypeUserName = value;
                OnPropertyChanged();
            }
        }

        private bool isLogined = true;
        public bool IsLogined
        {
            get => isLogined;
            set
            {
                isLogined = value;
                OnPropertyChanged();
            }
        }

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        #endregion

        public RightSideMenuViewModel()
        {
            //parse default User datat
            ParseDefaultUserData();

            cmdPressBackWindowButton = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                On_PressBackWindowButton();
            });

            cmdPressLogoutButton = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                OnClickLogout();
            });
        }
        public void ParseDefaultUserData()
        {
            Username = "";
            UserRole = "";
            UserPassword = "";
            ConfirmPassword = "";
            Visibility_ConfirmPass = Visibility.Collapsed;

            Account = null;
            IsCanTypeUserName = true;
            IsLogined = false;
        }
        public void ParseUserData(AccountModel account)
        {
            Username = account.displayName;
            UserPassword = account.userPassword;
            UserRole = account.RoleModel.displayName;
            Visibility_ConfirmPass = Visibility.Visible;

            Account = account;
            IsCanTypeUserName = false;

            OnFlipPop();
            IsLogined = true;
        }

        public void On_PressBackWindowButton()
        {

            if (!ValidateAccount()) return;

            if(this.account == null)
            {
                MainViewModel.Ins.On_Login(Username, UserPassword);
            }
            else
            {
                MainViewModel.Ins.On_EditAccount(Username, UserPassword);
            }
        }

        public void OnFlipPop()
        {
            Flipper.FlipCommand.Execute(null, null);
        }
        public void ShowNotify(string noti)
        {
            this.BoundMessageQueue.Enqueue(noti);
        }
        public bool ValidateAccount()
        {
            if (string.IsNullOrEmpty(Username))
            {
                this.ShowNotify("Tên đăng nhập không được rỗng");
                return false;
            }
            if (string.IsNullOrEmpty(UserPassword))
            {
                this.ShowNotify("Mật khẩu không được rỗng");
                return false;
            }
            if (Account != null && !UserPassword.Equals(ConfirmPassword))
            {
                this.ShowNotify("Mật khẩu xác thực không khớp");
                return false;
            }

            return true;

        }
        public void OnClickLogout()
        {
            this.ParseDefaultUserData();
            MainViewModel.Ins.On_Logout();
        }
    }
}
