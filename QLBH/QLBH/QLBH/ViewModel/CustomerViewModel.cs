﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class CustomerViewModel : baseViewModel
    {
        private static CustomerViewModel ins;
        public static CustomerViewModel Ins
        {
            get
            {
                if (ins == null) ins = new CustomerViewModel();
                return ins;
            }

            set { ins = value; }
        }


        private ObservableCollection<CustomerModel> customers;
        public ObservableCollection<CustomerModel> Customers
        {
            get => customers;
            set
            {
                customers = value;
                OnPropertyChanged();
            }
        }

        private bool isAddCustomerDialogOpen;
        public bool IsAddCustomerDialogOpen
        {
            get => isAddCustomerDialogOpen;
            set
            {
                isAddCustomerDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            get => editingOrderID;
            set
            {
                editingOrderID = value;
                ProductStateWindow = editingOrderID == -1 ? "Thêm Khách Hàng" : "Sửa Khách Hàng";
            }
        }
        private string productStateWindow;
        public string ProductStateWindow
        {
            get
            {
                return productStateWindow;
            }
            set
            {
                productStateWindow = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<QLBH_CustomerItemModel> customerConverted;
        public ObservableCollection<QLBH_CustomerItemModel> CustomerConverted
        {
            get => customerConverted;
            set
            {
                customerConverted = value;
                OnPropertyChanged();
            }
        }

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<CustomerModel> selectedCustomer;
        public ObservableCollection<CustomerModel> SelectedCustomer
        {
            get => selectedCustomer;
            set
            {
                selectedCustomer = value;
                OnPropertyChanged();
            }
        }




        #region Property

        #region Add Customer
        private string displayName;
        public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }


        private string displayAddress;
        public string DisplayAddress { get => displayAddress; set { displayAddress = value; OnPropertyChanged(); } }

        private string displayEmail;
        public string DisplayEmail { get => displayEmail; set { displayEmail = value; OnPropertyChanged(); } }

        private string displayPhone;
        public string DisplayPhone { get => displayPhone; set { displayPhone = value; OnPropertyChanged(); } }
        #endregion Add Customer

        #endregion

        #region Command
        public ICommand cmdLoadedCustomerDialog { get; set; }

        public ICommand cmdAddCustomer { get; set; }

        public ICommand cmdEditCustomer { get; set; }
        public ICommand cmdDeleteCustomer { get; set; }

        #endregion

        public CustomerViewModel()
        {
            this.Customers = DataProvider.DataInstance.Instance.data_Customer;
            this.CustomerConverted = DataProvider.DataInstance.Instance.data_QLBHCustomer;
            this.EditingOrderID = -1;

            //TODO: Khóa button nếu role user là staff trở xuống
            cmdLoadedCustomerDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                IsAddCustomerDialogOpen = true;
            });


            cmdAddCustomer = new RelayCommand<object>((p) => { return isFillAllField(); }, (p) =>
            {
                if (editingOrderID != -1)
                {
                    On_EditCustomer();
                    return;
                }

                On_AddCustomer();
            });

            cmdDeleteCustomer = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditCustomer = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //parse order data
                CustomerModel product = (p as QLBH_CustomerItemModel).BaseModel;
                this.EditingOrderID = product.id;

                if (product != null)
                {
                    this.DisplayName = product.displayName;
                    this.DisplayAddress = product.displayAddress;
                    this.DisplayEmail = product.displayEmail;
                    this.DisplayPhone = product.displayPhone;

                }
                //switch tab
                this.IsAddCustomerDialogOpen = true;
            });
        }

        public void On_AddCustomer()
        {
            var pnewSupplier = new CustomerModel()
            {
                displayName = this.DisplayName,
                displayAddress = this.DisplayAddress,
                displayEmail = this.DisplayEmail,
                displayPhone = this.DisplayPhone,
                totalBuyMoney = 0,
                totalDebtMoney = 0,
                startBuyingDate = DateTime.UtcNow,
            };

            DataProvider.DataController.Instance.DB.CustomerModels.Add(pnewSupplier);
            AppUtil.SaveData();

            DataProvider.DataInstance.Instance.data_Customer.Add(pnewSupplier);
            DataProvider.DataInstance.Instance.data_QLBHCustomer.Insert(0,new QLBH_CustomerItemModel(pnewSupplier));

            IsAddCustomerDialogOpen = false;

            this.BoundMessageQueue.Enqueue("Thêm khách hàng thành công");
        }
        public void On_EditCustomer()
        {
            CustomerModel product = DataProvider.DataController.Instance.GetCustomerByID(this.editingOrderID);
            product.displayName = this.displayName;
            product.displayAddress = this.displayAddress;
            product.displayEmail = this.displayEmail;
            product.displayPhone = this.displayPhone;

            //replace data instance

            CustomerModel instanceModel = DataProvider.DataInstance.Instance.data_Customer.ToList().Find(x => x.id == product.id);
            instanceModel.displayName = this.displayName;
            instanceModel.displayAddress = this.displayAddress;
            instanceModel.displayEmail = this.displayEmail;
            instanceModel.displayPhone = this.displayPhone;

            int index = DataProvider.DataInstance.Instance.data_QLBHCustomer.ToList().FindIndex(x => x.BaseModel.id == product.id);
            DataProvider.DataInstance.Instance.data_QLBHCustomer.RemoveAt(index);
            DataProvider.DataInstance.Instance.data_QLBHCustomer.Insert(index, new QLBH_CustomerItemModel(product));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Sửa NCC thành công");
            this.EditingOrderID = -1;
            IsAddCustomerDialogOpen = false;

        }
        public bool isFillAllField()
        {
            return
                (!string.IsNullOrEmpty(this.DisplayName) &&
                !string.IsNullOrEmpty(this.DisplayAddress) &&
                !string.IsNullOrEmpty(this.DisplayPhone));
        }
    }
}

