﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLBH
{
    public enum ListData
    {
        STORE = 0,
    }
    public class DataUpdater
    {
        private Dictionary<ListData, List<object>> DataList;

        private static DataUpdater ins;
        public static DataUpdater Ins
        {
            get
            {
                if (ins == null) ins = new DataUpdater();
                return ins;
            }

            set { ins = value; }
        }

        public DataUpdater()
        {
            this.DataList = new Dictionary<ListData, List<object>>();
        }

        public void RegisterDataList(ListData type, object observableCollection)
        {
            if (!this.DataList.ContainsKey(type)) this.DataList.Add(type, new List<object>());

            this.DataList[type].Add(observableCollection);
        }
        public void UnregisterDataList(ListData type, object observableCollection)
        {
            if (!this.DataList.ContainsKey(type)) return;

            this.DataList[type].Remove(observableCollection);
        }
        public void OnchangeValueDataList(ListData type, object newValue)
        {
            if (!this.DataList.ContainsKey(type)) return;

            for (int i = 0; i < this.DataList[type].Count; i++)
            {
                this.DataList[type][i] = newValue;
            }
        }
    }
}
