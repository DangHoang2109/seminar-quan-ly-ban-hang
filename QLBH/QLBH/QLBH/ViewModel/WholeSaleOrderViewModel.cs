﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class WholeSaleOrderViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }

        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get => selectedTabIndex;
            set
            {
                selectedTabIndex = value;
                this.On_SwitchingTab();
                OnPropertyChanged();
            }
        }

        #region Order Management

        #endregion Order Management

        #region Add new Order
        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderItemModel> chosingProducts;
        public ObservableCollection<OrderItemModel> ChosingProducts
        {
            get => chosingProducts;
            set
            {
                chosingProducts = value;

                UpdateTotalOrderPrice();

                OnPropertyChanged();
            }
        }

        private string displayTotalOrderPrice;
        public string DisplayTotalOrderPrice { get => displayTotalOrderPrice; set { displayTotalOrderPrice = value; OnPropertyChanged(); } }

        private ProductModel selectedItemsInComboBox;
        public ProductModel SelectedItemsInComboBox
        {
            get => selectedItemsInComboBox;
            set
            {
                selectedItemsInComboBox = value;
                OnPropertyChanged();
            }
        }

        //private string displayName;
        //public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                OnPropertyChanged();
            }
        }

        private string displayImportOrderNote;
        public string DisplayImportOrderNote { get => displayImportOrderNote; set { displayImportOrderNote = value; OnPropertyChanged(); } }

        /// <summary>
        /// Phí bên xuất trả -> bên ta trả
        /// </summary>
        private long displayExtraFee;
        public long DisplayExtraFee
        {
            get => displayExtraFee;
            set
            {
                displayExtraFee = value;
                UpdateTotalOrderPrice();
                OnPropertyChanged();
            }
        }

        //phí bên nhận trả
        private long displayExtraFeeForReceiver;
        public long DisplayExtraFeeForReceiver
        {
            get => displayExtraFeeForReceiver;
            set
            {
                displayExtraFeeForReceiver = value;
                OnPropertyChanged();
            }
        }
        #endregion Add new Order

        #endregion

        private static WholeSaleOrderViewModel ins;
        public static WholeSaleOrderViewModel Ins
        {
            get
            {
                if (ins == null) ins = new WholeSaleOrderViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public WholeSaleOrderViewModel()
        {
            this.products = DataProvider.DataInstance.Instance.data_Product;
            this.ChosingProducts = new ObservableCollection<OrderItemModel>();
            this.ListStore = DataProvider.DataInstance.Instance.data_Stores;
            this.DisplayTotalOrderPrice = "0";
            this.SelectedTabIndex = 0;

            cmdAddProduct = new RelayCommand<object>((p) => { return this.SelectedStore != null; }, (p) =>
            {
                On_AddProduct();
            });


            cmdDeleteProduct = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                OrderItemModel removeOItem = (OrderItemModel)p;
                this.ChosingProducts.Remove(removeOItem);
            });

            cmdAddOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null; }, (p) =>
            {
                AddNewExportOrder();
            });
        }

        public void UpdateTotalOrderPrice()
        {
            long? d = 0;
            for (int i = 0; i < this.chosingProducts.Count; i++)
            {
                d += this.chosingProducts[i].TotalPrice;
            }
            d -= this.DisplayExtraFee;

            this.DisplayTotalOrderPrice = AppUtil.FormatMoneyDot(d);
        }
        public void AddNewExportOrder()
        {
            ExportOrderModel newOrder = new ExportOrderModel()
            {
                customerID = DataProvider.DataController.Instance.GetExportDummyID(),
                dateExport = DateTime.UtcNow,
                shippingFee = this.DisplayExtraFee,
                extraFeeForReceiver = this.DisplayExtraFeeForReceiver,
                storeID = this.SelectedStore.id,
                orderNote = this.DisplayImportOrderNote,
                reasonExport = this.DisplayImportOrderNote,
                stateID = 5 //DONE
            };

            DataProvider.DataController.Instance.DB.ExportOrderModels.Add(newOrder);
            AppUtil.SaveData();


            for (int i = 0; i < this.ChosingProducts.Count; i++)
            {
                List<DataProvider.ProductDataForExport> export = DataProvider.DataController.Instance.GetFarestImportOrderIDByProduct(this.ChosingProducts[i].ProductData.id, this.ChosingProducts[i].Amount);
                if (export == null) continue;

                for (int ii = 0; ii < export.Count; ii++)
                {
                    ExportOrderInfoModel newInfoItem = new ExportOrderInfoModel()
                    {
                        amount = export[ii].amount,
                        priceDiscount = 0, //xuất kho ko có discount
                        exportOrderID = newOrder.id,
                        priceExport = this.ChosingProducts[i].OutputPrice,
                        storeInventoryID = export[ii].storeInventoryInfo.id
                    };
                    DataProvider.DataController.Instance.DB.ExportOrderInfoModels.Add(newInfoItem);
                }

            }

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Thêm hóa đơn xuất kho thành công");
        }

        public void On_SwitchingTab()
        {

        }
        public void On_AddProduct()
        {
            ExportOrderItemModel newOEtem = new ExportOrderItemModel(
                stt: this.ChosingProducts.Count + 1,
                displayName: this.SelectedItemsInComboBox.displayName,
                amount: 1,
                product: this.SelectedItemsInComboBox);

            newOEtem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
            newOEtem.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(this.SelectedItemsInComboBox.id, this.SelectedStore.id));
            this.ChosingProducts.Add(newOEtem);
        }


    }
}