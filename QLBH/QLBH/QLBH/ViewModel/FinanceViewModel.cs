﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLBH.Model;
using QLBH.DataProvider;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using System.Collections.ObjectModel;
using System.Windows;

namespace QLBH.ViewModel
{
    public class FinanceViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdOpenOrderDialog { get; set; }
        public ICommand cmdAddOrEditOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get => selectedTabIndex;
            set
            {
                selectedTabIndex = value;
                OnPropertyChanged();
            }
        }

        #region Order management
        private ObservableCollection<baseFinanceItemModel> listBills;
        public ObservableCollection<baseFinanceItemModel> ListBills
        {
            get => listBills;
            set
            {
                listBills = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<baseFinanceItemModel> listReceiptBills;
        public ObservableCollection<baseFinanceItemModel> ListReceiptBills
        {
            get => listReceiptBills;
            set
            {
                listReceiptBills = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<baseFinanceItemModel> listPaymentBills;
        public ObservableCollection<baseFinanceItemModel> ListPaymentBills
        {
            get => listPaymentBills;
            set
            {
                listPaymentBills = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Add new Order
        private int editingOrderID;
        public int EditingOrderID
        {
            set
            {
                editingOrderID = value;
                OrderStateWindow = editingOrderID == -1 ? "Thêm Phiếu Mới" : "Sửa Phiếu Đã Có";
            }
        }
        private string orderStateWindow;
        public string OrderStateWindow
        {
            get
            {
                return orderStateWindow;
            }
            set
            {
                orderStateWindow = value;
                OnPropertyChanged();
            }
        }

        private bool isAddOrderOpen;
        public bool IsAddOrderOpen
        {
            get => isAddOrderOpen;
            set
            {
                isAddOrderOpen = value;
                OnPropertyChanged();
            }
        }

        // Bill type
        private bool isCreatingInBill;
        public bool IsCreatingInBill
        {
            get => isCreatingInBill;
            set
            {
                isCreatingInBill = value;

                OnPropertyChanged();
            }
        }

        // Order State

        private ObservableCollection<CashFlowState> orderState;
        public ObservableCollection<CashFlowState> OrderState
        {
            get => orderState;
            set
            {
                orderState = value;
                OnPropertyChanged();
            }
        }

        private CashFlowState selectedOrderState;
        public CashFlowState SelectedOrderState
        {
            get => selectedOrderState;
            set
            {
                selectedOrderState = value;
                OnPropertyChanged();
            }
        }

        private StoreModel selectedCreateBillStore;
        public StoreModel SelectedCreateBillStore
        {
            get => selectedCreateBillStore;
            set
            {
                selectedCreateBillStore = value;
                OnPropertyChanged();
            }
        }

        //bill note
        private string displayBillNote;
        public string DisplayBillNote { get => displayBillNote; set { displayBillNote = value; OnPropertyChanged(); } }

        // value
        private long billValue;
        public long BillValue
        {
            get => billValue;
            set
            {
                billValue = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Metric
        private long currentCash;
        public long CurrentCash
        {
            get => currentCash;
            set
            {
                currentCash = value;
                OnPropertyChanged();
            }
        }

        //tổng tiền cần thanh toán
        private long pendingPaymentValue;
        public long PendingPaymentValue
        {
            get => pendingPaymentValue;
            set
            {
                pendingPaymentValue = value;
                OnPropertyChanged();
            }
        }

        //tổng tiền sắp nhận
        private long pendingReceiptValue;
        public long PendingReceiptValue
        {
            get => pendingReceiptValue;
            set
            {
                pendingReceiptValue = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Filtering
        private DateTime fromDate;
        public DateTime FromDate
        {
            get => fromDate;
            set
            {
                fromDate = value;
                CaculateMoneyMetric();
                OnPropertyChanged();
            }
        }

        private DateTime toDate;
        public DateTime ToDate
        {
            get => toDate;
            set
            {
                toDate = value;
                CaculateMoneyMetric();
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                CaculateMoneyMetric();

                OnPropertyChanged();
            }
        }
        #endregion

        #endregion

        private static FinanceViewModel ins;
        public static FinanceViewModel Ins
        {
            get
            {

                if (ins == null) ins = new FinanceViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public FinanceViewModel()
        {
            PrepareData();

            PrepareComand();

        }

        private void PrepareComand()
        {
            cmdOpenOrderDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                this.IsAddOrderOpen = true;
                IsCreatingInBill = true;
                this.EditingOrderID = -1;
            });
            cmdAddOrEditOrder = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                if (this.editingOrderID != -1)
                {
                    this.On_FinishEditOrder();
                }
                else this.On_FinishAddOrder();
            });

            cmdDeleteOrder = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditOrder = new RelayCommand<object>((p) => { return IsOrderCanEdit(p); }, (p) =>
            {
                this.On_ParseOrderData(p);
                this.IsAddOrderOpen = true;
            });
        }

        private void PrepareData()
        {
            this.ListPaymentBills = new ObservableCollection<baseFinanceItemModel>();
            this.ListReceiptBills = new ObservableCollection<baseFinanceItemModel>();
            this.ListBills = new ObservableCollection<baseFinanceItemModel>();

            this.SelectedTabIndex = 0;
            this.ListStore = DataInstance.Instance.data_Stores;
            this.OrderState = DataInstance.Instance.data_CashFlowState;
            //filtering
            this.SelectedStore = this.ListStore.FirstOrDefault();
            this.FromDate = DateTime.UtcNow;
            this.ToDate = DateTime.UtcNow;


        }

        public void ChangeObCollection(ObservableCollection<baseFinanceItemModel> c, IEnumerable<baseFinanceItemModel> content)
        {
            c.Clear();
            foreach (baseFinanceItemModel item in content)
            {
                c.Add(item);
            }
        }

        #region Validation
        private bool IsOrderCanEdit(object p)
        {
            baseFinanceItemModel baseBill = (baseFinanceItemModel)p;
            if (baseBill != null) return baseBill.BaseModel.orderID == null;

            return false;
        }
        
        #endregion Validation

        #region Set

        private void CaculateMoneyMetric()
        {
            if (this.SelectedStore == null || this.FromDate == null || this.ToDate == null) return;

            this.CurrentCash = (long)this.SelectedStore.totalMoneyinStore;

            this.PendingPaymentValue = (long)this.SelectedStore.StoreCashModels
                .Where(x => x.stateID == DataCashFlowDefine.PENDING_STATE && 
                            AppUtil.IsDayBetwwenRange(x.dateCreate, FromDate, ToDate) && 
                            x.valueBill < 0 && 
                            x.storeID == this.SelectedStore.id)
                .Sum(x => Math.Abs((long)x.valueBill));

            this.PendingReceiptValue = (long)this.SelectedStore.StoreCashModels
                .Where(x => x.stateID == DataCashFlowDefine.PENDING_STATE && 
                            AppUtil.IsDayBetwwenRange(x.dateCreate, FromDate, ToDate) && 
                            x.valueBill > 0 &&
                            x.storeID == this.SelectedStore.id)
                .Sum(x => (long)x.valueBill);

            UpdateListBill(FromDate, ToDate);

        }

        private void UpdateListBill(DateTime fromDate,DateTime toDate)
        {
            List<baseFinanceItemModel> rAll = new List<baseFinanceItemModel>(DataInstance.Instance.data_QLBHCashBill
                .Where(x => AppUtil.IsDayBetwwenRange(x.BaseModel.dateCreate, FromDate, ToDate) &&
                            x.BaseModel.storeID == this.SelectedStore.id));

            this.ChangeObCollection(this.ListBills, rAll);

            List<baseFinanceItemModel> rPay = new List<baseFinanceItemModel>(rAll
                .Where(x => (x.BillType == BillType.PAYMENT || 
                x.BillType == BillType.PAYMENT_FROM_ODER)));

            this.ChangeObCollection(this.ListPaymentBills, rPay);

            List<baseFinanceItemModel> rRe = new List<baseFinanceItemModel>(rAll
                .Where(x => (x.BillType == BillType.RECEIPT ||
                x.BillType == BillType.RECEIPT)));

            this.ChangeObCollection(this.ListReceiptBills, rRe);
        }

        #endregion Set

        #region Order
        private void On_ParseOrderData(object p)
        {
            baseFinanceItemModel baseOrder = (baseFinanceItemModel)p;

            this.IsCreatingInBill = baseOrder.BillType == BillType.RECEIPT || baseOrder.BillType == BillType.RECEIPT_FROM_ORDER;
            this.EditingOrderID = baseOrder.BaseModel.id;


            this.SelectedOrderState = baseOrder.BaseModel.CashFlowState;

            //parse order data
            this.SelectedStore = baseOrder.BaseModel.StoreModel;
            this.DisplayBillNote = baseOrder.BillNote;
            this.BillValue = Math.Abs((long)baseOrder.BillValue);
            this.SelectedOrderState = baseOrder.BaseModel.CashFlowState;
        }

        private void On_FinishAddOrder()
        {
            StoreCashModel bill = new StoreCashModel()
            {
                dateCreate = DateTime.UtcNow,
                detail = this.DisplayBillNote,
                valueBill = this.BillValue * (this.IsCreatingInBill ? 1 : -1),
                storeID = this.SelectedCreateBillStore.id,
                stateID = this.SelectedOrderState.id,
                orderID = null,
                creatorID = DataController.Instance.CurrentUser.id
            };
            DataController.Instance.DB.StoreCashModels.Add(bill);
            DataInstance.Instance.data_CashBill.Add(bill);
            DataInstance.Instance.data_QLBHCashBill.Insert(0, new baseFinanceItemModel(bill));

            if(bill.stateID == DataCashFlowDefine.COMPLETE_STATE)
                this.SelectedCreateBillStore.totalMoneyinStore += bill.valueBill;

            AppUtil.SaveData();

            CaculateMoneyMetric();

            this.EditingOrderID = -1;
            this.IsAddOrderOpen = false;
        }
        private void On_FinishEditOrder()
        {
            StoreCashModel bill = DataController.Instance.GetCashBillByBillID(this.editingOrderID);
            bill.detail = this.DisplayBillNote;
            bill.valueBill = this.BillValue * (this.IsCreatingInBill ? 1 : -1);
            bill.storeID = this.SelectedCreateBillStore.id;
            bill.stateID = this.SelectedOrderState.id;
            bill.creatorID = DataController.Instance.CurrentUser.id;

            this.EditingOrderID = -1;
        }

        #endregion Add and Edit order
    }
}
