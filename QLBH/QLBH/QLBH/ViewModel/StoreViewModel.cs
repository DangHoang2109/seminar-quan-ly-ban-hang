﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class StoreViewModel : baseViewModel
    {
        private static StoreViewModel ins;
        public static StoreViewModel Ins
        {
            get
            {
                if (ins == null) ins = new StoreViewModel();
                return ins;
            }

            set { ins = value; }
        }

        private ObservableCollection<StoreModel> stores;
        public ObservableCollection<StoreModel> Stores
        {
            get => stores;
            set
            {
                stores = value;
                OnPropertyChanged();
            }
        }

        private bool isAddStoreDialogOpen;
        public bool IsAddStoreDialogOpen
        {
            get => isAddStoreDialogOpen;
            set
            {
                isAddStoreDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            get => editingOrderID;
            set
            {
                editingOrderID = value;
                ProductStateWindow = editingOrderID == -1 ? "Thêm Cửa Hàng" : "Sửa Cửa Hàng";
            }
        }
        private string productStateWindow;
        public string ProductStateWindow
        {
            get
            {
                return productStateWindow;
            }
            set
            {
                productStateWindow = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<QLBH_StoreItemModel> storeConverted;
        public ObservableCollection<QLBH_StoreItemModel> StoreConverted
        {
            get => storeConverted;
            set
            {
                storeConverted = value;
                OnPropertyChanged();
            }
        }

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        #region Property
        #region Add Supplier Property
        private string displayName;
        public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }


        private string displayAddress;
        public string DisplayAddress { get => displayAddress; set { displayAddress = value; OnPropertyChanged(); } }

        private string displayWebsite;
        public string DisplayWebsite { get => displayWebsite; set { displayWebsite = value; OnPropertyChanged(); } }

        private ObservableCollection<StoreState> storeState;
        public ObservableCollection<StoreState> StoreState
        {
            get => storeState;
            set
            {
                storeState = value;
                OnPropertyChanged();
            }
        }

        private StoreState selectedStoreState;
        public StoreState SelectedStoreState
        {
            get => selectedStoreState;
            set
            {
                selectedStoreState = value;
                OnPropertyChanged();
            }
        }

        #endregion Add Supplier Property
        #endregion

        #region Command
        public ICommand cmdLoadedAddStoreDialog { get; set; }
        public ICommand cmdAddStore { get; set; }

        public ICommand cmdEditStore { get; set; }
        public ICommand cmdDeleteStore { get; set; }
        #endregion

        public StoreViewModel()
        {
            this.Stores = DataProvider.DataInstance.Instance.data_Stores;
            this.StoreConverted = DataProvider.DataInstance.Instance.data_QLBHStores;
            this.StoreState = DataProvider.DataInstance.Instance.data_StoreState;
            this.EditingOrderID = -1;
            cmdLoadedAddStoreDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                IsAddStoreDialogOpen = true;
            });

            cmdAddStore = new RelayCommand<object>((p) => { return isFillAllField(); }, (p) =>
            {
                if (editingOrderID != -1)
                {
                    On_EditStore();
                    return;
                }

                On_AddStore();
            });

            cmdDeleteStore = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditStore = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //parse order data
                StoreModel product = (p as QLBH_StoreItemModel).BaseModel;
                this.EditingOrderID = product.id;

                if (product != null)
                {
                    this.DisplayName = product.displayName;
                    this.DisplayAddress = product.displayAddress;
                    this.DisplayWebsite = product.displayWebsite;

                    this.SelectedStoreState = product.StoreState;
                }
                //switch tab
                this.IsAddStoreDialogOpen = true;
            });
        }

        public void On_AddStore()
        {
            var newStoreItem = new StoreModel()
            {
                displayName = this.DisplayName,
                displayAddress = this.DisplayAddress,
                displayWebsite = this.DisplayWebsite,
                totalMoneyinStore = 0,
                storeStateID = this.SelectedStoreState.id
            };

            DataProvider.DataController.Instance.DB.StoreModels.Add(newStoreItem);
            DataProvider.DataInstance.Instance.data_Stores.Add(newStoreItem);
            DataProvider.DataInstance.Instance.data_QLBHStores.Add(new QLBH_StoreItemModel(newStoreItem));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Thêm cửa hàng mới thành công");

            IsAddStoreDialogOpen = false;
        }
        public void On_EditStore()
        {
            StoreModel product = DataProvider.DataController.Instance.GetStoreByID(this.editingOrderID);
            product.displayName = this.displayName;
            product.displayAddress = this.displayAddress;
            product.displayWebsite = this.displayWebsite;
            product.storeStateID = this.selectedStoreState.id;

            //replace data instance

            StoreModel instanceModel = DataProvider.DataInstance.Instance.data_Stores.ToList().Find(x => x.id == product.id);
            instanceModel.displayName = this.displayName;
            instanceModel.displayAddress = this.displayAddress;
            instanceModel.displayWebsite = this.displayWebsite;
            instanceModel.storeStateID = this.selectedStoreState.id;

            int index = DataProvider.DataInstance.Instance.data_QLBHStores.ToList().FindIndex(x => x.BaseModel.id == product.id);
            DataProvider.DataInstance.Instance.data_QLBHStores.RemoveAt(index);
            DataProvider.DataInstance.Instance.data_QLBHStores.Insert(index, new QLBH_StoreItemModel(product));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Sửa cửa hàng thành công");
            this.EditingOrderID = -1;
            IsAddStoreDialogOpen = false;

        }
        public bool isFillAllField()
        {
            return
                (!string.IsNullOrEmpty(this.DisplayName) &&
                !string.IsNullOrEmpty(this.DisplayAddress) &&
                this.SelectedStoreState != null);
        }
    }
}
