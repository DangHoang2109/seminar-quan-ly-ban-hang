﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace QLBH.ViewModel
{
    public class ControlBarViewModel : baseViewModel
    {

        #region Command
        public ICommand cmdCloseWindow { get; set; }
        public ICommand cmdMinimizeWindow { get; set; }
        public ICommand cmdMaximizeWindow { get; set; }
        public ICommand cmdMouseMove { get; set; }
        public ICommand cmdShowMenu { get; set; }
        #endregion

        public ControlBarViewModel()
        {

            cmdShowMenu = new RelayCommand<UserControl>((p) => { return true; }, (p) =>
            {
                Window w = GetWondowParent(p) as Window;
                if (w != null) w.Close();

            });

            cmdCloseWindow = new RelayCommand<UserControl>((p) => { return p != null; }, (p) =>
            {
                Window w = GetWondowParent(p) as Window;
                if (w != null) w.Close();

            });
            cmdMinimizeWindow = new RelayCommand<UserControl>((p) => { return p != null; }, (p) =>
            {
                Window w = GetWondowParent(p) as Window;
                if (w != null)
                {
                    if (w.WindowState != WindowState.Minimized) w.WindowState = WindowState.Minimized;
                    else w.WindowState = WindowState.Normal;
                }

            });
            cmdMaximizeWindow = new RelayCommand<UserControl>((p) => { return p != null; }, (p) =>
            {
                Window w = GetWondowParent(p) as Window;
                if (w != null)
                {
                    if (w.WindowState != WindowState.Maximized) w.WindowState = WindowState.Maximized;
                    else w.WindowState = WindowState.Normal;
                }

            });
            cmdMouseMove = new RelayCommand<UserControl>((p) => { return p != null; }, (p) =>
            {
                Window w = GetWondowParent(p) as Window;
                if (w != null)
                {
                    w.DragMove();
                }

            });
        }

        private FrameworkElement GetWondowParent(UserControl p)
        {
            FrameworkElement r = p;

            while(r.Parent != null)
            {
                r = r.Parent as FrameworkElement;
            }

            return r;
        }
    }
}
