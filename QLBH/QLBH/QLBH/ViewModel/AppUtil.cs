﻿using QLBH.DataProvider;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLBH
{
    public class AppUtil
    {
        public static void SaveData()
        {
            try
            {
                DataController.Instance.DB.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        Console.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    }
                }
                Console.Write(ex.StackTrace);
            }
        }

        public static void ChangeObCollection(System.Collections.ObjectModel.ObservableCollection<object> c, IEnumerable<object> content) 
        {
            c.Clear();
            foreach (object item in content)
            {
                c.Add(item);
            }
        }

        public static string FormatMoney(long? num)
        {
            num = num == null ? 0 : num;
            string[] letters = { "K", "M", "B" };
            int letterLen = letters.Length;

            string s = num.ToString();
            int numlenght = s.Length;

            int i = (numlenght - 1) / 3;
            i = i > letterLen ? letterLen : i;

            if (i > 0)
            {
                if (numlenght - 3 * i + 1 < s.Length)
                    s = s.Remove(numlenght - 3 * i + 1);
                s = s.Insert(numlenght - 3 * i, ".");
                float f = float.Parse(s);
                s = f.ToString();
                s += letters[i - 1];
            }
            if (num < 0)
                s = "-" + s;
            return s;
        }
        public static string FormatMoneyDot(long? money, string separator = ".")
        {
            bool isNegative = false;
            money = money == null ? 0 : money;

            if (money < 0)
            {
                money = -money;
                isNegative = true;
            }

            string result = money.ToString();
            int index = result.Length - 1;
            int split = 0;
            while (index > 0)
            {
                split++;
                if (split % 3 == 0)
                {
                    result = result.Insert(index, "" + separator);
                    split = 0;
                }
                index--;
            }
            if (isNegative)
                result = "-" + result;
            return result;
        }
        public static bool IsDayBetwwenRange(DateTime? check, DateTime? from, DateTime? to)
        {
            return check.Value.Date <= to.Value.Date && check.Value.Date >= from.Value.Date;
        }
    }
}
