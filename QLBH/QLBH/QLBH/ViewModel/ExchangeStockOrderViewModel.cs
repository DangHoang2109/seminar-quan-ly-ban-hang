﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class ExchangeStockOrderViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }
        public ICommand cmdAddDraftOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        public ICommand cmdSearchOrder { get; set; }
        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<ExportOrderState> orderState;
        public ObservableCollection<ExportOrderState> OrderState
        {
            get => orderState;
            set
            {
                orderState = value;
                OnPropertyChanged();
            }
        }
        private ExportOrderState selectedOrderState;
        public ExportOrderState SelectedOrderState
        {
            get => selectedOrderState;
            set
            {
                selectedOrderState = value;
                OnPropertyChanged();
            }
        }

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get => selectedTabIndex;
            set
            {
                selectedTabIndex = value;
                this.On_SwitchingTab();
                OnPropertyChanged();
            }
        }

        private Visibility visibility_SearchBarProduct;
        public Visibility Visibility_SearchBarProduct
        {
            get => visibility_SearchBarProduct;
            set
            {
                visibility_SearchBarProduct = value;
                OnPropertyChanged();
            }
        }
        private Visibility visibility_SearchBarOrderID;
        public Visibility Visibility_SearchBarOrderID
        {
            get => visibility_SearchBarOrderID;
            set
            {
                visibility_SearchBarOrderID = value;
                OnPropertyChanged();
            }
        }

        #region Order Management
        private ObservableCollection<QLBH_ExportOrderModel> listOrders;
        public ObservableCollection<QLBH_ExportOrderModel> ListOrders
        {
            get => listOrders;
            set
            {
                listOrders = value;
                OnPropertyChanged();
            }
        }

        private QLBH_ExportOrderModel selectedOrderInSearch;
        public QLBH_ExportOrderModel SelectedOrderInSearch
        {
            get => selectedOrderInSearch;
            set
            {
                selectedOrderInSearch = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            set
            {
                editingOrderID = value;
                OrderStateWindow = editingOrderID == -1 ? "Thêm Phiếu Mới" : "Sửa Phiếu Đã Có";
            }
        }
        private string orderStateWindow;
        public string OrderStateWindow
        {
            get
            {
                return orderStateWindow;
            }
            set
            {
                orderStateWindow = value;
                OnPropertyChanged();
            }
        }
        #endregion Order Management

        #region Add new Order
        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderItemModel> chosingProducts;
        public ObservableCollection<OrderItemModel> ChosingProducts
        {
            get => chosingProducts;
            set
            {
                chosingProducts = value;
                OnPropertyChanged();
            }
        }

        private string displayTotalOrderPrice;
        public string DisplayTotalOrderPrice { get => displayTotalOrderPrice; set { displayTotalOrderPrice = value; OnPropertyChanged(); } }

        private ProductModel selectedItemsInComboBox;
        public ProductModel SelectedItemsInComboBox
        {
            get => selectedItemsInComboBox;
            set
            {
                selectedItemsInComboBox = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                UpdateStockAllChosingProduct();
                OnPropertyChanged();
            }
        }

        private string displayImportOrderNote;
        public string DisplayImportOrderNote { get => displayImportOrderNote; set { displayImportOrderNote = value; OnPropertyChanged(); } }

        /// <summary>
        /// Phí bên xuất trả -> bên ta trả
        /// </summary>
        private long displayExtraFee;
        public long DisplayExtraFee
        {
            get => displayExtraFee;
            set
            {
                displayExtraFee = value;
                OnPropertyChanged();
            }
        }

        //phí bên nhận trả
        private long displayExtraFeeForReceiver;
        public long DisplayExtraFeeForReceiver
        {
            get => displayExtraFeeForReceiver;
            set
            {
                displayExtraFeeForReceiver = value;
                OnPropertyChanged();
            }
        }
        #endregion Add new Order

        #endregion

        private static ExchangeStockOrderViewModel ins;
        public static ExchangeStockOrderViewModel Ins
        {
            get
            {
                if (ins == null) ins = new ExchangeStockOrderViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public ExchangeStockOrderViewModel()
        {
            this.products = DataProvider.DataInstance.Instance.data_Product;
            this.ChosingProducts = new ObservableCollection<OrderItemModel>();
            this.ListStore = DataProvider.DataInstance.Instance.data_Stores;

            cmdAddProduct = new RelayCommand<object>((p) => { return this.SelectedStore != null; }, (p) =>
            {
                On_AddProduct();
            });


            cmdDeleteProduct = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                OrderItemModel removeOItem = (OrderItemModel)p;
                this.ChosingProducts.Remove(removeOItem);
            });

            cmdAddOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null; }, (p) =>
            {

            });
        }
        private void UpdateStockAllChosingProduct()
        {
            foreach (OrderItemModel itemModel in this.ChosingProducts)
            {
                itemModel.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(itemModel.ProductData.id, this.SelectedStore.id));
            }
        }

        public void AddNewExchangeStockOrder()
        {
            //ImportOrderModel newOrder = new ImportOrderModel()
            //{
            //    supplierID = this.SelectedSupplier.id,
            //    dateImport = DateTime.UtcNow,
            //    shippingFee = this.DisplayExtraFee,
            //    storeID = this.SelectedStore.id,
            //    orderNote = this.DisplayImportOrderNote,

            //    stateID = 3 //SEND TO SUPPLIER
            //};

            //DataProvider.DataController.Instance.DB.ImportOrderModels.Add(newOrder);
            //AppUtil.SaveData();

            //for (int i = 0; i < this.ChosingProducts.Count; i++)
            //{
            //    ImportOrderInfoModel newInfoItem = new ImportOrderInfoModel()
            //    {
            //        productID = this.ChosingProducts[i].ProductData.id,
            //        amount = this.ChosingProducts[i].Amount,
            //        priceImport = this.ChosingProducts[i].InputPrice,
            //        realRetailPrice = this.ChosingProducts[i].OutputPrice,

            //        importOrderID = newOrder.id
            //    };
            //    DataProvider.DataController.Instance.DB.ImportOrderInfoModels.Add(newInfoItem);

            //    StoreInventoryModel newInventoryItem = new StoreInventoryModel()
            //    {
            //        storeID = this.SelectedStore.id,
            //        importOrderProductID = newInfoItem.id,
            //        stockProduct = this.ChosingProducts[i].Amount,
            //    };
            //    DataProvider.DataController.Instance.DB.StoreInventoryModels.Add(newInventoryItem);

            //    AppUtil.SaveData();

            //}


            //this.BoundMessageQueue.Enqueue("Thêm hóa đơn nhập kho thành công");
        }

        public void On_SwitchingTab()
        {

        }
        public void On_AddProduct()
        {
            if (this.SelectedItemsInComboBox != null)
            {

                //switch (this.SelectedTabIndex)
                //{
                //    case 0:
                //        ImportOrderItemModel newOItem = new ImportOrderItemModel(
                //            stt: this.ChosingProducts.Count + 1,
                //            displayName: this.SelectedItemsInComboBox.displayName,
                //            amount: 1,
                //            product: this.SelectedItemsInComboBox);
                //        newOItem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
                //        newOItem.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(this.SelectedItemsInComboBox.id, this.SelectedStore.id));
                //        this.ChosingProducts.Add(newOItem);
                //        break;
                //    case 1:
                //        ExportOrderItemModel newOEtem = new ExportOrderItemModel(
                //            stt: this.ChosingProducts.Count + 1,
                //            displayName: this.SelectedItemsInComboBox.displayName,
                //            amount: 1,
                //            product: this.SelectedItemsInComboBox);

                //        newOEtem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
                //        newOEtem.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(this.SelectedItemsInComboBox.id, this.SelectedStore.id));
                //        this.ChosingProducts.Add(newOEtem);
                //        break;
                //}

                //tìm prestock và set cho item

            }
        }


    }
}