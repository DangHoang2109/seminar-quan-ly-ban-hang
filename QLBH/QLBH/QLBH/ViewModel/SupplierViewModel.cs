﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class SupplierViewModel : baseViewModel
    {
        private static SupplierViewModel ins;
        public static SupplierViewModel Ins
        {
            get
            {
                if (ins == null) ins = new SupplierViewModel();
                return ins;
            }

            set { ins = value; }
        }

        private bool isAddSupplierDialogOpen;
        public bool IsAddSupplierDialogOpen
        {
            get => isAddSupplierDialogOpen;
            set
            {
                isAddSupplierDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            get => editingOrderID;
            set
            {
                editingOrderID = value;
                ProductStateWindow = editingOrderID == -1 ? "Thêm NCC" : "Sửa NCC";
            }
        }
        private string productStateWindow;
        public string ProductStateWindow
        {
            get
            {
                return productStateWindow;
            }
            set
            {
                productStateWindow = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<SupplierModel> suppliers;
        public ObservableCollection<SupplierModel> Suppliers
        {
            get => suppliers;
            set
            {
                suppliers = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<QLBH_SupplierItemModel> suppliersConverted;
        public ObservableCollection<QLBH_SupplierItemModel> SuppliersConverted
        {
            get => suppliersConverted;
            set
            {
                suppliersConverted = value;
                OnPropertyChanged();
            }
        }

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<SupplierModel> selectedSupplier;
        public ObservableCollection<SupplierModel> SelectedSupplier
        {
            get => selectedSupplier;
            set
            {
                selectedSupplier = value;
                OnPropertyChanged();
            }
        }



        #region Property

        #region Add Supplier Property
        private string displayName;
        public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }


        private string displayAddress;
        public string DisplayAddress { get => displayAddress; set { displayAddress = value; OnPropertyChanged(); } }

        private string displayEmail;
        public string DisplayEmail { get => displayEmail; set { displayEmail = value; OnPropertyChanged(); } }

        private string displayWebsite;
        public string DisplayWebsite { get => displayWebsite; set { displayWebsite = value; OnPropertyChanged(); } }

        private string displayPhone;
        public string DisplayPhone { get => displayPhone; set { displayPhone = value; OnPropertyChanged(); } }

        private ObservableCollection<SupplierState> suppplierState;
        public ObservableCollection<SupplierState> SuppplierState
        {
            get => suppplierState;
            set
            {
                suppplierState = value;
                OnPropertyChanged();
            }
        }

        private SupplierState selectedSupplierState;
        public SupplierState SelectedSupplierState
        {
            get => selectedSupplierState;
            set
            {
                selectedSupplierState = value;
                OnPropertyChanged();
            }
        }

        #endregion Add Supplier Property

        #endregion

        #region Command
        public ICommand cmdLoadedSupplierDialog { get; set; }

        public ICommand cmdAddSupplier { get; set; }

        public ICommand cmdEditSupplier { get; set; }
        public ICommand cmdDeleteSupplier { get; set; }
        #endregion

        public SupplierViewModel()
        {
            this.Suppliers = DataProvider.DataInstance.Instance.data_Supplier;
            this.SuppliersConverted = DataProvider.DataInstance.Instance.data_QLBHSupplier;
            this.SuppplierState = DataProvider.DataInstance.Instance.data_SupplierState;
            this.EditingOrderID = -1;

            //TODO: Khóa button nếu role user là staff trở xuống
            cmdLoadedSupplierDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                IsAddSupplierDialogOpen = true;
            });

            cmdAddSupplier = new RelayCommand<object>((p) => { return isFillAllField(); }, (p) =>
            {
                if(editingOrderID != -1)
                {
                    On_EditSupplier();
                    return;
                }

                On_AddSupplier();
            });

            cmdDeleteSupplier = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditSupplier = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //parse order data
                SupplierModel product = (p as QLBH_SupplierItemModel).BaseModel;
                this.EditingOrderID = product.id;

                if (product != null)
                {
                    this.DisplayName = product.displayName;
                    this.DisplayAddress = product.displayAddress;
                    this.DisplayEmail = product.displayEmail;
                    this.DisplayWebsite = product.displayWebsite;
                    this.DisplayPhone = product.displayPhone;

                    this.SelectedSupplierState = product.SupplierState;
                }
                //switch tab
                this.IsAddSupplierDialogOpen = true;
            });
        }

        public void On_AddSupplier()
        {
            var pnewSupplier = new SupplierModel()
            {
                displayName = this.DisplayName,
                displayAddress = this.DisplayAddress,
                displayEmail = this.DisplayEmail,
                displayPhone = this.DisplayPhone,
                displayWebsite = this.DisplayWebsite,
                startContractDate = DateTime.UtcNow,
                supplierStateID = this.SelectedSupplierState.id
            };

            DataProvider.DataController.Instance.DB.SupplierModels.Add(pnewSupplier);
            AppUtil.SaveData();

            DataProvider.DataInstance.Instance.data_Supplier.Add(pnewSupplier);
            DataProvider.DataInstance.Instance.data_QLBHSupplier.Insert(0, new QLBH_SupplierItemModel(pnewSupplier));

            IsAddSupplierDialogOpen = false;

            this.BoundMessageQueue.Enqueue("Thêm nhà cung cấp thành công");
        }
        public void On_EditSupplier()
        {
            SupplierModel product =  DataProvider.DataController.Instance.GetSupplierByID(this.editingOrderID);
            product.displayName = this.displayName;
            product.displayAddress = this.displayAddress;
            product.displayEmail = this.displayEmail;
            product.displayPhone = this.displayPhone;
            product.displayWebsite = this.displayWebsite;
            product.supplierStateID = this.SelectedSupplierState.id;

            //replace data instance

            SupplierModel instanceModel = DataProvider.DataInstance.Instance.data_Supplier.ToList().Find(x => x.id == product.id);
            instanceModel.displayName = this.displayName;
            instanceModel.displayAddress = this.displayAddress;
            instanceModel.displayEmail = this.displayEmail;
            instanceModel.displayPhone = this.displayPhone;
            instanceModel.displayWebsite = this.displayWebsite;
            instanceModel.supplierStateID = this.SelectedSupplierState.id;

            int index = DataProvider.DataInstance.Instance.data_QLBHSupplier.ToList().FindIndex(x => x.BaseModel.id == product.id);
            DataProvider.DataInstance.Instance.data_QLBHSupplier.RemoveAt(index);
            DataProvider.DataInstance.Instance.data_QLBHSupplier.Insert(index, new QLBH_SupplierItemModel(product));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Sửa NCC thành công");
            this.EditingOrderID = -1;
            IsAddSupplierDialogOpen = false;

        }
        public bool isFillAllField()
        {
            return
                (!string.IsNullOrEmpty(this.DisplayName) &&
                !string.IsNullOrEmpty(this.DisplayAddress) &&
                this.SelectedSupplierState != null);
        }
    }
}
