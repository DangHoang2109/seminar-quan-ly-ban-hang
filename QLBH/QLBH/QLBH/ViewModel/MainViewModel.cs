﻿using QLBH.DataProvider;
using QLBH.FolderUserControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public enum ViewWindow
    {
        DASHBOARD = 0,
        IM_EXORDER = 1,
        SALE_ORDER = 2,
        PRODUCT = 3,
        CASHFLOW = 4,
        ACCOUNT = 5,
        CUSTOMER = 6,
        SUPPLIER = 7,
        STORE = 8,
    }
    public class MainViewModel : baseViewModel
    {
        #region Variable

        #endregion

        private static MainViewModel ins;
        public static MainViewModel Ins {
            get
            {
                if (ins == null) ins = new MainViewModel();
                return ins;
            }
            set => ins = value; }

        private int currentTab;
        public int CurrentTab
        {
            get => currentTab;
            set
            {
                currentTab = value;
                OnPropertyChanged();
            }
        }

        private AccountModel account;
        public AccountModel Account
        {
            get => account;
            set
            {
                account = value;
                OnPropertyChanged();
            }
        }

        public int RoleID => (int)this.Account.roleID;


        #region Feature Validate
        private bool isAvailableUse_DashBoard;
        public bool IsAvailableUse_DashBoard
        {
            get => isAvailableUse_DashBoard;
            set
            {
                isAvailableUse_DashBoard = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_InOutOrder;
        public bool IsAvailableUse_InOutOrder
        {
            get => isAvailableUse_InOutOrder;
            set
            {
                isAvailableUse_InOutOrder = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_InOutOrder_ImportOrder;
        public bool IsAvailableUse_InOutOrder_ImportOrder
        {
            get => isAvailableUse_InOutOrder_ImportOrder;
            set
            {
                isAvailableUse_InOutOrder_ImportOrder = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_InOutOrder_ExportOrder;
        public bool IsAvailableUse_InOutOrder_ExportOrder
        {
            get => isAvailableUse_InOutOrder_ExportOrder;
            set
            {
                isAvailableUse_InOutOrder_ExportOrder = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_InOutOrder_ExchangetOrder;
        public bool IsAvailableUse_InOutOrder_ExchangetOrder
        {
            get => isAvailableUse_InOutOrder_ExchangetOrder;
            set
            {
                isAvailableUse_InOutOrder_ExchangetOrder = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_InOutOrder_CorrectlyOrder;
        public bool IsAvailableUse_InOutOrder_CorrectlyOrder
        {
            get => isAvailableUse_InOutOrder_CorrectlyOrder;
            set
            {
                isAvailableUse_InOutOrder_CorrectlyOrder = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Order;
        public bool IsAvailableUse_Order
        {
            get => isAvailableUse_Order;
            set
            {
                isAvailableUse_Order = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_Order_Retail;
        public bool IsAvailableUse_Order_Retail
        {
            get => isAvailableUse_Order_Retail;
            set
            {
                isAvailableUse_Order_Retail = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_Order_Wholesale;
        public bool IsAvailableUse_Order_Wholesale
        {
            get => isAvailableUse_Order_Wholesale;
            set
            {
                isAvailableUse_Order_Wholesale = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Finance;
        public bool IsAvailableUse_Finance
        {
            get => isAvailableUse_Finance;
            set
            {
                isAvailableUse_Finance = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_Finance_Cash;
        public bool IsAvailableUse_Finance_Cash
        {
            get => isAvailableUse_Finance_Cash;
            set
            {
                isAvailableUse_Finance_Cash = value;
                OnPropertyChanged();
            }
        }
        private bool isAvailableUse_Finance_Analytist;
        public bool IsAvailableUse_Finance_Analytist
        {
            get => isAvailableUse_Finance_Analytist;
            set
            {
                isAvailableUse_Finance_Analytist = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Account;
        public bool IsAvailableUse_Account
        {
            get => isAvailableUse_Account;
            set
            {
                isAvailableUse_Account = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Product;
        public bool IsAvailableUse_Product
        {
            get => isAvailableUse_Product;
            set
            {
                isAvailableUse_Product = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Customer;
        public bool IsAvailableUse_Customer
        {
            get => isAvailableUse_Customer;
            set
            {
                isAvailableUse_Customer = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Supplier;
        public bool IsAvailableUse_Supplier
        {
            get => isAvailableUse_Supplier;
            set
            {
                isAvailableUse_Supplier = value;
                OnPropertyChanged();
            }
        }

        private bool isAvailableUse_Store;
        public bool IsAvailableUse_Store
        {
            get => isAvailableUse_Store;
            set
            {
                isAvailableUse_Store = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public ICommand cmdLoadedWindow { get; set; }

        public bool isLoaded = false;

        public event Action<bool> CallbackLoginSuccess;
        
        public MainViewModel()
        {
            //InitWindowContentState();
            cmdLoadedWindow = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                isLoaded = true;
                //do event when load the window
            });
        }

        private FrameworkElement GetWondowParent(UserControl p)
        {
            FrameworkElement r = p;

            while (r.Parent != null)
            {
                r = r.Parent as FrameworkElement;
            }

            return r;
        }

        public void On_Login(string username, string pass)
        {
            //verify account
            AccountModel acc = DataController.Instance.GetAccountByUsername(username);
            if (acc == null)
            {
                this.OnLoginFail(DataAccountErrorDefine.ACCOUNT_NOT_EXIST);
                return;
            }

            if (!acc.userPassword.Equals(pass))
            {
                OnLoginFail(DataAccountErrorDefine.WRONG_PASS);
                return;
            }

            OnLoginSuccess(acc);
        }

        private void OnLoginSuccess(AccountModel account)
        {
            RightSideMenuViewModel.Ins.ParseUserData(account);
            this.Account = account;
            this.SetAccess((int)account.roleID);
            CallbackLoginSuccess?.Invoke(true);
        }
        private void OnLoginFail(int errorCode)
        {
            switch (errorCode)
            {
                case DataAccountErrorDefine.ACCOUNT_NOT_EXIST:
                    RightSideMenuViewModel.Ins.ShowNotify("Không tồn tại tài khoản");
                    break;

                case DataAccountErrorDefine.WRONG_PASS:
                    RightSideMenuViewModel.Ins.ShowNotify("Sai mật khẩu");
                    break;
            }
        }

        public void On_EditAccount(string username, string pass)
        {
            AccountModel acc = DataController.Instance.GetAccountByUsername(username);
            if (acc == null)
            {
                this.OnLoginFail(DataAccountErrorDefine.ACCOUNT_NOT_EXIST);
                return;
            }

            acc.userPassword = pass;
            AppUtil.SaveData();
        }

        public void SetAccess(int roleId = -1)
        {
            if(roleId == -1)
            {
                IsAvailableUse_DashBoard = false;
                IsAvailableUse_Order = false;
                IsAvailableUse_Order_Retail = false;
                IsAvailableUse_Customer = false;
                IsAvailableUse_Product = false;
                IsAvailableUse_Supplier = false;
                IsAvailableUse_Account = false;
                IsAvailableUse_Store = false;
                IsAvailableUse_InOutOrder = false;
                IsAvailableUse_InOutOrder_ImportOrder = false;
                IsAvailableUse_InOutOrder_ExportOrder = false;
                //IsAvailableUse_InOutOrder_CorrectlyOrder = true;
                //IsAvailableUse_InOutOrder_ExchangetOrder = true;
                IsAvailableUse_Finance = false;
                IsAvailableUse_Finance_Cash = false;
                IsAvailableUse_Finance_Analytist = false;

                return;
            }

            IsAvailableUse_DashBoard = true;

            //đi từ dưới lên để cấp quyền
            if(roleId <= DataRoleDefine.STAFF)
            {
                IsAvailableUse_Order = true;
                IsAvailableUse_Order_Retail = true;
                IsAvailableUse_Customer = true;
            }
            if(roleId <= DataRoleDefine.STORE_MANAGER)
            {
                //TODO
            }
            if(roleId <= DataRoleDefine.OWNER)
            {
                IsAvailableUse_Product = true;
                IsAvailableUse_Supplier = true;
                IsAvailableUse_Account = true;
                IsAvailableUse_Store = true;
                IsAvailableUse_InOutOrder = true;
                IsAvailableUse_InOutOrder_ImportOrder = true;
                IsAvailableUse_InOutOrder_ExportOrder = true;
                //IsAvailableUse_InOutOrder_CorrectlyOrder = true;
                //IsAvailableUse_InOutOrder_ExchangetOrder = true;
                IsAvailableUse_Finance = true;
                IsAvailableUse_Finance_Cash = true;
                IsAvailableUse_Finance_Analytist = true;
            }
        }

        public void On_Logout()
        {
            CallbackLoginSuccess?.Invoke(false);

            this.CallbackLoginSuccess = null;
            this.Account = null;
            this.SetAccess(-1);
        }
    }
}
