﻿using MaterialDesignThemes.Wpf;
using QLBH.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using QLBH.DataProvider;
namespace QLBH.ViewModel
{
    public class DashboardViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }
        public ICommand cmdAddDraftOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        public ICommand cmdSearchOrder { get; set; }
        #endregion

        #region Property
        private bool isLoaded = false;

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<StoreModel> stores;
        public ObservableCollection<StoreModel> Stores
        {
            get => stores;
            set
            {
                stores = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                this.OnValueChangeData();
                OnPropertyChanged();
            }
        }

        #region Totaol Order Management
        private List<QLBH_ExportOrderModel> qLBH_SaleOrderModels;
        private List<QLBH_ExportOrderModel> qLBH_DeliveringOrderModels;
        private List<QLBH_ExportOrderModel> qLBH_PendingOrderModels;
        private List<QLBH_ExportOrderModel> qLBH_ReturnOrderModels;
        private List<QLBH_ExportOrderModel> qLBH_NoticeOrderModels;


        private ObservableCollection<QLBH_ExportOrderModel> displayListOrders;
        public ObservableCollection<QLBH_ExportOrderModel> DisplayListOrders
        {
            get => displayListOrders;
            set
            {
                displayListOrders = value;
                OnPropertyChanged();
            }
        }

        #endregion Order Management

        #region Order Summary

        private int displayNumPendingOrder;
        public int DisplayNumPendingOrder
        {
            get => displayNumPendingOrder;
            set
            {
                displayNumPendingOrder = value;
                OnPropertyChanged();
            }
        }
        private int displayNumReturnOrder;
        public int DisplayNumReturnOrder
        {
            get => displayNumReturnOrder;
            set
            {
                displayNumReturnOrder = value;
                OnPropertyChanged();
            }
        }
        private int displayNumNoticeOrder;
        public int DisplayNumNoticeOrder
        {
            get => displayNumNoticeOrder;
            set
            {
                displayNumNoticeOrder = value;
                OnPropertyChanged();
            }
        }
        private int displayNumDeliveringOrder;
        public int DisplayNumDeliveringOrder
        {
            get => displayNumDeliveringOrder;
            set
            {
                displayNumDeliveringOrder = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Product Management
        private List<ProductModel> qLBH_Products;

        private ObservableCollection<ProductDashBoardItemModel> displayProducts;
        public ObservableCollection<ProductDashBoardItemModel> DisplayProducts
        {
            get => displayProducts;
            set
            {
                displayProducts = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #endregion

        private static DashboardViewModel ins;
        public static DashboardViewModel Ins
        {
            get
            {
                if (ins == null) ins = new DashboardViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public DashboardViewModel()
        {
            this.PrepareData();
            this.PrepareCommand();

            this.isLoaded = true;
        }

        private void PrepareData()
        {
            this.Stores = DataInstance.Instance.data_Stores;
            this.SelectedStore = this.Stores.FirstOrDefault();

            this.DisplayListOrders = new ObservableCollection<QLBH_ExportOrderModel>();
            this.DisplayProducts = new ObservableCollection<ProductDashBoardItemModel>();

            this.qLBH_SaleOrderModels = new List<QLBH_ExportOrderModel>(DataInstance.Instance.Data_QLBHRetailOrder.Where(x => x.BaseOrder.storeID == this.SelectedStore.id));
            this.qLBH_Products = new List<ProductModel>(DataInstance.Instance.Data_Product);

            OnUpdateListOrder();
            OnUpdateSummaryOrder();
            OnUpdateListProducts();
        }
        private void PrepareCommand()
        {
            cmdAddProduct = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
            });

        }

        public void OnValueChangeData()
        {
            if (!this.isLoaded) return;

            this.qLBH_SaleOrderModels = new List<QLBH_ExportOrderModel>(DataInstance.Instance.Data_QLBHRetailOrder.Where(x => x.BaseOrder.storeID == this.SelectedStore.id));
            this.OnUpdateSummaryOrder();
            this.OnUpdateListProducts();
        }
        private void OnUpdateSummaryOrder()
        {
            this.DisplayNumDeliveringOrder = qLBH_DeliveringOrderModels.Count;
            this.DisplayNumReturnOrder = qLBH_ReturnOrderModels.Count;
            this.DisplayNumPendingOrder = qLBH_NoticeOrderModels.Count;
            this.DisplayNumNoticeOrder = qLBH_PendingOrderModels.Count;
        }
        private void OnUpdateListProducts()
        {
            List<ProductDashBoardItemModel> r = new List<ProductDashBoardItemModel>();
            foreach(ProductModel p in qLBH_Products)
            {
                int stock = (int)DataController.Instance.GetProductPrestock(p.id, this.SelectedStore.id);
                if(stock < p.recommendStock)
                    r.Add(new ProductDashBoardItemModel(p, stock));
            }

            r.Sort((x, y) => x.CurrentStock.CompareTo(y.CurrentStock));

            this.ChangeObCollection(this.DisplayProducts, r);
        }
        private void OnUpdateListOrder()
        {
            this.qLBH_DeliveringOrderModels = new List<QLBH_ExportOrderModel>(this.qLBH_SaleOrderModels.Where(x => x.BaseOrder.ExportOrderState.id == DataExportOrderDefine.DELIVERING_STATE_ID));
            this.qLBH_ReturnOrderModels = new List<QLBH_ExportOrderModel>();
            this.qLBH_NoticeOrderModels = new List<QLBH_ExportOrderModel>(this.qLBH_SaleOrderModels.Where(x => x.BaseOrder.ExportOrderState.id == DataExportOrderDefine.PREPARING_STATE_ID &&
                                                                    !string.IsNullOrEmpty(x.BaseOrder.orderNote)));
            this.qLBH_PendingOrderModels = new List<QLBH_ExportOrderModel>(this.qLBH_SaleOrderModels.Where(x => x.BaseOrder.ExportOrderState.id == DataExportOrderDefine.PREPARING_STATE_ID));

            List<QLBH_ExportOrderModel> r = new List<QLBH_ExportOrderModel>();
            r.AddRange(this.qLBH_PendingOrderModels);
            r.AddRange(this.qLBH_DeliveringOrderModels);

            this.ChangeObCollection(DisplayListOrders, r);
        }

        public void ChangeObCollection(ObservableCollection<ProductDashBoardItemModel> c, IEnumerable<ProductDashBoardItemModel> content)
        {
            c.Clear();
            foreach (ProductDashBoardItemModel item in content)
            {
                c.Add(item);
            }
        }
        public void ChangeObCollection(ObservableCollection<QLBH_ExportOrderModel> c, IEnumerable<QLBH_ExportOrderModel> content)
        {
            c.Clear();
            foreach (QLBH_ExportOrderModel item in content)
            {
                c.Add(item);
            }
        }

    }
}
