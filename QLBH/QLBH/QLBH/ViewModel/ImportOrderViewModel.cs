﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.DataProvider;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class ImportOrderViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }
        public ICommand cmdAddDraftOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        public ICommand cmdSearchOrder { get; set; }
        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<ImportOrderState> orderState;
        public ObservableCollection<ImportOrderState> OrderState
        {
            get => orderState;
            set
            {
                orderState = value;
                OnPropertyChanged();
            }
        }
        private ImportOrderState selectedOrderState;
        public ImportOrderState SelectedOrderState
        {
            get => selectedOrderState;
            set
            {
                selectedOrderState = value;
                OnPropertyChanged();
            }
        }

        private int selectedTabIndex;
        public int SelectedTabIndex { get => selectedTabIndex;
            set {
                selectedTabIndex = value;
                this.On_SwitchingTab();
                OnPropertyChanged();
            } }

        private Visibility visibility_SearchBarProduct;
        public Visibility Visibility_SearchBarProduct
        {
            get => visibility_SearchBarProduct;
            set
            {
                visibility_SearchBarProduct = value;
                OnPropertyChanged();
            }
        }
        private Visibility visibility_SearchBarOrderID;
        public Visibility Visibility_SearchBarOrderID
        {
            get => visibility_SearchBarOrderID;
            set
            {
                visibility_SearchBarOrderID = value;
                OnPropertyChanged();
            }
        }

        private bool isCanEditListProduct;
        public bool IsCanEditListProduct
        {
            get => isCanEditListProduct;
            set
            {
                isCanEditListProduct = value;
                OnPropertyChanged();
            }
        }

        #region Order Management
        private ObservableCollection<QLBH_ImportOrderModel> listOrders;
        public ObservableCollection<QLBH_ImportOrderModel> ListOrders
        {
            get => listOrders;
            set
            {
                listOrders = value;
                OnPropertyChanged();
            }
        }

        private QLBH_ImportOrderModel selectedOrderInSearch;
        public QLBH_ImportOrderModel SelectedOrderInSearch
        {
            get => selectedOrderInSearch;
            set
            {
                selectedOrderInSearch = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            set
            {
                editingOrderID = value;
                OrderStateWindow = editingOrderID == -1 ? "Thêm Phiếu Mới" : "Sửa Phiếu Đã Có";
            }
        }
        private string orderStateWindow;
        public string OrderStateWindow
        {
            get
            {
                return orderStateWindow;
            }
            set
            {
                orderStateWindow = value;
                OnPropertyChanged();
            }
        }
        #endregion Order Management

        #region Add new Order
        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderItemModel> chosingProducts;
        public ObservableCollection<OrderItemModel> ChosingProducts
        {
            get => chosingProducts;
            set
            {
                chosingProducts = value;

                UpdateTotalOrderPrice();

                OnPropertyChanged();
            }
        }

        private string displayTotalOrderPrice;
        public string DisplayTotalOrderPrice { get => displayTotalOrderPrice; set { displayTotalOrderPrice = value; OnPropertyChanged(); } }

        private ProductModel selectedItemsInComboBox;
        public ProductModel SelectedItemsInComboBox { get => selectedItemsInComboBox;
            set
            {
                selectedItemsInComboBox = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<SupplierModel> listSupplier;
        public ObservableCollection<SupplierModel> ListSupplier
        {
            get => listSupplier;
            set
            {
                listSupplier = value;
                OnPropertyChanged();
            }
        }
        private SupplierModel selectedSupplier;
        public SupplierModel SelectedSupplier
        {
            get => selectedSupplier;
            set
            {
                selectedSupplier = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                UpdateStockAllChosingProduct();
                OnPropertyChanged();
            }
        }

        private string displayImportOrderNote;
        public string DisplayImportOrderNote { get => displayImportOrderNote; set { displayImportOrderNote = value; OnPropertyChanged(); } }

        /// <summary>
        /// Phí bên xuất trả -> bên ta trả
        /// </summary>
        private long displayExtraFee;
        public long DisplayExtraFee { get => displayExtraFee;
            set {
                displayExtraFee = value;
                UpdateTotalOrderPrice();
                OnPropertyChanged();
            } }
        #endregion Add new Order
        long? totalBillPrice;
        #endregion

        private static ImportOrderViewModel ins;
        public static ImportOrderViewModel Ins
        {
            get
            {
                if (ins == null) ins = new ImportOrderViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public ImportOrderViewModel()
        {
            PrepareData();

            PrepareCommand();
        }

        private void PrepareData()
        {
            this.products = DataInstance.Instance.data_Product;
            this.ChosingProducts = new ObservableCollection<OrderItemModel>();

            this.ListSupplier = DataInstance.Instance.data_Supplier;
            this.ListStore = DataInstance.Instance.data_Stores;

            this.SelectedStore = this.ListStore.FirstOrDefault();
            this.SelectedSupplier = this.ListSupplier.FirstOrDefault();

            this.DisplayTotalOrderPrice = "0";
            this.SelectedTabIndex = 0;
            this.EditingOrderID = -1;

            this.OrderState = DataInstance.Instance.data_ImportOrderState;
            this.SelectedOrderState = this.OrderState.ToList().Find(x => x.id == DataImportOrderDefine.DRAFT_IMPORT_ORDER_STATE_ID);

            this.ListOrders = DataInstance.Instance.Data_QLBHImportOrder;
            IsCanEditListProduct = true;
        }

        private void PrepareCommand()
        {
            cmdAddProduct = new RelayCommand<object>((p) => { return this.IsCanEditListProduct && this.SelectedStore != null; }, (p) =>
            {
                On_AddProduct(this.SelectedItemsInComboBox);
            });

            cmdDeleteProduct = new RelayCommand<object>((p) => { return IsProductCanEdit(); }, (p) =>
            {
                OrderItemModel removeOItem = (OrderItemModel)p;
                this.ChosingProducts.Remove(removeOItem);
                this.UpdateTotalOrderPrice();

            });

            cmdDeleteOrder = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditOrder = new RelayCommand<object>((p) => { return IsOrderCanEdit(p); }, (p) =>
            {
                //parse order data
                ParseOrderData(p);
                //switch tab
                this.SelectedTabIndex = 0;
            });

            cmdAddOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedSupplier != null && this.SelectedOrderState != null; }, (p) =>
            {
                if (this.editingOrderID == -1) AddNewImportOrder();
                else this.EditExistImportOrder();
            });

            cmdAddDraftOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedSupplier != null && this.SelectedOrderState != null && this.SelectedOrderState.id == DataProvider.DataImportOrderDefine.DRAFT_IMPORT_ORDER_STATE_ID; }, (p) =>
            {
                AddNewImportOrder();
            });

            cmdSearchOrder = new RelayCommand<object>((p) => { return SelectedOrderInSearch != null; }, (p) =>
            {
                //parse order data
                ParseOrderData(SelectedOrderInSearch);
                //switch tab
                this.SelectedTabIndex = 0;
            });
        }

        private bool IsProductCanEdit()
        {
            return this.SelectedOrderState.id < 4; // NCC chaua7 xác nhận thì còn sửa được
        }

        private void ParseOrderData(object p)
        {
            QLBH_ImportOrderModel editOrder = (QLBH_ImportOrderModel)p;
            ImportOrderModel baseOrder = editOrder.BaseOrder;

            this.EditingOrderID = baseOrder.id;
            //parse order data
            this.SelectedStore = baseOrder.StoreModel;
            this.SelectedSupplier = baseOrder.SupplierModel;
            this.DisplayImportOrderNote = baseOrder.orderNote;
            this.DisplayExtraFee = (long)baseOrder.shippingFee;
            this.SelectedOrderState = baseOrder.ImportOrderState;

            List<ImportOrderInfoModel> details = (baseOrder.ImportOrderInfoModels).ToList();
            this.ChosingProducts.Clear();
            for (int i = 0; i < details.Count; i++)
            {
                this.On_AddProduct(details[i].ProductModel);
                this.ChosingProducts[i].SetPrice(
                    inPrice: (long)details[i].priceImport,
                    outPrice: (long)details[i].realRetailPrice,
                    discount: 0
                    );

                this.ChosingProducts[i].Amount = details[i].amount;
            }

            IsCanEditListProduct = this.IsProductCanEdit();
        }

        private void UpdateStockAllChosingProduct()
        {
            foreach (OrderItemModel itemModel in this.ChosingProducts)
            {
                itemModel.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(itemModel.ProductData.id, this.SelectedStore.id));
            }
        }

        private bool IsOrderCanEdit(object p)
        {
            QLBH_ImportOrderModel editOrder = (QLBH_ImportOrderModel)p;
            ImportOrderModel baseOrder = editOrder.BaseOrder;

            return baseOrder.stateID < DataImportOrderDefine.COMPLETED_STATE_ID; //chưa hoàn thành là sửa dc
        }

        public void EditExistImportOrder()
        {
            ImportOrderModel newOrder = DataProvider.DataController.Instance.GetImportOrderByID(this.editingOrderID);
            newOrder.orderNote = this.DisplayImportOrderNote;
            newOrder.shippingFee = this.DisplayExtraFee;
            newOrder.storeID = this.SelectedStore.id;
            newOrder.supplierID = this.SelectedSupplier.id;
            newOrder.stateID = this.SelectedOrderState.id;
            foreach (ImportOrderInfoModel info in newOrder.ImportOrderInfoModels.ToList())
            {
                DataProvider.DataController.Instance.DB.ImportOrderInfoModels.Remove(info);
            }
            //replace data instance

            ImportOrderModel instanceModel = DataProvider.DataInstance.Instance.data_ImportOrder.ToList().Find(x => x.id == newOrder.id);
            instanceModel.orderNote = this.DisplayImportOrderNote;
            instanceModel.shippingFee = this.DisplayExtraFee;
            instanceModel.storeID = this.SelectedStore.id;
            instanceModel.supplierID = this.SelectedSupplier.id;
            instanceModel.stateID = this.SelectedOrderState.id;

            int index = DataInstance.Instance.Data_QLBHImportOrder.ToList().FindIndex(x => x.BaseOrder.id == newOrder.id);
            DataInstance.Instance.Data_QLBHImportOrder.RemoveAt(index);
            DataInstance.Instance.Data_QLBHImportOrder.Insert(index, new QLBH_ImportOrderModel(newOrder));

            AppUtil.SaveData();

            AddOrderDetail(newOrder);

            if (this.SelectedOrderState.id >= DataImportOrderDefine.CONFIRMED_STATE_ID)
            {
                //tạo phiếu chi nếu trong data chưa có
                StoreCashModel bill = DataController.Instance.GetCashBillByOrderID(newOrder.id, true);
                if(bill == null)
                {
                    AddNewCashOutOrder(newOrder);
                }
                else
                {
                    if (bill.stateID == DataCashFlowDefine.PENDING_STATE && newOrder.stateID == DataImportOrderDefine.COMPLETED_STATE_ID)
                        bill.stateID = DataCashFlowDefine.COMPLETE_STATE;
                }
                AppUtil.SaveData();

            }

            this.BoundMessageQueue.Enqueue("Sửa hóa đơn nhập kho thành công");

            ClearOrderData();
            this.EditingOrderID = -1;
        }

        public void UpdateTotalOrderPrice()
        {
            totalBillPrice = 0;
            for (int i = 0; i < this.chosingProducts.Count; i++)
            {
                totalBillPrice += this.chosingProducts[i].TotalPrice;
            }
            totalBillPrice += this.DisplayExtraFee;

            this.DisplayTotalOrderPrice = AppUtil.FormatMoneyDot(totalBillPrice);
        }

        public void AddNewImportOrder()
        {
            ImportOrderModel newOrder = new ImportOrderModel()
            {
                supplierID = this.SelectedSupplier.id,
                dateImport = DateTime.UtcNow,
                shippingFee = this.DisplayExtraFee,
                storeID = this.SelectedStore.id,
                orderNote = this.DisplayImportOrderNote,
                stateID = this.SelectedOrderState.id,
                creatorID = DataController.Instance.CurrentUser.id
            };

            DataController.Instance.DB.ImportOrderModels.Add(newOrder);
            DataInstance.Instance.data_ImportOrder.Add(newOrder);

            AppUtil.SaveData();

            AddOrderDetail(newOrder);

            DataInstance.Instance.Data_QLBHImportOrder.Insert(0, new QLBH_ImportOrderModel(newOrder));

            AddNewCashOutOrder(newOrder);

            this.BoundMessageQueue.Enqueue("Thêm hóa đơn nhập kho thành công");

            ClearOrderData();
        }

        private void AddNewCashOutOrder(ImportOrderModel newOrder)
        {
            if (this.SelectedOrderState.id >= DataImportOrderDefine.CONFIRMED_STATE_ID)
            {
                //tạo phiếu chi
                StoreCashModel bill = new StoreCashModel()
                {
                    detail = newOrder.orderNote,
                    dateCreate = DateTime.UtcNow,
                    valueBill = -1 * (this.totalBillPrice + DisplayExtraFee),
                    storeID = newOrder.storeID,
                    stateID = newOrder.stateID == DataImportOrderDefine.COMPLETED_STATE_ID ? DataCashFlowDefine.COMPLETE_STATE : DataCashFlowDefine.PENDING_STATE,
                    orderID = newOrder.id,
                    creatorID = DataController.Instance.CurrentUser.id
                };
                DataController.Instance.DB.StoreCashModels.Add(bill);
                DataInstance.Instance.data_CashBill.Add(bill);
                DataInstance.Instance.data_QLBHCashBill.Insert(0, new baseFinanceItemModel(bill));

                StoreModel store = DataController.Instance.GetStoreByID((int)newOrder.storeID);
                store.totalMoneyinStore += bill.valueBill;

                AppUtil.SaveData();
            }
        }

        private void AddOrderDetail(ImportOrderModel newOrder)
        {
            for (int i = 0; i < this.ChosingProducts.Count; i++)
            {
                ImportOrderInfoModel newInfoItem = new ImportOrderInfoModel()
                {
                    productID = this.ChosingProducts[i].ProductData.id,
                    amount = this.ChosingProducts[i].Amount,
                    priceImport = this.ChosingProducts[i].InputPrice,
                    realRetailPrice = this.ChosingProducts[i].OutputPrice,

                    importOrderID = newOrder.id
                };
                DataController.Instance.DB.ImportOrderInfoModels.Add(newInfoItem);

                if (this.SelectedOrderState.id != DataImportOrderDefine.COMPLETED_STATE_ID) continue;

                StoreInventoryModel newInventoryItem = new StoreInventoryModel()
                {
                    storeID = this.SelectedStore.id,
                    importOrderProductID = newInfoItem.id,
                    stockProduct = this.ChosingProducts[i].Amount,
                };
                DataController.Instance.DB.StoreInventoryModels.Add(newInventoryItem);
                AppUtil.SaveData();
            }

        }

        private void ClearOrderData()
        {
            this.ChosingProducts.Clear();
            
            DisplayImportOrderNote = "";
            DisplayExtraFee = 0;

            this.IsCanEditListProduct = true;
        }

        public void On_SwitchingTab()
        {
            switch (this.SelectedTabIndex)
            {
                case 0:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
                case 1:
                    this.Visibility_SearchBarOrderID = Visibility.Visible;
                    this.Visibility_SearchBarProduct = Visibility.Collapsed;
                    break;
                default:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
            }
        }

        public void On_AddProduct(ProductModel p)
        {
            if (p == null) return;

            OrderItemModel item = this.ChosingProducts.ToList().Find(x => x.ProductData.id == p.id);
            if (item != null)
            {
                item.Amount += 1;
                return;
            }

            if (item == null || this.ChosingProducts.Count == 0)
            {
                ImportOrderItemModel newOItem = new ImportOrderItemModel(
                    stt: this.ChosingProducts.Count + 1,
                    displayName: p.displayName,
                    amount: 1,
                    product: p);
                newOItem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
                newOItem.SetPreStock(DataController.Instance.GetProductPrestock(p.id, this.SelectedStore.id));
                this.ChosingProducts.Add(newOItem);
            }
        }
    }
}
