﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
using QLBH.DataProvider;

namespace QLBH.ViewModel
{
    public class ProductWindowViewModel : baseViewModel
    {
        private static ProductWindowViewModel ins;
        public static ProductWindowViewModel Ins
        {
            get
            {
                if (ins == null) ins = new ProductWindowViewModel();
                return ins;
            }

            set { ins = value; }
        }


        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products { get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            } }

        private ObservableCollection<QLBH_ProductItemModel> convertedDataProduct;
        public ObservableCollection<QLBH_ProductItemModel> ConvertedDataProduct
        {
            get => convertedDataProduct;
            set
            {
                convertedDataProduct = value;
                OnPropertyChanged();
            }
        }

        #region Property
        private bool isAddProductDialogOpen;
        public bool IsAddProductDialogOpen
        {
            get => isAddProductDialogOpen;
            set
            {
                isAddProductDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private bool isAddCategoryDialogOpen;
        public bool IsAddCategoryDialogOpen
        {
            get => isAddCategoryDialogOpen;
            set
            {
                isAddCategoryDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private bool isAddComboDialogOpen;
        public bool IsAddComboDialogOpen
        {
            get => isAddComboDialogOpen;
            set
            {
                isAddComboDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            get => editingOrderID;
            set
            {
                editingOrderID = value;
                ProductStateWindow = editingOrderID == -1 ? "Thêm Sản Phẩm" : "Sửa Sản Phẩm";
            }
        }
        private string productStateWindow;
        public string ProductStateWindow
        {
            get
            {
                return productStateWindow;
            }
            set
            {
                productStateWindow = value;
                OnPropertyChanged();
            }
        }


        #region Add Product Property
        private string displayName;
        public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }

        private long recommendRetailPrice;
        public long RecommendRetailPrice { get => recommendRetailPrice; set { recommendRetailPrice = value; OnPropertyChanged(); } }

        private int? recommendStock;
        public int? RecommendStock { get => recommendStock; set { recommendStock = value; OnPropertyChanged(); } }

        private int? weight;
        public int? Weight { get => weight; set { weight = value; OnPropertyChanged(); } }

        private string sku;
        public string SKU { get => sku; set { sku = value; OnPropertyChanged(); } }

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<ProductState> productState;
        public ObservableCollection<ProductState> ProductState
        {
            get => productState;
            set
            {
                productState = value;
                OnPropertyChanged();
            }
        }
        private ProductState selectedProductStatee;
        public ProductState SelectedProductStatee
        {
            get => selectedProductStatee;
            set
            {
                selectedProductStatee = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<CategoryModel> listCategory;
        public ObservableCollection<CategoryModel> ListCategory
        {
            get => listCategory;
            set
            {
                listCategory = value;
                OnPropertyChanged();
            }
        }
        private CategoryModel selectedCategory;
        public CategoryModel SelectedCategory
        {
            get => selectedCategory;
            set
            {
                selectedCategory = value;
                OnPropertyChanged();
            }
        }
        #endregion Add Product Property

        #region Add Category Propery
        private string displayCategoryName;
        public string DisplayCategoryName
        {
            get => displayCategoryName;
            set
            {
                displayCategoryName = value;
                OnPropertyChanged();
            }
        }
        #endregion Add Category Propery

        #region Add Combo Propery
        private string displayComboName;
        public string DisplayComboName
        {
            get => displayComboName;
            set
            {
                displayComboName = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ProductModel> listProductAddToCombo;
        public ObservableCollection<ProductModel> ListProductAddToCombo
        {
            get => listProductAddToCombo;
            set
            {
                listProductAddToCombo = value;
                OnPropertyChanged();
            }
        }

        private ProductModel selectedItemsAddToCombo;
        public ProductModel SelectedItemsAddToCombo
        {
            get => selectedItemsAddToCombo;
            set
            {
                selectedItemsAddToCombo = value;
                OnPropertyChanged();
            }
        }
        #endregion Add Combo Propery


        #endregion

        #region Command
        public ICommand cmdLoadedProductDialog { get; set; }
        public ICommand cmdLoadedCategoryDialog { get; set; }
        public ICommand cmdLoadedComboDialog { get; set; }
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdAddCategory { get; set; }
        public ICommand cmdEditProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }

        //TODONEXTPHASE
        public ICommand cmdAddProductToCombo { get; set; }

        #endregion

        public ProductWindowViewModel()
        {
            this.Products = DataInstance.Instance.data_Product;
            this.ListCategory = DataInstance.Instance.data_Category;
            this.ProductState = DataInstance.Instance.data_ProductState;
            this.RecommendStock = 0;
            this.Weight = 0;
            this.EditingOrderID = -1;
            this.RecommendRetailPrice = 0;
            ListProductAddToCombo = new ObservableCollection<ProductModel>();
            this.ConvertedDataProduct = DataInstance.Instance.Data_QLBHProduct;

            //TODO: Khóa button nếu role user là staff trở xuống
            cmdLoadedProductDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                IsAddProductDialogOpen = true;
            });
            cmdLoadedCategoryDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                IsAddCategoryDialogOpen = true;
            });

            //TODONEXTPHASE
            cmdLoadedComboDialog = new RelayCommand<object>((p) => { return false; }, (p) =>
            {
                IsAddComboDialogOpen = true;
            });

            cmdAddProduct = new RelayCommand<object>((p) => { return isFillAllField(); }, (p) =>
            {
                if (this.editingOrderID != -1)
                {
                    On_EditProduct();
                    return;
                }

                On_AddProduct();
            });

            cmdAddCategory = new RelayCommand<object>((p) => { return !string.IsNullOrEmpty(DisplayCategoryName); }, (p) =>
            {
                var pnewCategory = new CategoryModel()
                {
                    displayName = this.DisplayCategoryName,
                };

                DataController.Instance.DB.CategoryModels.Add(pnewCategory);
                AppUtil.SaveData();

                DataInstance.Instance.data_Category.Add(pnewCategory);

                IsAddCategoryDialogOpen = false;

                this.BoundMessageQueue.Enqueue("Thêm ngành hàng thành công");
            });

            //TODONEXTPHASE
            cmdAddProductToCombo = new RelayCommand<object>((p) => { return this.SelectedItemsAddToCombo!= null; }, (p) =>
            {
                this.ListProductAddToCombo.Add(SelectedItemsAddToCombo);
            });

            cmdDeleteProduct = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditProduct = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //parse order data
                ProductModel product = (p as QLBH_ProductItemModel).BaseProduct;
                this.EditingOrderID = product.id;

                if(product != null)
                {
                    this.DisplayName = product.displayName;
                    this.RecommendRetailPrice = (long)product.recommendRetailPrice;
                    this.RecommendStock = product.recommendStock;
                    this.Weight = product.weight;
                    this.SKU = product.SKU;

                    this.SelectedCategory = product.CategoryModel;
                    this.SelectedProductStatee = product.ProductState;
                }
                //switch tab
                this.IsAddProductDialogOpen = true;
            });

        }

        private void On_AddProduct()
        {
            var pnewProduct = new ProductModel()
            {
                displayName = this.DisplayName,
                categoryID = this.SelectedCategory.id,
                stateID = this.SelectedProductStatee.id,
                recommendRetailPrice = this.RecommendRetailPrice,
                recommendStock = this.RecommendStock,
                weight = this.Weight,
                SKU = this.SKU
            };

            DataController.Instance.DB.ProductModels.Add(pnewProduct);
            AppUtil.SaveData();

            DataInstance.Instance.data_Product.Add(pnewProduct);
            DataInstance.Instance.Data_QLBHProduct.Add(new QLBH_ProductItemModel(pnewProduct));

            IsAddProductDialogOpen = false;

            this.BoundMessageQueue.Enqueue("Thêm sản phẩm thành công");
        }

        private void On_EditProduct()
        {
            ProductModel product = DataController.Instance.GetProductByID(this.editingOrderID);
            product.displayName = this.displayName;
            product.recommendRetailPrice = this.recommendRetailPrice;
            product.recommendStock = this.recommendStock;
            product.weight = this.weight;
            product.SKU = this.SKU;

            product.categoryID = this.SelectedCategory.id;
            product.stateID = this.SelectedProductStatee.id;

            //replace data instance

            ProductModel instanceModel = DataInstance.Instance.data_Product.ToList().Find(x => x.id == product.id);
            instanceModel.displayName = this.displayName;
            instanceModel.recommendRetailPrice = this.recommendRetailPrice;
            instanceModel.recommendStock = this.recommendStock;
            instanceModel.weight = this.weight;
            instanceModel.SKU = this.SKU;

            instanceModel.categoryID = this.SelectedCategory.id;
            instanceModel.stateID = this.SelectedProductStatee.id;

            int index = DataInstance.Instance.Data_QLBHProduct.ToList().FindIndex(x => x.BaseProduct.id == product.id);
            DataInstance.Instance.Data_QLBHProduct.RemoveAt(index);
            DataInstance.Instance.Data_QLBHProduct.Insert(index, new QLBH_ProductItemModel(product));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Sửa sản phẩm thành công");
            this.EditingOrderID = -1;
            IsAddProductDialogOpen = false;

        }

        public bool isFillAllField()
        {
            return
                (!string.IsNullOrEmpty(this.DisplayName) &&
                this.SelectedProductStatee != null &&
                this.SelectedCategory != null);
        }
    }
}
