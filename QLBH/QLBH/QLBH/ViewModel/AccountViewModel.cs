﻿using MaterialDesignThemes.Wpf;
using QLBH.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace QLBH.ViewModel
{
    public class AccountViewModel : baseViewModel
    {

        private static AccountViewModel ins;
        public static AccountViewModel Ins
        {
            get
            {
                if (ins == null) ins = new AccountViewModel();
                return ins;
            }

            set { ins = value; }
        }

        private ObservableCollection<QLBH_AccountItemModel> accounts;
        public ObservableCollection<QLBH_AccountItemModel> Accounts
        {
            get => accounts;
            set
            {
                accounts = value;
                OnPropertyChanged();
            }
        }

        private bool isAddAccountDialogOpen;
        public bool IsAddAccountDialogOpen
        {
            get => isAddAccountDialogOpen;
            set
            {
                isAddAccountDialogOpen = value;
                OnPropertyChanged();
            }
        }

        private int editingAccountID;
        public int EditingAccountID
        {
            get => editingAccountID;
            set
            {
                editingAccountID = value;
                AccountStateWindow = editingAccountID == -1 ? "Thêm Tài khoản" : "Sửa Tài khoản";
            }
        }
        private string accountStateWindow;
        public string AccountStateWindow
        {
            get
            {
                return accountStateWindow;
            }
            set
            {
                accountStateWindow = value;
                OnPropertyChanged();
            }
        }

        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        #region Property
        #region Add Account Property
        private string displayName;
        public string DisplayName { get => displayName; set { displayName = value; OnPropertyChanged(); } }

        private string displayEmail;
        public string DisplayEmail { get => displayEmail; set { displayEmail = value; OnPropertyChanged(); } }

        private string displayPassword;
        public string DisplayPassword { get => displayPassword; set { displayPassword = value; OnPropertyChanged(); } }

        private string displayConfirmPassword;
        public string DisplayConfirmPassword { get => displayConfirmPassword; set { displayConfirmPassword = value; OnPropertyChanged(); } }

        private ObservableCollection<RoleModel> roleList;
        public ObservableCollection<RoleModel> RoleList
        {
            get => roleList;
            set
            {
                roleList = value;
                OnPropertyChanged();
            }
        }

        private RoleModel selectedRole;
        public RoleModel SelectedRole
        {
            get => selectedRole;
            set
            {
                selectedRole = value;
                OnPropertyChanged();
            }
        }

        #endregion Add Account Property
        #endregion

        #region Command
        public ICommand cmdLoadedAddAccountDialog { get; set; }

        public ICommand cmdEditAccount { get; set; }
        public ICommand cmdDeleteAccount { get; set; }
        #endregion

        public AccountViewModel()
        {
            PrepareData();

            PrepareCommand();
        }

        private void PrepareData()
        {
            this.Accounts = DataProvider.DataInstance.Instance.data_QLBHAccount;
            this.RoleList = DataProvider.DataInstance.Instance.data_Role;
            this.EditingAccountID = -1;
        }

        private void PrepareCommand()
        {
            cmdLoadedAddAccountDialog = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                IsAddAccountDialogOpen = true;
            });

            cmdDeleteAccount = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditAccount = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                //parse order data
                AccountModel account = (p as QLBH_AccountItemModel).BaseModel;

                if (account != null)
                {
                    this.EditingAccountID = account.id;

                    this.DisplayName = account.displayName;
                    this.DisplayEmail = account.displayEmail;
                    this.DisplayPassword = account.userPassword;
                    this.DisplayConfirmPassword = "";

                    this.SelectedRole = account.RoleModel;
                }
                //switch tab
                this.IsAddAccountDialogOpen = true;
            });
        }

        public void On_FinishRegAccount()
        {
            if (!this.Is_FieldValid()) return;

            if (editingAccountID != -1)
            {
                On_EditAccount();
                return;
            }

            On_AddAccount();
        }
        public void On_AddAccount()
        {
            if (DataProvider.DataController.Instance.GetAccountByUsername(this.DisplayName) != null)
            {
                this.BoundMessageQueue.Enqueue("Tài khoản đã tồn tại");
                return;
            }

            var newAccountItem = new AccountModel()
            {
                displayName = this.DisplayName,
                displayEmail = this.DisplayEmail,
                userPassword = this.DisplayPassword,
                startUsingDate = DateTime.UtcNow,
                roleID = this.SelectedRole.id
            };

            DataProvider.DataController.Instance.DB.AccountModels.Add(newAccountItem);
            DataProvider.DataInstance.Instance.data_Account.Add(newAccountItem);
            DataProvider.DataInstance.Instance.data_QLBHAccount.Add(new QLBH_AccountItemModel(newAccountItem));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Thêm tài khoản mới thành công");

            IsAddAccountDialogOpen = false;
        }

        public void On_EditAccount()
        {
            AccountModel account = DataProvider.DataController.Instance.GetAccountByID(this.editingAccountID);
            account.displayName = this.displayName;
            account.displayEmail = this.DisplayEmail;
            account.userPassword = this.DisplayPassword;
            account.roleID = this.SelectedRole.id;

            //replace data instance

            AccountModel instanceAccount = DataProvider.DataInstance.Instance.data_Account.ToList().Find(x => x.id == account.id);
            instanceAccount.displayName = this.displayName;
            instanceAccount.displayEmail = this.displayEmail;
            instanceAccount.userPassword = this.DisplayPassword;
            instanceAccount.roleID = this.SelectedRole.id;

            int index = DataProvider.DataInstance.Instance.data_QLBHAccount.ToList().FindIndex(x => x.BaseModel.id == account.id);
            DataProvider.DataInstance.Instance.data_QLBHAccount.RemoveAt(index);
            DataProvider.DataInstance.Instance.data_QLBHAccount.Insert(index, new QLBH_AccountItemModel(account));

            AppUtil.SaveData();

            this.BoundMessageQueue.Enqueue("Sửa tài khoản thành công");
            this.EditingAccountID = -1;
            IsAddAccountDialogOpen = false;

        }

        #region Validation

        public bool isFillAllField()
        {
            return
                (!string.IsNullOrEmpty(this.DisplayName) &&
                !string.IsNullOrEmpty(this.DisplayEmail) &&
                !string.IsNullOrEmpty(this.DisplayPassword) &&
                !string.IsNullOrEmpty(this.DisplayConfirmPassword) &&
                this.SelectedRole != null);
        }
        public bool Is_FieldValid()
        {
            if (!this.isFillAllField())
            {
                this.BoundMessageQueue.Enqueue("Vui lòng điền đủ các trường");
                return false;
            }
            if (!this.DisplayConfirmPassword.Equals(this.DisplayPassword))
            {
                this.BoundMessageQueue.Enqueue("Mật khẩu xác thực không khớp");
                return false;
            }

            return true;
        }
        #endregion Validation
    }
}

