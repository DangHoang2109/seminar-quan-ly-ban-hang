﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class CorrectlyStockOrderViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }
        public ICommand cmdAddDraftOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        public ICommand cmdSearchOrder { get; set; }
        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<ExportOrderState> orderState;
        public ObservableCollection<ExportOrderState> OrderState
        {
            get => orderState;
            set
            {
                orderState = value;
                OnPropertyChanged();
            }
        }
        private ExportOrderState selectedOrderState;
        public ExportOrderState SelectedOrderState
        {
            get => selectedOrderState;
            set
            {
                selectedOrderState = value;
                OnPropertyChanged();
            }
        }

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get => selectedTabIndex;
            set
            {
                selectedTabIndex = value;
                this.On_SwitchingTab();
                OnPropertyChanged();
            }
        }

        private Visibility visibility_SearchBarProduct;
        public Visibility Visibility_SearchBarProduct
        {
            get => visibility_SearchBarProduct;
            set
            {
                visibility_SearchBarProduct = value;
                OnPropertyChanged();
            }
        }
        private Visibility visibility_SearchBarOrderID;
        public Visibility Visibility_SearchBarOrderID
        {
            get => visibility_SearchBarOrderID;
            set
            {
                visibility_SearchBarOrderID = value;
                OnPropertyChanged();
            }
        }

        #region Order Management
        private ObservableCollection<QLBH_ExportOrderModel> listOrders;
        public ObservableCollection<QLBH_ExportOrderModel> ListOrders
        {
            get => listOrders;
            set
            {
                listOrders = value;
                OnPropertyChanged();
            }
        }

        private QLBH_ExportOrderModel selectedOrderInSearch;
        public QLBH_ExportOrderModel SelectedOrderInSearch
        {
            get => selectedOrderInSearch;
            set
            {
                selectedOrderInSearch = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            set
            {
                editingOrderID = value;
                OrderStateWindow = editingOrderID == -1 ? "Thêm Phiếu Mới" : "Sửa Phiếu Đã Có";
            }
        }
        private string orderStateWindow;
        public string OrderStateWindow
        {
            get
            {
                return orderStateWindow;
            }
            set
            {
                orderStateWindow = value;
                OnPropertyChanged();
            }
        }
        #endregion Order Management

        #region Add new Order
        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderItemModel> chosingProducts;
        public ObservableCollection<OrderItemModel> ChosingProducts
        {
            get => chosingProducts;
            set
            {
                chosingProducts = value;
                OnPropertyChanged();
            }
        }

        private string displayTotalOrderPrice;
        public string DisplayTotalOrderPrice { get => displayTotalOrderPrice; set { displayTotalOrderPrice = value; OnPropertyChanged(); } }

        private ProductModel selectedItemsInComboBox;
        public ProductModel SelectedItemsInComboBox
        {
            get => selectedItemsInComboBox;
            set
            {
                selectedItemsInComboBox = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                UpdateStockAllChosingProduct();
                OnPropertyChanged();
            }
        }

        private string displayImportOrderNote;
        public string DisplayImportOrderNote { get => displayImportOrderNote; set { displayImportOrderNote = value; OnPropertyChanged(); } }

        #endregion Add new Order

        #endregion

        private static CorrectlyStockOrderViewModel ins;
        public static CorrectlyStockOrderViewModel Ins
        {
            get
            {
                if (ins == null) ins = new CorrectlyStockOrderViewModel();
                return ins;
            }

            set { ins = value; }
        }
        private void UpdateStockAllChosingProduct()
        {
            foreach (OrderItemModel itemModel in this.ChosingProducts)
            {
                itemModel.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(itemModel.ProductData.id, this.SelectedStore.id));
            }
        }

        public CorrectlyStockOrderViewModel()
        {
            this.products = DataProvider.DataInstance.Instance.data_Product;
            this.ChosingProducts = new ObservableCollection<OrderItemModel>();
            this.ListStore = DataProvider.DataInstance.Instance.data_Stores;
            this.DisplayTotalOrderPrice = "0";
            this.SelectedTabIndex = 0;
            this.EditingOrderID = -1;
            this.OrderState = DataProvider.DataInstance.Instance.data_ExportOrderState;
            this.ListOrders = DataProvider.DataInstance.Instance.Data_QLBHExportOrder;

            cmdAddProduct = new RelayCommand<object>((p) => { return this.SelectedStore != null; }, (p) =>
            {
                On_AddProduct(this.SelectedItemsInComboBox);
            });

            cmdDeleteProduct = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                OrderItemModel removeOItem = (OrderItemModel)p;
                this.ChosingProducts.Remove(removeOItem);
            });

            cmdDeleteOrder = new RelayCommand<object>((p) => { return false; }, (p) =>
            {

            });

            cmdEditOrder = new RelayCommand<object>((p) => { return IsOrderCanEdit(p); }, (p) =>
            {
                //parse order data
                ParseOrderData(p);
                //switch tab
                this.SelectedTabIndex = 0;
            });

            cmdAddOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedOrderState != null; }, (p) =>
            {
                if (this.editingOrderID == -1) AddNewExportOrder();
                else this.EditExistExportOrder();
            });

            cmdAddDraftOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedOrderState != null && this.SelectedOrderState.id == DataProvider.DataImportOrderDefine.DRAFT_IMPORT_ORDER_STATE_ID; }, (p) =>
            {
                AddNewExportOrder();
            });

            cmdSearchOrder = new RelayCommand<object>((p) => { return SelectedOrderInSearch != null; }, (p) =>
            {
                //parse order data
                ParseOrderData(SelectedOrderInSearch);
                //switch tab
                this.SelectedTabIndex = 0;
            });
        }

        private void ParseOrderData(object p)
        {
            QLBH_ExportOrderModel editOrder = (QLBH_ExportOrderModel)p;
            ExportOrderModel baseOrder = editOrder.BaseOrder;

            this.EditingOrderID = baseOrder.id;
            //parse order data
            this.SelectedStore = baseOrder.StoreModel;
            this.DisplayImportOrderNote = baseOrder.orderNote;
            this.SelectedOrderState = baseOrder.ExportOrderState;

            List<ExportOrderInfoModel> details = (baseOrder.ExportOrderInfoModels).ToList();
            this.ChosingProducts.Clear();

            for (int i = 0; i < details.Count; i++)
            {
                this.On_AddProduct(details[i].StoreInventoryModel.ImportOrderInfoModel.ProductModel);
                this.ChosingProducts[i].SetPrice(
                    inPrice: (long)details[i].StoreInventoryModel.ImportOrderInfoModel.priceImport,
                    outPrice: (long)details[i].priceExport,
                    discount: 0
                    );

                this.ChosingProducts[i].Amount = details[i].amount;
            }
        }
        private bool IsOrderCanEdit(object p)
        {
            QLBH_ExportOrderModel editOrder = (QLBH_ExportOrderModel)p;
            ExportOrderModel baseOrder = editOrder.BaseOrder;

            return (baseOrder.stateID == 1 || //cancel
                baseOrder.stateID == 2 || //draft
                baseOrder.stateID == 3); //preparing
        }
        public void EditExistExportOrder()
        {
            ExportOrderModel newOrder = DataProvider.DataController.Instance.GetExportOrderByID(this.editingOrderID);
            newOrder.orderNote = this.DisplayImportOrderNote;
            newOrder.storeID = this.SelectedStore.id;
            newOrder.stateID = this.SelectedOrderState.id;
            foreach (ExportOrderInfoModel info in newOrder.ExportOrderInfoModels.ToList())
            {
                DataProvider.DataController.Instance.DB.ExportOrderInfoModels.Remove(info);
            }
            //replace data instance

            ExportOrderModel instanceModel = DataProvider.DataInstance.Instance.data_ExportOrder.ToList().Find(x => x.id == newOrder.id);
            instanceModel.orderNote = this.DisplayImportOrderNote;
            instanceModel.storeID = this.SelectedStore.id;
            instanceModel.stateID = this.SelectedOrderState.id;

            int index = DataProvider.DataInstance.Instance.Data_QLBHExportOrder.ToList().FindIndex(x => x.BaseOrder.id == newOrder.id);

            AddOrderDetail(newOrder);

            DataProvider.DataInstance.Instance.Data_QLBHExportOrder.RemoveAt(index);
            DataProvider.DataInstance.Instance.Data_QLBHExportOrder.Insert(index, new QLBH_ExportOrderModel(newOrder));

            AppUtil.SaveData();


            this.BoundMessageQueue.Enqueue("Sửa hóa đơn nhập kho thành công");

            ClearOrderData();
            this.EditingOrderID = -1;
        }

        public void UpdateTotalOrderPrice()
        {
            long? d = 0;
            for (int i = 0; i < this.chosingProducts.Count; i++)
            {
                d += this.chosingProducts[i].TotalPrice;
            }

            this.DisplayTotalOrderPrice = AppUtil.FormatMoneyDot(d);
        }

        public void AddNewExportOrder()
        {
            ExportOrderModel newOrder = new ExportOrderModel()
            {
                customerID = DataProvider.DataController.Instance.GetExportDummyID(),
                dateExport = DateTime.UtcNow,
                shippingFee = 0,
                extraFeeForReceiver = 0,
                storeID = this.SelectedStore.id,
                orderNote = this.DisplayImportOrderNote,
                reasonExport = string.Format("Hóa đơn xuất kho ngày {0}", DateTime.UtcNow.ToShortDateString()),
                stateID = this.SelectedOrderState.id
            };

            AddOrderDetail(newOrder);

            DataProvider.DataController.Instance.DB.ExportOrderModels.Add(newOrder);
            DataProvider.DataInstance.Instance.data_ExportOrder.Add(newOrder);
            DataProvider.DataInstance.Instance.Data_QLBHExportOrder.Insert(0, new QLBH_ExportOrderModel(newOrder));
            AppUtil.SaveData();


            this.BoundMessageQueue.Enqueue("Thêm hóa đơn xuất kho thành công");
        }

        private void AddOrderDetail(ExportOrderModel newOrder)
        {
            for (int i = 0; i < this.ChosingProducts.Count; i++)
            {
                List<DataProvider.ProductDataForExport> export = DataProvider.DataController.Instance.GetFarestImportOrderIDByProduct(this.ChosingProducts[i].ProductData.id, this.ChosingProducts[i].Amount);
                if (export == null) continue;

                for (int ii = 0; ii < export.Count; ii++)
                {
                    ExportOrderInfoModel newInfoItem = new ExportOrderInfoModel()
                    {
                        amount = export[ii].amount,
                        priceDiscount = 0, //xuất kho ko có discount
                        exportOrderID = newOrder.id,
                        priceExport = this.ChosingProducts[i].OutputPrice,
                        storeInventoryID = export[ii].storeInventoryInfo.id
                    };
                    DataProvider.DataController.Instance.DB.ExportOrderInfoModels.Add(newInfoItem);
                }

                if (this.SelectedOrderState.id != DataProvider.DataExportOrderDefine.COMPLETED_STATE_ID) continue;
            }
            AppUtil.SaveData();

        }

        private void ClearOrderData()
        {
            this.ChosingProducts.Clear();

            DisplayImportOrderNote = "";

        }
        public void On_SwitchingTab()
        {
            switch (this.SelectedTabIndex)
            {
                case 0:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
                case 1:
                    this.Visibility_SearchBarOrderID = Visibility.Visible;
                    this.Visibility_SearchBarProduct = Visibility.Collapsed;
                    break;
                default:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
            }
        }
        public void On_AddProduct(ProductModel p)
        {
            if (p == null) return;

            OrderItemModel item = this.ChosingProducts.ToList().Find(x => x.ProductData.id == p.id);
            if (item != null)
            {
                item.Amount += 1;
                return;
            }

            if (item == null || this.ChosingProducts.Count == 0)
            {
                ExportOrderItemModel newOEtem = new ExportOrderItemModel(
                        stt: this.ChosingProducts.Count + 1,
                        displayName: this.SelectedItemsInComboBox.displayName,
                        amount: 1,
                        product: this.SelectedItemsInComboBox);

                newOEtem.OutputPrice = this.SelectedItemsInComboBox.recommendRetailPrice;

                newOEtem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
                newOEtem.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(this.SelectedItemsInComboBox.id, this.SelectedStore.id));
                this.ChosingProducts.Add(newOEtem);
            }
        }


    }
}