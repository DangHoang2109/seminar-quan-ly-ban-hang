﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace QLBH
{
    public class IConverter_FormatDotMoney : IValueConverter
    {
        private static IConverter_FormatDotMoney ins;
        public static IConverter_FormatDotMoney Ins
        {
            get
            {

                if (ins == null) ins = new IConverter_FormatDotMoney();
                return ins;
            }

            set { ins = value; }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return AppUtil.FormatMoneyDot((long)value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
