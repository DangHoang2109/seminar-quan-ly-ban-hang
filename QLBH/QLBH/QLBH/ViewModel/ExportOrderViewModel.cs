﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using QLBH.DataProvider;
using QLBH.Model;
namespace QLBH.ViewModel
{
    public class ExportOrderViewModel : baseViewModel
    {
        #region Command
        public ICommand cmdAddProduct { get; set; }
        public ICommand cmdDeleteProduct { get; set; }
        public ICommand cmdAddOrder { get; set; }
        public ICommand cmdAddDraftOrder { get; set; }
        public ICommand cmdEditOrder { get; set; }
        public ICommand cmdDeleteOrder { get; set; }
        public ICommand cmdSearchOrder { get; set; }
        #endregion

        #region Property
        public SnackbarMessageQueue BoundMessageQueue { get; } = new SnackbarMessageQueue();

        private ObservableCollection<ExportOrderState> orderState;
        public ObservableCollection<ExportOrderState> OrderState
        {
            get => orderState;
            set
            {
                orderState = value;
                OnPropertyChanged();
            }
        }
        private ExportOrderState selectedOrderState;
        public ExportOrderState SelectedOrderState
        {
            get => selectedOrderState;
            set
            {
                selectedOrderState = value;
                OnPropertyChanged();
            }
        }

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get => selectedTabIndex;
            set
            {
                selectedTabIndex = value;
                this.On_SwitchingTab();
                OnPropertyChanged();
            }
        }

        private Visibility visibility_SearchBarProduct;
        public Visibility Visibility_SearchBarProduct
        {
            get => visibility_SearchBarProduct;
            set
            {
                visibility_SearchBarProduct = value;
                OnPropertyChanged();
            }
        }
        private Visibility visibility_SearchBarOrderID;
        public Visibility Visibility_SearchBarOrderID
        {
            get => visibility_SearchBarOrderID;
            set
            {
                visibility_SearchBarOrderID = value;
                OnPropertyChanged();
            }
        }

        #region Order Management
        private ObservableCollection<QLBH_ExportOrderModel> listOrders;
        public ObservableCollection<QLBH_ExportOrderModel> ListOrders
        {
            get => listOrders;
            set
            {
                listOrders = value;
                OnPropertyChanged();
            }
        }

        private QLBH_ExportOrderModel selectedOrderInSearch;
        public QLBH_ExportOrderModel SelectedOrderInSearch
        {
            get => selectedOrderInSearch;
            set
            {
                selectedOrderInSearch = value;
                OnPropertyChanged();
            }
        }

        private int editingOrderID;
        public int EditingOrderID
        {
            set
            {
                editingOrderID = value;
                OrderStateWindow = editingOrderID == -1 ? "Thêm Phiếu Mới" : "Sửa Phiếu Đã Có";
            }
        }
        private string orderStateWindow;
        public string OrderStateWindow
        {
            get
            {
                return orderStateWindow;
            }
            set
            {
                orderStateWindow = value;
                OnPropertyChanged();
            }
        }
        #endregion Order Management

        #region Add new Order
        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get => products;
            set
            {
                products = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderItemModel> chosingProducts;
        public ObservableCollection<OrderItemModel> ChosingProducts
        {
            get => chosingProducts;
            set
            {
                chosingProducts = value;

                UpdateTotalOrderPrice();

                OnPropertyChanged();
            }
        }

        private string displayTotalOrderPrice;
        public string DisplayTotalOrderPrice { get => displayTotalOrderPrice; set { displayTotalOrderPrice = value; OnPropertyChanged(); } }

        private ProductModel selectedItemsInComboBox;
        public ProductModel SelectedItemsInComboBox
        {
            get => selectedItemsInComboBox;
            set
            {
                selectedItemsInComboBox = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<StoreModel> listStore;
        public ObservableCollection<StoreModel> ListStore
        {
            get => listStore;
            set
            {
                listStore = value;
                OnPropertyChanged();
            }
        }
        private StoreModel selectedStore;
        public StoreModel SelectedStore
        {
            get => selectedStore;
            set
            {
                selectedStore = value;
                UpdateStockAllChosingProduct();
                OnPropertyChanged();
            }
        }

        private string displayImportOrderNote;
        public string DisplayImportOrderNote { get => displayImportOrderNote; set { displayImportOrderNote = value; OnPropertyChanged(); } }

        /// <summary>
        /// Phí bên xuất trả -> bên ta trả
        /// </summary>
        private long displayExtraFee = 0;
        public long DisplayExtraFee
        {
            get => displayExtraFee;
            set
            {
                displayExtraFee = value;
                UpdateTotalOrderPrice();
                OnPropertyChanged();
            }
        }

        //phí bên nhận trả
        private long displayExtraFeeForReceiver = 0;
        public long DisplayExtraFeeForReceiver
        {
            get => displayExtraFeeForReceiver;
            set
            {
                displayExtraFeeForReceiver = value;
                OnPropertyChanged();
            }
        }
        #endregion Add new Order

        #endregion

        private static ExportOrderViewModel ins;
        public static ExportOrderViewModel Ins
        {
            get
            {
                if (ins == null) ins = new ExportOrderViewModel();
                return ins;
            }

            set { ins = value; }
        }

        public ExportOrderViewModel()
        {
            PrepareData();

            PrepareCommand();
        }

        private void PrepareCommand()
        {
            cmdAddProduct = new RelayCommand<object>((p) => { return this.IsCanEditListProduct && this.SelectedStore != null; }, (p) =>
            {
                On_AddProduct(this.SelectedItemsInComboBox);
            });

            cmdDeleteProduct = new RelayCommand<object>((p) => { return IsProductCanEdit(); }, (p) =>
            {
                OrderItemModel removeOItem = (OrderItemModel)p;
                this.ChosingProducts.Remove(removeOItem);
                this.UpdateTotalOrderPrice();
            });

            cmdDeleteOrder = new RelayCommand<object>((p) => { return true; }, (p) =>
            {
                QLBH_ExportOrderModel qlbhOrder = p as QLBH_ExportOrderModel;
                if(qlbhOrder != null)
                {
                    int orderID = qlbhOrder.BaseOrder.id;

                    //đổi state về deleted 
                    QLBH_ExportOrderModel viewOrder = DataInstance.Instance.Data_QLBHExportOrder.ToList().Find(x => x.BaseOrder.id == orderID);
                    DataInstance.Instance.Data_QLBHExportOrder.Remove(viewOrder);

                    DataInstance.Instance.data_ExportOrder.ToList().Find(x => x.id == orderID).stateID = DataExportOrderDefine.DELETED_STATE_ID;
                    
                    AppUtil.SaveData();
                }
            });

            cmdEditOrder = new RelayCommand<object>((p) => { return IsOrderCanEdit(p); }, (p) =>
            {
                //parse order data
                ParseOrderData(p);
                //switch tab
                this.SelectedTabIndex = 0;
            });

            cmdAddOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedOrderState != null; }, (p) =>
            {
                if (this.editingOrderID == -1) AddNewExportOrder();
                else this.EditExistExportOrder();
            });

            cmdAddDraftOrder = new RelayCommand<object>((p) => { return this.SelectedStore != null && this.SelectedOrderState != null && this.SelectedOrderState.id == DataProvider.DataImportOrderDefine.DRAFT_IMPORT_ORDER_STATE_ID; }, (p) =>
            {
                AddNewExportOrder();
            });

            cmdSearchOrder = new RelayCommand<object>((p) => { return SelectedOrderInSearch != null; }, (p) =>
            {
                //parse order data
                ParseOrderData(SelectedOrderInSearch);
                //switch tab
                this.SelectedTabIndex = 0;
            });
        }

        private void PrepareData()
        {
            this.products = DataInstance.Instance.data_Product;
            this.ChosingProducts = new ObservableCollection<OrderItemModel>();

            this.ListStore = DataInstance.Instance.data_Stores;
            this.SelectedStore = this.ListStore.FirstOrDefault();

            this.DisplayTotalOrderPrice = "0";
            this.SelectedTabIndex = 0;
            this.EditingOrderID = -1;

            this.OrderState = DataInstance.Instance.data_ExportOrderState;
            this.SelectedOrderState = this.OrderState.ToList().Find(x => x.id == DataExportOrderDefine.DRAFT_EXPORT_ORDER_STATE_ID);

            this.ListOrders = DataInstance.Instance.Data_QLBHExportOrder;
            this.IsCanEditListProduct = true;
        }

        private void ParseOrderData(object p)
        {
            QLBH_ExportOrderModel editOrder = (QLBH_ExportOrderModel)p;
            ExportOrderModel baseOrder = editOrder.BaseOrder;

            this.EditingOrderID = baseOrder.id;
            //parse order data
            this.SelectedStore = baseOrder.StoreModel;
            this.DisplayImportOrderNote = baseOrder.orderNote;
            this.DisplayExtraFee = (long)baseOrder.shippingFee;
            this.DisplayExtraFeeForReceiver = (long)baseOrder.extraFeeForReceiver;
            this.SelectedOrderState = baseOrder.ExportOrderState;

            List<ExportOrderInfoModel> details = (baseOrder.ExportOrderInfoModels).ToList();
            this.ChosingProducts.Clear();
            for (int i = 0; i < details.Count; i++)
            {
                if(details[i].storeInventoryID == null)
                {
                    //đơn hủy parse lại, ko có inventory id
                    //add như 1 product bth 
                    this.On_AddProduct(details[i].ProductModel);
                    continue;
                }

                this.On_AddProduct(details[i].StoreInventoryModel.ImportOrderInfoModel.ProductModel);
                this.ChosingProducts[i].SetPrice(
                    inPrice: (long)details[i].StoreInventoryModel.ImportOrderInfoModel.priceImport,
                    outPrice: (long)details[i].priceExport,
                    discount: 0
                    );

                this.ChosingProducts[i].Amount = details[i].amount;
            }

            IsCanEditListProduct = this.IsProductCanEdit();
        }

        private void UpdateStockAllChosingProduct()
        {
            foreach (OrderItemModel itemModel in this.ChosingProducts)
            {
                itemModel.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(itemModel.ProductData.id, this.SelectedStore.id));
            }
        }

        private bool IsOrderCanEdit(object p)
        {
            QLBH_ExportOrderModel editOrder = (QLBH_ExportOrderModel)p;
            ExportOrderModel baseOrder = editOrder.BaseOrder;

            return (baseOrder.stateID < 5);
        }

        public void EditExistExportOrder()
        {
            ExportOrderModel newOrder = DataController.Instance.GetExportOrderByID(this.editingOrderID);
            newOrder.orderNote = this.DisplayImportOrderNote;
            newOrder.shippingFee = this.DisplayExtraFee;
            newOrder.storeID = this.SelectedStore.id;
            newOrder.extraFeeForReceiver = this.DisplayExtraFeeForReceiver;
            newOrder.stateID = this.SelectedOrderState.id;
            foreach (ExportOrderInfoModel info in newOrder.ExportOrderInfoModels.ToList())
            {
                //cộng lại tồn của những bill này trước rồi mới xóa
                if(info.storeInventoryID != null)
                    DataProvider.DataController.Instance.DB.StoreInventoryModels.Find(info.storeInventoryID).stockProduct += info.amount;

                DataProvider.DataController.Instance.DB.ExportOrderInfoModels.Remove(info);
            }
            //replace data instance

            ExportOrderModel instanceModel = DataProvider.DataInstance.Instance.data_ExportOrder.ToList().Find(x => x.id == newOrder.id);
            instanceModel.orderNote = this.DisplayImportOrderNote;
            instanceModel.shippingFee = this.DisplayExtraFee;
            instanceModel.storeID = this.SelectedStore.id;
            instanceModel.extraFeeForReceiver = this.DisplayExtraFeeForReceiver;
            instanceModel.stateID = this.SelectedOrderState.id;

            int index = DataProvider.DataInstance.Instance.Data_QLBHExportOrder.ToList().FindIndex(x => x.BaseOrder.id == newOrder.id);

            AddOrderDetail(newOrder);

            DataProvider.DataInstance.Instance.Data_QLBHExportOrder.RemoveAt(index);
            DataProvider.DataInstance.Instance.Data_QLBHExportOrder.Insert(index, new QLBH_ExportOrderModel(newOrder));

            if (this.SelectedOrderState.id >= DataExportOrderDefine.PREPARING_STATE_ID)
            {
                //tạo phiếu chi nếu trong data chưa có
                StoreCashModel bill = DataController.Instance.GetCashBillByOrderID(newOrder.id, false);
                if (bill == null)
                {
                    AddNewCashOutOrder(newOrder);
                }
                else
                {
                    if (bill.stateID == DataCashFlowDefine.PENDING_STATE && newOrder.stateID == DataImportOrderDefine.COMPLETED_STATE_ID)
                        bill.stateID = DataCashFlowDefine.COMPLETE_STATE;
                }
                AppUtil.SaveData();

            }

            AppUtil.SaveData();


            this.BoundMessageQueue.Enqueue("Sửa hóa đơn nhập kho thành công");

            ClearOrderData();
            this.EditingOrderID = -1;
        }

        public void UpdateTotalOrderPrice()
        {
            totalBillPrice = 0;
            for (int i = 0; i < this.chosingProducts.Count; i++)
            {
                totalBillPrice += this.chosingProducts[i].TotalPrice;
            }
            totalBillPrice -= this.DisplayExtraFee;
            totalBillPrice += this.DisplayExtraFeeForReceiver;

            this.DisplayTotalOrderPrice = AppUtil.FormatMoneyDot(totalBillPrice);
        }

        private bool IsProductCanEdit()
        {
            return this.SelectedOrderState.id < 4; // NCC chaua7 xác nhận thì còn sửa được
        }
        long? totalBillPrice;
        private bool isCanEditListProduct;
        public bool IsCanEditListProduct
        {
            get => isCanEditListProduct;
            set
            {
                isCanEditListProduct = value;
                OnPropertyChanged();
            }
        }

        private void AddNewCashOutOrder(ExportOrderModel newOrder)
        {
            if (this.SelectedOrderState.id >= DataExportOrderDefine.PREPARING_STATE_ID)
            {
                //tạo phiếu chi
                StoreCashModel bill = new StoreCashModel()
                {
                    detail = newOrder.orderNote,
                    dateCreate = DateTime.UtcNow,
                    valueBill = this.totalBillPrice + DisplayExtraFeeForReceiver - DisplayExtraFee,
                    storeID = newOrder.storeID,
                    stateID = newOrder.stateID == DataImportOrderDefine.COMPLETED_STATE_ID ? DataCashFlowDefine.COMPLETE_STATE : DataCashFlowDefine.PENDING_STATE,
                    orderID = newOrder.id,
                    creatorID = DataController.Instance.CurrentUser.id
                };
                DataController.Instance.DB.StoreCashModels.Add(bill);
                DataInstance.Instance.data_CashBill.Add(bill);
                DataInstance.Instance.data_QLBHCashBill.Insert(0, new baseFinanceItemModel(bill));

                StoreModel store = DataController.Instance.GetStoreByID((int)newOrder.storeID);
                store.totalMoneyinStore += bill.valueBill;

                AppUtil.SaveData();

            }
        }

        public void AddNewExportOrder()
        {
            ExportOrderModel newOrder = new ExportOrderModel()
            {
                customerID = DataController.Instance.GetExportDummyID(),
                dateExport = DateTime.UtcNow,
                shippingFee = this.DisplayExtraFee,
                extraFeeForReceiver = this.DisplayExtraFeeForReceiver,
                storeID = this.SelectedStore.id,
                orderNote = this.DisplayImportOrderNote,
                reasonExport = string.Format("{1} số {0}", DateTime.UtcNow.ToShortDateString(), DataExportOrderDefine.PREFIX_EXPORT_ORDER),
                stateID = this.SelectedOrderState.id,
                creatorID = DataController.Instance.CurrentUser.id
            };

            DataController.Instance.DB.ExportOrderModels.Add(newOrder);

            AppUtil.SaveData();

            AddOrderDetail(newOrder);
            DataInstance.Instance.data_ExportOrder.Add(newOrder);

           DataInstance.Instance.Data_QLBHExportOrder.Insert(0, new QLBH_ExportOrderModel(newOrder));
            AppUtil.SaveData();

            AddNewCashOutOrder(newOrder);

            this.BoundMessageQueue.Enqueue("Thêm hóa đơn xuất kho thành công");
        }

        //đoạn cộng lại tồn nếu storeinventory null id thì ko cộng
        // với mỗi sp khi check, kiểm tra nếu là đơn hủy thì vẫn tạo, amont là của chossing product nhưng sotreinventory = null -> khi vào lại đơn này sẽ ko cộng tồn ảo lên

        private void AddOrderDetail(ExportOrderModel newOrder)
        {
            for (int i = 0; i < this.ChosingProducts.Count; i++)
            {
                if(this.SelectedOrderState.id == DataProvider.DataExportOrderDefine.CANCEL_STATE_ID)
                {
                    ExportOrderInfoModel newInfoItem = new ExportOrderInfoModel()
                    {
                        amount = ChosingProducts[i].Amount,
                        priceDiscount = 0, //xuất kho ko có discount
                        exportOrderID = newOrder.id,
                        priceExport = this.ChosingProducts[i].OutputPrice,
                        storeInventoryID = null,
                        productID = ChosingProducts[i].ProductData.id
                    };
                    DataController.Instance.DB.ExportOrderInfoModels.Add(newInfoItem);
                    continue;
                }

                List<ProductDataForExport> export = DataController.Instance.GetFarestImportOrderIDByProduct(this.ChosingProducts[i].ProductData.id, this.ChosingProducts[i].Amount);
                if (export == null) continue;

                for (int ii = 0; ii < export.Count; ii++)
                {
                    ExportOrderInfoModel newInfoItem = new ExportOrderInfoModel()
                    {
                        amount = export[ii].amount,
                        priceDiscount = 0, //xuất kho ko có discount
                        exportOrderID = newOrder.id,
                        priceExport = this.ChosingProducts[i].OutputPrice,
                        storeInventoryID = export[ii].storeInventoryInfo.id,
                        productID = ChosingProducts[i].ProductData.id
                    };
                    DataProvider.DataController.Instance.DB.ExportOrderInfoModels.Add(newInfoItem);
                }
            }
            AppUtil.SaveData();

        }

        private void ClearOrderData()
        {
            this.ChosingProducts.Clear();

            DisplayImportOrderNote = "";
            DisplayExtraFee = 0;
            DisplayExtraFeeForReceiver = 0;

            this.IsCanEditListProduct = true;

        }
        public void On_SwitchingTab()
        {
            switch (this.SelectedTabIndex)
            {
                case 0:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
                case 1:
                    this.Visibility_SearchBarOrderID = Visibility.Visible;
                    this.Visibility_SearchBarProduct = Visibility.Collapsed;
                    break;
                default:
                    this.Visibility_SearchBarOrderID = Visibility.Collapsed;
                    this.Visibility_SearchBarProduct = Visibility.Visible;
                    break;
            }
        }
        public void On_AddProduct(ProductModel p)
        {
            if (p == null) return;

            OrderItemModel item = this.ChosingProducts.ToList().Find(x => x.ProductData.id == p.id);
            if (item != null)
            {
                item.Amount += 1;
                return;
            }

            if (item == null || this.ChosingProducts.Count == 0)
            {
                ExportOrderItemModel newOEtem = new ExportOrderItemModel(
                        stt: this.ChosingProducts.Count + 1,
                        displayName: p.displayName,
                        amount: 1,
                        product: p);

                newOEtem.OutputPrice = p.recommendRetailPrice;

                newOEtem.CbUpdateTotalOrder = this.UpdateTotalOrderPrice;
                newOEtem.SetPreStock(DataProvider.DataController.Instance.GetProductPrestock(p.id, this.SelectedStore.id));
                this.ChosingProducts.Add(newOEtem);
            }

            this.UpdateTotalOrderPrice();
        }


    }
}