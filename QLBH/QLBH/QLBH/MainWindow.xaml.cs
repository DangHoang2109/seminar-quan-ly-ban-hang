﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QLBH.ViewModel;
namespace QLBH
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<UserControl> contentControl;
        MainViewModel VM;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = VM = MainViewModel.Ins;

            contentControl = new List<UserControl>()
            {
                ucDashBoard,

                ucImportOrder,
                ucExportOrderWindow,
                ucExchangeStockOrder,
                ucCorrectlyStockOrder,

                ucSaleOrder,
                ucProduct,
                ucFinance,
                ucAnnalytic,
                ucAccount,
                ucCustomer,
                ucSupplier,
                ucStore
            };

            SetAllVisibility(false);

            this.VM.CallbackLoginSuccess += (isSucces) =>
            {
                if (isSucces) this.twDashBoard_Selected(null, null);
                else this.SetAllVisibility(false);
            };
        }

        private void SetAllVisibility(bool active)
        {
            for (int i = 0; i < this.contentControl.Count; i++)
            {
                this.contentControl[i].Visibility = active ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void twDashBoard_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucDashBoard.Visibility = Visibility.Visible;
        }

        private void twImportOrder_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucImportOrder.Visibility = Visibility.Visible;
        }
        private void twExportOrder_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucExportOrderWindow.Visibility = Visibility.Visible;
        }
        private void twExchangeStockOrder_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucExchangeStockOrder.Visibility = Visibility.Visible;
        }
        private void twCorrectlyStockOrder_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucCorrectlyStockOrder.Visibility = Visibility.Visible;
        }


        private void twSaleOrder_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucSaleOrder.Visibility = Visibility.Visible;
        }

        private void twProduct_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucProduct.Visibility = Visibility.Visible;
        }

        private void twFinance_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucFinance.Visibility = Visibility.Visible;
        }

        private void twAnnalytic_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucAnnalytic.Visibility = Visibility.Visible;
        }

        private void twAccount_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucAccount.Visibility = Visibility.Visible;
        }

        private void twCustomer_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucCustomer.Visibility = Visibility.Visible;
        }

        private void twSupplier_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucSupplier.Visibility = Visibility.Visible;
        }

        private void twStore_Selected(object sender, RoutedEventArgs e)
        {
            SetAllVisibility(false);
            ucStore.Visibility = Visibility.Visible;
        }
    }
}
