﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QLBH.ViewModel;
namespace QLBH.FolderUserControl
{
    /// <summary>
    /// Interaction logic for ucAccountControl.xaml
    /// </summary>
    public partial class ucAccountControl : UserControl
    {
        public AccountViewModel VM;
        public ucAccountControl()
        {
            InitializeComponent();
            this.DataContext = VM = AccountViewModel.Ins;
        }

        private void AccountPopupBtn_Click(object sender, RoutedEventArgs e)
        {
            VM.DisplayPassword = passBox.Password;
            VM.DisplayConfirmPassword = confirmpassBox.Password;

            VM.On_FinishRegAccount();
        }
    }
}
