﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QLBH.ViewModel;
namespace QLBH.FolderUserControl
{
    /// <summary>
    /// Interaction logic for ucCustomerWindow.xaml
    /// </summary>
    public partial class ucCustomerWindow : UserControl
    {
        public CustomerViewModel VM { get; set; }

        public ucCustomerWindow()
        {
            InitializeComponent();
            this.DataContext = VM = CustomerViewModel.Ins;
        }
    }
}
