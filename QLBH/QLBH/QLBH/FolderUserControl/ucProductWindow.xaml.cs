﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QLBH.FolderUserControl
{
    /// <summary>
    /// Interaction logic for ucProductWindow.xaml
    /// </summary>
    public partial class ucProductWindow : UserControl
    {
        public ProductWindowViewModel VM { get; set; }
        public ucProductWindow()
        {
            InitializeComponent();
            this.DataContext = VM = ProductWindowViewModel.Ins;
        }
    }
}
