﻿using QLBH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QLBH.FolderUserControl
{
    /// <summary>
    /// Interaction logic for ucRightSideMenu.xaml
    /// </summary>
    public partial class ucRightSideMenu : UserControl
    {
        public RightSideMenuViewModel VM { get; set; }

        public ucRightSideMenu()
        {
            InitializeComponent();
            this.DataContext = VM = RightSideMenuViewModel.Ins;
        }

        private void BackWinButton_Click(object sender, RoutedEventArgs e)
        {
            RightSideMenuViewModel.Ins.UserPassword = passBox.Password;
            RightSideMenuViewModel.Ins.ConfirmPassword = confirmpassBox.Password;

            passBox.Password = "";
            confirmpassBox.Password = "";

            RightSideMenuViewModel.Ins.On_PressBackWindowButton();
        }

        public void OnFlipPop()
        {
            MaterialDesignThemes.Wpf.Flipper.FlipCommand.Execute(null, null);
        }
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                this.BackWinButton_Click(sender, null);

        }
    }
}
