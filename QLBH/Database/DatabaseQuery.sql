﻿create database QLBH
go

use QLBH
go


create table StoreState(
	id int identity(1,1) primary key,
	displayName nvarchar(20),
)
insert into StoreState(displayName) values(N'DELETED')
insert into StoreState(displayName) values(N'Đóng cửa vĩnh viễn')
insert into StoreState(displayName) values(N'Đang hoạt động')
insert into StoreState(displayName) values(N'Tạm nghỉ')	 

create table SupplierState(
	id int identity(1,1) primary key   ,
	displayName nvarchar(20)  ,
)
insert into SupplierState(displayName) values(N'DELETED')
insert into SupplierState(displayName) values(N'Ngưng hợp tác')
insert into SupplierState(displayName) values(N'Đang hợp tác')

create table ProductState(
	id int identity(1,1)  primary key  ,
	displayName nvarchar(20)  ,
)
insert into ProductState(displayName) values(N'DELETED')
insert into ProductState(displayName) values(N'Ngưng Kinh Doanh')
insert into ProductState(displayName) values(N'Đang Bán')
insert into ProductState(displayName) values(N'Hàng Hot')

create table ImportOrderState(
	id int identity(1,1)  primary key  ,
	displayName nvarchar(20)  ,
)
insert into ImportOrderState(displayName) values(N'Hủy')
insert into ImportOrderState(displayName) values(N'Nháp')
insert into ImportOrderState(displayName) values(N'Chờ xác nhận ')
insert into ImportOrderState(displayName) values(N'Đang giao')
insert into ImportOrderState(displayName) values(N'Hoàn thành')

create table ExportOrderState(
	id int identity(1,1)  primary key  ,
	displayName nvarchar(20)  ,
)
insert into ExportOrderState(displayName) values(N'Hủy')
insert into ExportOrderState(displayName) values(N'Nháp')
insert into ExportOrderState(displayName) values(N'Chuẩn bị hàng ')
insert into ExportOrderState(displayName) values(N'Đang giao')
insert into ExportOrderState(displayName) values(N'Hoàn thành')

create table CashFlowState(
	id int identity(1,1)   primary key ,
	displayName nvarchar(20)  ,
)
insert into CashFlowState(displayName) values(N'Hủy')
insert into CashFlowState(displayName) values(N'Nháp')
insert into CashFlowState(displayName) values(N'Chờ xác nhận')
insert into CashFlowState(displayName) values(N'Hoàn thành')


create table RoleModel(
	id int identity(1,1)  primary key  ,
	displayName nvarchar(max),
) 
insert into RoleModel(displayName) values(N'Chủ cửa hàng')
insert into RoleModel(displayName) values(N'Quản lý cửa hàng')
insert into RoleModel(displayName) values(N'Nhân viên bán hàng')
insert into RoleModel(displayName) values(N'Admin')
 
create table AccountModel(
	id int identity(1,1)   primary key ,
	displayName nvarchar(max),
	userPassword nvarchar(max),
	displayEmail nvarchar(max),
	roleID int  ,
	startUsingDate date,

	foreign key(roleID) references RoleModel(id),
) 

create table SupplierModel(
	id int identity(1,1) primary key,
	displayName nvarchar(max)  ,
	displayAddress nvarchar(max),
	displayEmail nvarchar(max),
	displayPhone nvarchar(max),
	displayWebsite nvarchar(max),
	totalPriceSale bigint,
	startContractDate date,
	supplierStateID int, 
	foreign key(supplierStateID) references SupplierState(id),
)
 
create table CustomerModel(
	id int identity(1,1)  primary key  ,
	displayName nvarchar(max)  ,
	displayAddress nvarchar(max),
	displayEmail nvarchar(max),
	displayPhone nvarchar(max),
	totalBuyMoney bigint default 0,
	totalDebtMoney bigint default 0,
	startBuyingDate date,
)
insert into CustomerModel(displayName, displayAddress, displayEmail, displayPhone) values(N'ExportToken,', N'NoAdrress',N'NoEmail',N'NoPhone')

create table CategoryModel(
	id int identity(1,1)   primary key ,
	displayName nvarchar(max)  ,
) 

create table ProductModel(
	id int identity(1,1)  primary key  ,
	displayName nvarchar(max)  ,
	categoryID int  ,
	stateID int  ,
	recommendRetailPrice bigint default 0,
	SKU nvarchar(max)  ,
	recommendStock int  ,
	weight int  ,

	foreign key(categoryID) references CategoryModel(id),
	foreign key(stateID) references ProductState(id),
) 

create table StoreModel(
	id int identity(1,1)  primary key  ,
	displayName	nvarchar(max),
	displayAddress nvarchar(max),
	totalMoneyinStore bigint default 0,
	displayWebsite nvarchar(max),
	storeStateID int,
	foreign key(storeStateID) references StoreState(id),

) 

create table ImportOrderModel(
	id int identity(1,1)  primary key  ,
	supplierID int  ,
	dateImport date  ,
	shippingFee bigint default 0,
	storeID int  ,
	stateID int  ,
	orderNote nvarchar(max),
	creatorID int,

	foreign key(creatorID) references AccountModel(id),
	foreign key(storeID) references StoreModel(id),
	foreign key(stateID) references ImportOrderState(id),
	foreign key(supplierID) references SupplierModel(id),

)
 
create table ImportOrderInfoModel(
	id int identity(1,1)   primary key ,
	productID int  ,
	amount int  default 0,
	priceImport bigint default 0,
	importOrderID int  ,
	realRetailPrice bigint,
	foreign key(productID) references ProductModel(id),
	foreign key(importOrderID) references ImportOrderModel(id),
) 
create table StoreInventoryModel(
	id int identity(1,1)  primary key  ,
	storeID	int,
	importOrderProductID int,
	stockProduct int,
	creatorID int,

	foreign key(creatorID) references AccountModel(id),
	foreign key(storeID) references StoreModel(id),
	foreign key(importOrderProductID) references ImportOrderInfoModel(id),
) 
create table ExportOrderModel(
	id int identity(1,1)   primary key ,
	customerID int  ,
	dateExport date  ,
	shippingFee bigint default 0,
	storeID int  ,
	stateID int  ,
	extraFeeForReceiver bigint  ,
	reasonExport nvarchar(max),
	orderNote nvarchar(max),
	creatorID int,

	foreign key(creatorID) references AccountModel(id),
	foreign key(storeID) references StoreModel(id),
	foreign key(stateID) references ExportOrderState(id),
	foreign key(customerID) references CustomerModel(id),
) 

create table ExportOrderInfoModel(
	id int identity(1,1)  primary key  ,
	amount int  default 0,
	priceDiscount bigint default 0,
	storeInventoryID int  ,
	exportOrderID int  ,
	priceExport bigint default 0,
	productID int,

	foreign key(exportOrderID) references ExportOrderModel(id),
	foreign key(storeInventoryID) references StoreInventoryModel(id),
	foreign key(productID) references ProductModel(id),
) 

create table StoreCashModel(
	id int identity(1,1)  primary key  ,
	detail nvarchar(max),
	dateCreate date  ,
	valueBill bigint default 0,
	storeID int  ,
	stateID int  ,
	orderID int,
	creatorID int,

	foreign key(creatorID) references AccountModel(id),
	foreign key(storeID) references StoreModel(id),
	foreign key(stateID) references CashFlowState(id),
) 


